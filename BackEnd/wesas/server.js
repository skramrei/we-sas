var express = require('express'),
    app = express(),
    DEV_PORT = 3000,
    bodyParser = require('body-parser');

// NOTE: As of now our API treats a PATCH request as if it were a PUT request

// ----- CORS Config -----
// TODO: Need to change DEV_EMBER_PORT in production
const DEV_EMBER_PORT = 'http://localhost:4200';
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', DEV_EMBER_PORT);
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.header('Access-Control-Allow-Methods', 'POST, PATCH, GET, PUT, DELETE, OPTIONS');
    next();
});


// ----- Body Parsing Middleware -----
app.use(bodyParser.urlencoded({'extended': true}));
app.use(bodyParser.json());


// ----- Logging Middleware Config -----
// NOTE: We only log each request in dev mode
if (app.get('env') != 'production') {
    var logger = require('./services/logger.js');
    logger(app);
}


// ----- DB Config -----
var mongoose = require('mongoose');
mongoose.connect('mongodb://127.0.0.1:27017/sas_dev');


var Models = {
    AcademicLoadModel:          require('./models/academic-load'),
    AcademicProgramCodeModel:   require('./models/academic-program-code'),
    AdmissionRuleModel:         require('./models/admission-rule'),
    BasisOfAdmissionCodeModel:  require('./models/basis-of-admission-code'),
    BasisOfAdmissionModel:      require('./models/basis-of-admission'),
    CityModel:                  require('./models/city'),
    CommentCodeModel:           require('./models/comment-code'),
    CountryModel:               require('./models/country'),
    CourseCodeModel:            require('./models/course-code'),
    DegreeCodeModel:            require('./models/degree-code'),
    DepartmentModel:            require('./models/department'),
    DistributionInfoModel:      require('./models/distribution-info'),
    DistributionResultModel:    require('./models/distribution-result'),
    FacultyModel:               require('./models/faculty'),
    GenderModel:                require('./models/gender'),
    GradeModel:                 require('./models/grade'),
    HSAdmissionAverageModel:    require('./models/hs-admission-average'),
    HSCourseMarkModel:          require('./models/hs-course-mark'),
    HSSubjectModel:             require('./models/hs-subject'),
    ITRProgramModel:            require('./models/itr-program'),
    LogicalExpressionModel:     require('./models/logical-expression'),
    LoginModel:                 require('./models/login'),
    PasswordModel:              require('./models/password'),
    ProgramAdministrationModel: require('./models/program-administration'),
    ProgramRecordModel:         require('./models/program-record'),
    ProvinceModel:              require('./models/province'),
    ResidencyModel:             require('./models/residency'),
    RoleCodeModel:              require('./models/role-code'),
    RolePermissionModel:        require('./models/role-permission'),
    RootModel:                  require('./models/root'),
    ScholarAwardCodeModel:      require('./models/scholar-award-code'),
    SecondarySchoolModel:       require('./models/secondary-school'),
    StudentModel:               require('./models/student'),
    TermCodeModel:              require('./models/term-code'),
    UserModel:                  require('./models/user'),
    UserRoleModel:              require('./models/user-role')
};


// ----- Middleware to Serve Static Files -----
app.use(express.static('public'));


// ----- REST Routes -----
/*
 NOTE: On all resources with only 1 modifyable attribute (ie gender),
 we still support PATCH requests in the spirit of completeness however
 the handlers for these requests will be the same as the handler for the
 PUT request on that resource.
 */

var academicLoadRouteCreator = require('./routes/academic-load-routes');
academicLoadRouteCreator(app, Models.AcademicLoadModel);

var academicProgramCodeRouteCreator = require('./routes/academic-program-code-routes');
academicProgramCodeRouteCreator(app, Models.AcademicProgramCodeModel);

var admissionRuleRouteCreator = require('./routes/admission-rule-routes');
admissionRuleRouteCreator(app, Models.AdmissionRuleModel, Models.LogicalExpressionModel);

var basisOfAdmissionCodeCreator = require('./routes/basis-of-admission-code-routes');
basisOfAdmissionCodeCreator(app, Models.BasisOfAdmissionCodeModel);

var basisOfAdmissionCreator = require('./routes/basis-of-admission-routes');
basisOfAdmissionCreator(app, Models.BasisOfAdmissionModel);

var cityRouteCreator = require('./routes/city-routes');
cityRouteCreator(app, Models.CityModel);

var commentCodeCreator = require('./routes/comment-code-routes');
commentCodeCreator(app, Models.CommentCodeModel);

var countryRouteCreator = require('./routes/country-routes');
countryRouteCreator(app, Models.CountryModel);

var courseCodeCreator = require('./routes/course-code-routes');
courseCodeCreator(app, Models.CourseCodeModel);

var degreeCodeCreator = require('./routes/degree-code-routes');
degreeCodeCreator(app, Models.DegreeCodeModel);

var departmentRouteCreator = require('./routes/department-routes');
departmentRouteCreator(app, Models.DepartmentModel);

var distributionInfoRouteCreator = require('./routes/distribution-info-routes');
distributionInfoRouteCreator(app, Models.DistributionInfoModel);

var distributionResultCreator = require('./routes/distribution-result-routes');
distributionResultCreator(app, Models.DistributionResultModel, Models.DistributionInfoModel, Models.StudentModel, Models.GradeModel,
    Models.CourseCodeModel, Models.AcademicProgramCodeModel, Models.ITRProgramModel, Models.AdmissionRuleModel,
    Models.LogicalExpressionModel, Models.CommentCodeModel);

var facultyRouteCreator = require('./routes/faculty-routes');
facultyRouteCreator(app, Models.FacultyModel);

var genderRouteCreator = require('./routes/gender-routes');
genderRouteCreator(app, Models.GenderModel);

var gradeCreator = require('./routes/grade-routes');
gradeCreator(app, Models.GradeModel);

var hsAdmissionAverageCreator = require('./routes/hs-admission-average-routes');
hsAdmissionAverageCreator(app, Models.HSAdmissionAverageModel);

var hsCourseMarkCreator = require('./routes/hs-course-mark-routes');
hsCourseMarkCreator(app, Models.HSCourseMarkModel);

var hsSubjectCreator = require('./routes/hs-subject-routes');
hsSubjectCreator(app, Models.HSSubjectModel);

var itrRouteCreator = require('./routes/itr-program-routes');
itrRouteCreator(app, Models.ITRProgramModel, Models.StudentModel, Models.AcademicProgramCodeModel, Models.DistributionResultModel, Models.CommentCodeModel, Models.DistributionInfoModel);

var logicalExpressionRouteCreator = require('./routes/logical-expression-routes');
logicalExpressionRouteCreator(app, Models.LogicalExpressionModel);

var loginRouteCreator = require('./routes/login-routes');
loginRouteCreator(app, Models.LoginModel, Models.PasswordModel, Models.UserRoleModel, Models.RolePermissionModel);

var passwordRouteCreator = require('./routes/password-routes');
passwordRouteCreator(app, Models.PasswordModel);

var programAdministrationRouteCreator = require('./routes/program-administration-routes');
programAdministrationRouteCreator(app, Models.ProgramAdministrationModel);

var programRecordCreator = require('./routes/program-record-routes');
programRecordCreator(app, Models.ProgramRecordModel);

var provinceRouteCreator = require('./routes/province-routes');
provinceRouteCreator(app, Models.ProvinceModel);

var residencyRouteCreator = require('./routes/residency-routes');
residencyRouteCreator(app, Models.ResidencyModel);

var roleCodeRouteCreator = require('./routes/role-code-routes');
roleCodeRouteCreator(app, Models.RoleCodeModel);

var rolePermissionRouteCreator = require('./routes/role-permission-routes');
rolePermissionRouteCreator(app, Models.RolePermissionModel);

var rootRouteCreator = require('./routes/root-routes');
rootRouteCreator(app, Models.RootModel);

var scholarAwardCodeCreator = require('./routes/scholar-award-code-routes');
scholarAwardCodeCreator(app, Models.ScholarAwardCodeModel);

var secondarySchoolCreator = require('./routes/secondary-school-routes');
secondarySchoolCreator(app, Models.SecondarySchoolModel);

var studentRouteCreator = require('./routes/student-routes');
studentRouteCreator(app, Models.StudentModel);

var termCodeCreator = require('./routes/term-code-routes');
termCodeCreator(app, Models.TermCodeModel);

var userRoleRouteCreator = require('./routes/user-role-routes');
userRoleRouteCreator(app, Models.UserRoleModel);

var userRouteCreator = require('./routes/user-routes');
userRouteCreator(app, Models.UserModel);

// ----- Configure Catch-All Routes -----
// Any get request to our domain with incorrect directories should be responsed to with 404
app.get('*', function (req, res) {
    res.sendStatus(404);
});

/*
 If a request gets here without being handled by some of the middleware above
 it must be a bad api call (respond with code 400), unless it is an OPTIONS
 request which is valid but handled by express later on in the middleware stack.
 */
app.all('*', function (req, res, next) {
    // OPTIONS requests are handled by Express automaically and are valid.
    if (req.method === 'OPTIONS') {
        next();
    }
    else {
        res.sendStatus(400);
    }
});


// ----- Error Handling Middleware Config -----
var errorHandler = require('./services/error-handler.js');
app.use(errorHandler);

// ----- Configure the Single Distribution Info Document -----
Models.DistributionInfoModel.findOne({id: 1}, function (err, results) {
    if (err) {
        console.log('Error initializing Distribution Info, cannot start server. Raw error:\n' + JSON.stringify(err));
    }
    else {
        if (results == null) {
            // Need to make the document before listening
            var info = new Models.DistributionInfoModel({
                id: 1
            });
            info.save(function (err) {
                if (err) {
                    console.log('Error initializing new Distribution info document, cannot start server. Raw error:\n'
                        + JSON.stringify(err));
                }
                else {
                    // Can start listening
                    app.listen(DEV_PORT, () => {
                        console.log('Server listening on port', DEV_PORT);
                    });
                }
            })
        }
        else {
            // Can just start listening
            app.listen(DEV_PORT, () => {
                console.log('Server listening on port', DEV_PORT);
            });
        }
    }
});
