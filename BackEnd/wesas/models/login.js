//
// Created by stefan on 2016-03-09 3:53 PM
//

var mongoose = require('mongoose');

var schema = new mongoose.Schema({
    userName: String,
    password: String,
    nonce: String,
    response: String,
    token: String,
    requestType: String,
    wrongUserName: Boolean,
    wrongPassword: Boolean,
    passwordMustChanged: Boolean,
    passwordReset: Boolean,
    loginFailed: Boolean,
    sessionIsActive: Boolean
});

module.exports = mongoose.model('Login', schema);