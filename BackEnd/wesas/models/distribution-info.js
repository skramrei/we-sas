/*
    This model contains information about only the latest distribution which needs to be
    persisted because the server needs to be able to find the latest distribution made
    at any given time. There should only be one document of this type in the database
    at any time. This document is found using the hook to locate it.

    Date refers to the latest distribution date and dates referes to all the previous distribution dates
*/

var mongoose = require('mongoose');

var schema = new mongoose.Schema({
    "id": Number,
	"date": String,
    "dates": [String]
});

module.exports = mongoose.model("DistributionInfo", schema);
