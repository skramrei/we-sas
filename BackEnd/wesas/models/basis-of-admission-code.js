var mongoose = require('mongoose');

var schema = new mongoose.Schema({
    name: String,
    basis: {type: mongoose.Schema.ObjectId, ref: "BasisOfAdmission"}
});

module.exports = mongoose.model('BasisOfAdmissionCode', schema);