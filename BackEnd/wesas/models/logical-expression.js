var mongoose = require("mongoose");

var schema = new mongoose.Schema({
    "booleanExp": String,
    "logicalLink": String,
    "link": { "type": mongoose.Schema.ObjectId, "ref": "LogicalExpression" },
    "rule": { "type": mongoose.Schema.ObjectId, "ref": "AdmissionRule" },
    "isFirst": Boolean, // Indicates whether this is the first expression in a linked list of expressions (used to simplify iterations though all expressions associated with a particular admission rule)
    "ifFull": Boolean // Indicates whether this is the a hard rule or only used when program cap is reached
});

module.exports = mongoose.model("LogicalExpression", schema);
