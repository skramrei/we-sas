/*
 NOTE: The names under the ref option should match the
 associated model name defined during the creation of that
 schema's model. TODO: Should we do something to make this
 more maintainable?
 */

var mongoose = require("mongoose");

var schema = new mongoose.Schema({
    name: String,
    code: String,
    subCode: String,
    rule: {type: mongoose.Schema.ObjectId, ref: "AdmissionRule"},
    dept: [{type: mongoose.Schema.ObjectId, ref: "ProgramAdministration"}],
    itrProgram: [{type: mongoose.Schema.ObjectId, ref: "ITRProgram"}],
    commentCode: {type: mongoose.Schema.ObjectId, ref: "CommentCode"}
});

module.exports = mongoose.model("AcademicProgramCode", schema);
