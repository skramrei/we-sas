var mongoose = require('mongoose');

var schema = new mongoose.Schema({
    name: String,
    student: {type: mongoose.Schema.ObjectId, ref: "Student"},
    courseInfo: [{type: mongoose.Schema.ObjectId, ref: "HSCourseMark"}]
});

module.exports = mongoose.model('SecondarySchool', schema);