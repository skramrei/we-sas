/*
 NOTE: The names under the ref option should match the
 associated model name defined during the creation of that
 schema's model. TODO: Should we do something to make this
 more maintainable?
 */

var mongoose = require('mongoose');

var schema = new mongoose.Schema({
    code: String,
    number: String,
    name: String,
    unit: String,
    grades: [{type: mongoose.Schema.ObjectId, ref: "Grade"}]
});

module.exports = mongoose.model("CourseCode", schema);