var mongoose = require('mongoose');

var schema = new mongoose.Schema({
    name: String,
    description: String,
    marks: [{type: mongoose.Schema.ObjectId, ref: "HSCourseMark"}]
});

module.exports = mongoose.model('HSSubject', schema);