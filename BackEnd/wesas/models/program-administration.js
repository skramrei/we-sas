//
// Modified
//

var mongoose = require('mongoose');

var schema = new mongoose.Schema({
    name: String,
    position: String,
    department: {type: mongoose.Schema.ObjectId, ref: "Department"},
    academicProgramCode: {type: mongoose.Schema.ObjectId, ref: "AcademicProgramCode"}
});

module.exports = mongoose.model("ProgramAdministration", schema);