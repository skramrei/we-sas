/*
	NOTE: The names under the ref option should match the
	associated model name defined during the creation of that 
	schema's model. TODO: Should we do something to make this 
	more maintainable?
*/


// TODO: What are the types for these attributes?
var mongoose = require('mongoose');

var schema = new mongoose.Schema({
	"order": Number,
	"eligibility": Boolean,
	"student": { "type": mongoose.Schema.ObjectId, "ref": "Student" },
	"program": { "type": mongoose.Schema.ObjectId, "ref": "AcademicProgramCode" }
});

module.exports = mongoose.model("ITRProgram", schema);