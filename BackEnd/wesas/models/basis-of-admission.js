var mongoose = require('mongoose');

var schema = new mongoose.Schema({
    date: String,
    comment: String,
    code: {type: mongoose.Schema.ObjectId, ref: "BasisOfAdmissionCode"},
    student: {type: mongoose.Schema.ObjectId, ref: "Student"}
});

module.exports = mongoose.model('BasisOfAdmission', schema);