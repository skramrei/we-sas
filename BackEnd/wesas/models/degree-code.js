//
// Created by stefan on 2016-02-27 12:49 PM
//

var mongoose = require("mongoose");

var schema = new mongoose.Schema({
    name: String,
    programRecords: [{type: mongoose.Schema.ObjectId, ref: "ProgramRecord"}]
});

module.exports = mongoose.model("DegreeCode", schema);