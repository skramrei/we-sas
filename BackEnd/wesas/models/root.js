//
// Created by stefan on 2016-03-09 3:56 PM
//

var mongoose = require('mongoose');

var schema = new mongoose.Schema({
    password: String,
    nonce: String,
    response: String,
    wrongPassword: Boolean,
    sessionIsActive: Boolean
});

module.exports = mongoose.model('Root', schema);