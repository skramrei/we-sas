var mongoose = require('mongoose');

var schema = new mongoose.Schema({
	"code": String,
  "progAction": String,
  "description":String,
  "notes": String,
  "results": [{ "type": mongoose.Schema.ObjectId, "ref": "DistributionResult" }],
  "programs": [{ "type": mongoose.Schema.ObjectId, "ref": "AcademicProgramCode" }]
});

module.exports = mongoose.model("CommentCode", schema);
