//
// Modified
//

var mongoose = require('mongoose');

var schema = new mongoose.Schema({
    name: String,
    faculty: {type: mongoose.Schema.ObjectId, ref: "Faculty"},
    programAdministrations: [{type: mongoose.Schema.ObjectId, ref: "ProgramAdministration"}]
});

module.exports = mongoose.model("Department", schema);