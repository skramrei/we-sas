var mongoose = require('mongoose');

var schema = new mongoose.Schema({
    first: String,
    midYear: String,
    finalVal: String,
    grade11: String,
    student: {type: mongoose.Schema.ObjectId, ref: "Student"}
});

module.exports = mongoose.model('HSAdmissionAverage', schema);