/*
 NOTE: The names under the ref option should match the
 associated model name defined during the creation of that
 schema's model. TODO: Should we do something to make this
 more maintainable?
 */

var mongoose = require("mongoose");

var schema = new mongoose.Schema({
    mark: String,
    section: String,
    courseCode: {type: mongoose.Schema.ObjectId, ref: "CourseCode"},
    programRecord: {type: mongoose.Schema.ObjectId, ref: "ProgramRecord"},
    student: {type: mongoose.Schema.ObjectId, ref: "Student"}
});

module.exports = mongoose.model("Grade", schema);

