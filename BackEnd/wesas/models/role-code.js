//
// Created by stefan on 2016-03-09 12:33 PM
//

var mongoose = require('mongoose');

var schema = new mongoose.Schema({
    name: String,
    userRoles: [{type: mongoose.Schema.ObjectId, ref: 'UserRole'}],
    features: [{type: mongoose.Schema.ObjectId, ref: 'RolePermission'}]
});

module.exports = mongoose.model('RoleCode', schema);