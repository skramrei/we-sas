//
// Created by stefan on 2016-03-09 12:28 PM
//

var mongoose = require('mongoose');

var schema = new mongoose.Schema({
    userName: String,
    salt: String,
    encryptedPassword: String,
    userAccountExpiryDate: Date,
    passwordMustChanged : Boolean,
    passwordReset: Boolean,
    user: {type: mongoose.Schema.ObjectId, ref: 'User'}
});

module.exports = mongoose.model('Password', schema);