//
// Created by stefan on 2016-02-27 9:21 AM
//

var mongoose = require("mongoose");

var schema = new mongoose.Schema({
    level: String,
    status: String,
    comment: String,
    degreeCode: {type: mongoose.Schema.ObjectId, ref: "DegreeCode"},
    termCode: {type: mongoose.Schema.ObjectId, ref: "TermCode"},
    grades: [{type: mongoose.Schema.ObjectId, ref: "Grade"}]
});

module.exports = mongoose.model("ProgramRecord", schema);