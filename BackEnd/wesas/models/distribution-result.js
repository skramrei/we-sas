var mongoose = require('mongoose');

var schema = new mongoose.Schema({
	"date": String,
	"student": { "type": mongoose.Schema.ObjectId, "ref": "Student" },
  	"commentCode": { "type": mongoose.Schema.ObjectId, "ref": "CommentCode" },
	"rejected": Boolean
});

module.exports = mongoose.model("DistributionResult", schema);
