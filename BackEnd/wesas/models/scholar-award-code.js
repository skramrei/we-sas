var mongoose = require('mongoose');

var schema = new mongoose.Schema({
    name: String,
    students: [{type: mongoose.Schema.ObjectId, ref: "Student"}]
});

module.exports = mongoose.model('ScholarAwardCode', schema);