//
// Created by Stefan Kramreither on 2/25/2016 9:05
//

var mongoose = require("mongoose");

var schema = new mongoose.Schema({
	name: String,
	programRecords: [{type: mongoose.Schema.ObjectId, ref: "ProgramRecord"}]
});

module.exports = mongoose.model("TermCode", schema);