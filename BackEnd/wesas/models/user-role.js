//
// Created by stefan on 2016-03-09 12:31 PM
//

var mongoose = require('mongoose');

var schema = new mongoose.Schema({
    dateAssigned: Date,
    user: {type: mongoose.Schema.ObjectId, ref: 'User'},
    role: {type: mongoose.Schema.ObjectId, ref: 'RoleCode'}
});

module.exports = mongoose.model('UserRole', schema);