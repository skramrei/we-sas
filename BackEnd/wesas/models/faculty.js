//
// Modified
//

var mongoose = require('mongoose');

var schema = new mongoose.Schema({
    name: String,
    departments: [{type: mongoose.Schema.ObjectId, ref: "Department"}]
});

module.exports = mongoose.model("Faculty", schema);