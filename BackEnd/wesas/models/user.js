//
// Created by stefan on 2016-03-09 12:23 PM
//

var mongoose = require('mongoose');

var schema = new mongoose.Schema({
    firstName: String,
    lastName: String,
    email: String,
    enabled: Boolean,
    userShadow: {type: mongoose.Schema.ObjectId, ref: 'Password'},
    userRoles: [{type: mongoose.Schema.ObjectId, ref: 'UserRole'}]
});

module.exports = mongoose.model('User', schema);