var mongoose = require('mongoose');

var schema = new mongoose.Schema({
    level: String,
    source: String,
    unit: String,
    grade: String,
    subject: {type: mongoose.Schema.ObjectId, ref: "HSSubject"},
    school: {type: mongoose.Schema.ObjectId, ref: "SecondarySchool"}
});

module.exports = mongoose.model('HSCourseMark', schema);