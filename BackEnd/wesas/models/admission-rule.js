var mongoose = require("mongoose");

var schema = new mongoose.Schema({
    description: String,
    testExpressions: [{type: mongoose.Schema.ObjectId, ref: "LogicalExpression"}],
    code: [{type: mongoose.Schema.ObjectId, ref: "AcademicProgramCode"}]
});

module.exports = mongoose.model("AdmissionRule", schema);
