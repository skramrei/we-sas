//
// Created by stefan on 2016-03-09 12:36 PM
//

var mongoose = require('mongoose');

var schema = new mongoose.Schema({
    code: String,
    sysFeature: String,
    roleCodes: [{type: mongoose.Schema.ObjectId, ref: 'RoleCode'}]
});

module.exports = mongoose.model('RolePermission', schema);