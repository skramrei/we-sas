/*
 NOTE: The names under the ref option should match the
 associated model name defined during the creation of that 
 schema's model. TODO: Should we do something to make this 
 more maintainable?
 */


var mongoose = require('mongoose');

var schema = new mongoose.Schema({
    studentNumber: String,
    firstName: String,
    lastName: String,
    DOB: Date,
    cumAVG: String,
    residency: {type: mongoose.Schema.ObjectId, ref: "Residency"},
    gender: {type: mongoose.Schema.ObjectId, ref: "Gender"},
    academicLoad: {type: mongoose.Schema.ObjectId, ref: "AcademicLoad"},
    country: {type: mongoose.Schema.ObjectId, ref: "Country"},
    province: {type: mongoose.Schema.ObjectId, ref: "Province"},
    city: {type: mongoose.Schema.ObjectId, ref: "City"},
    grades: [{type: mongoose.Schema.ObjectId, ref: "Grade"}],
    itrPrograms: [{type: mongoose.Schema.ObjectId, ref: "ITRProgram"}],
    distributionResults: [{type: mongoose.Schema.ObjectId, ref: "DistributionResult"}],
    admBase: {type: mongoose.Schema.ObjectId, ref: "BasisOfAdmission"},
    hsGrade: {type: mongoose.Schema.ObjectId, ref: "HSAdmissionAverage"},
    hSchool: [{type: mongoose.Schema.ObjectId, ref: "SecondarySchool"}],
    awardInfo: {type: mongoose.Schema.ObjectId, ref: "ScholarAwardCode"}
});

module.exports = mongoose.model('Student', schema);