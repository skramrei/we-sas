

module.exports = function (app, DistributionInfoModel) {

    app.route('/distributionInfos')
        .get(function (req, res, next) {
            DistributionInfoModel.findOne({id: 1}, function (err, info) {
                if (err) {
                    res.status(404).json({ error: err });
                    return next(err);
                }
                res.json({distributionInfo: info});
            });
        })

}
