/*
 This file was created to refactor all the city route definitions
 out of server.js. It takes an instance of express() and mounts routes
 onto it, so the caller (server.js) should pass a reference to the
 instance of express() used for the server. It also takes a reference
 to the MongoDB model for the cities collection so it can do
 the appropriate DB operations in responding to requests.
 */

module.exports = function (app, CityModel) {

    app.route('/cities')
            .get(function (req, res, next) {
                if (req.query.province) {
                    CityModel.find({province: req.query.province}, function (err, city) {
                        if (err) {
                            res.status(404).json({ error: err });
                            return next(err);
                        }
                        res.json({city: city});
                    });
                } else {
                    CityModel.find(function (err, city) {
                        if (err) {
                            res.status(404).json({ error: err });
                            return next(err);
                        }
                        res.json({city: city});
                    });
                }
            })
            .post(function (req, res, next) {
                var city = new CityModel({
                    name: req.body.city.name,
                    province: req.body.city.province,
                    students: req.body.city.students
                });
                city.save(function (err) {
                    if (err) {
                        res.status(500).json({ error: err });
                        return next(err);
                    }
                    res.json({city: city});
                });
            })
            .delete(function (req, res, next) {
                CityModel.remove(function (err) {
                    if (err) {
                        res.status(500).json({ error: err });
                        return next(err);
                    }
                    res.json({});
                });
            });


    app.route('/cities/:id')
            .get(function (req, res, next) {
                CityModel.findById(req.params.id, function (err, city) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    res.json({city: city});
                });
            })
            .put(function (req, res, next) {
                CityModel.findById(req.params.id, function (err, city) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    city.name = req.body.city.name;
                    city.province = req.body.city.province;
                    city.studetns = req.body.city.students;
                    city.save(function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.status(201).json({city: city});
                    });
                });
            })
            .patch(function (req, res, next) {
                CityModel.findById(req.params.id, function (err, city) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    city.name = req.body.city.name;
                    city.province = req.body.city.province;
                    city.studetns = req.body.city.students;
                    city.save(function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.status(201).json({city: city});
                    });
                });
            })
            .delete(function (req, res, next) {
                CityModel.findById(req.params.id, function (err, city) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    var tmp = city;
                    CityModel.remove({_id: req.params.id}, function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.json({city: tmp});
                    });
                });
            });
};
