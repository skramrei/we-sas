module.exports = function (app, HSCourseMarkModel) {

    app.route('/hsCourseMarks')
        .get(function (req, res, next) {
            HSCourseMarkModel.find(function (err, mark) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                res.json({hsCourseMark: mark});
            });
        })
        .post(function (req, res, next) {
            var mark = new HSCourseMarkModel({
                level: req.body.hsCourseMark.level,
                source: req.body.hsCourseMark.source,
                unit: req.body.hsCourseMark.unit,
                grade: req.body.hsCourseMark.grade,
                subject: req.body.hsCourseMark.subject,
                school: req.body.hsCourseMark.school
            });
            mark.save(function (err) {
                if (err) {
                    res.status(500).json({error: err});
                    return next(err);
                }
                res.json({hsCourseMark: mark});
            });
        })
        .delete(function (req, res, next) {
            HSCourseMarkModel.remove(function (err) {
                if (err) {
                    res.status(500).json({error: err});
                    return next(err);
                }
                res.json({});
            });
        });


    app.route('/hsCourseMarks/:id')
        .get(function (req, res, next) {
            HSCourseMarkModel.findById(req.params.id, function (err, mark) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                res.json({hsCourseMark: mark});
            });
        })
        .put(function (req, res, next) {
            HSCourseMarkModel.findById(req.params.id, function (err, mark) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                mark.level = req.body.hsCourseMark.level;
                mark.source = req.body.hsCourseMark.source;
                mark.unit = req.body.hsCourseMark.unit;
                mark.grade = req.body.hsCourseMark.grade;
                mark.subject = req.body.hsCourseMark.subject;
                mark.school = req.body.hsCourseMark.school;
                mark.save(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.status(201).json({hsCourseMark: mark});
                });
            });
        })
        .patch(function (req, res, next) {
            HSCourseMarkModel.findById(req.params.id, function (err, mark) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                mark.level = req.body.hsCourseMark.level;
                mark.source = req.body.hsCourseMark.source;
                mark.unit = req.body.hsCourseMark.unit;
                mark.grade = req.body.hsCourseMark.grade;
                mark.subject = req.body.hsCourseMark.subject;
                mark.school = req.body.hsCourseMark.school;
                mark.save(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.status(201).json({hsCourseMark: mark});
                });
            });
        })
        .delete(function (req, res, next) {
            HSCourseMarkModel.findById(req.params.id, function (err, mark) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                var tmp = mark;
                HSCourseMarkModel.remove({_id: req.params.id}, function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.json({hsCourseMark: tmp});
                });
            });
        });
};
