/*
 This file was created to refactor all the faculty route definitions
 out of server.js. It takes an instance of express() and mounts routes
 onto it, so the caller (server.js) should pass a reference to the
 instance of express() used for the server. It also takes a reference
 to the MongoDB model for the programs collection so it can do
 the appropriate DB operations in responding to requests.
 */

module.exports = function (app, FacultyModel) {

    app.route('/faculties')
            .get(function (req, res, next) {
                if (req.query.name) {
                    FacultyModel.find({name: req.query.name}, function (err, faculty) {
                        if (err) {
                            res.status(404).json({error: err});
                            return next(err);
                        }
                        res.json({faculty: faculty});
                    });
                } else {
                    FacultyModel.find(function (err, faculty) {
                        if (err) {
                            res.status(404).json({error: err});
                            return next(err);
                        }
                        res.json({faculty: faculty});
                    });
                }
            })
            .post(function (req, res, next) {
                var faculty = new FacultyModel({
                    name: req.body.faculty.name,
                    departments: req.body.faculty.departments
                });
                faculty.save(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.json({faculty: faculty});
                });
            })
            .delete(function (req, res, next) {
                FacultyModel.remove(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.json({});
                });
            });

    app.route('/faculties/:id')
            .get(function (req, res, next) {
                FacultyModel.findById(req.params.id, function (err, faculty) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    res.json({faculty: faculty});
                });
            })
            .put(function (req, res, next) {
                FacultyModel.findById(req.params.id, function (err, faculty) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    faculty.name = req.body.faculty.name;
                    faculty.students = req.body.faculty.departments;
                    faculty.save(function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.status(201).json({faculty: faculty});
                    });
                });
            })
            .patch(function (req, res, next) {
                FacultyModel.findById(req.params.id, function (err, faculty) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    faculty.name = req.body.faculty.name;
                    faculty.students = req.body.faculty.departments;
                    faculty.save(function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.status(201).json({faculty: faculty});
                    });
                });
            })
            .delete(function (req, res, next) {
                FacultyModel.findById(req.params.id, function (err, faculty) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    var tmp = faculty;
                    FacultyModel.remove({_id: req.params.id}, function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.json({faculty: tmp});
                    });
                });
            });
};
