module.exports = function (app, SecondarySchoolModel) {

    app.route('/secondarySchools')
        .get(function (req, res, next) {
            SecondarySchoolModel.find(function (err, secondarySchool) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                res.json({secondarySchool: secondarySchool});
            });
        })
        .post(function (req, res, next) {
            var secondarySchool = new SecondarySchoolModel({
                name: req.body.secondarySchool.name,
                student: req.body.secondarySchool.student,
                courseInfo: req.body.secondarySchool.courseInfo
            });
            secondarySchool.save(function (err) {
                if (err) {
                    res.status(500).json({error: err});
                    return next(err);
                }
                res.json({secondarySchool: secondarySchool});
            });
        })
        .delete(function (req, res, next) {
            SecondarySchoolModel.remove(function (err) {
                if (err) {
                    res.status(500).json({error: err});
                    return next(err);
                }
                res.json({});
            });
        });


    app.route('/secondarySchools/:id')
        .get(function (req, res, next) {
            SecondarySchoolModel.findById(req.params.id, function (err, secondarySchool) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                res.json({secondarySchool: secondarySchool});
            });
        })
        .put(function (req, res, next) {
            SecondarySchoolModel.findById(req.params.id, function (err, secondarySchool) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                secondarySchool.name = req.body.secondarySchool.name;
                secondarySchool.student = req.body.secondarySchool.student;
                secondarySchool.courseInfo = req.body.secondarySchool.courseInfo;
                secondarySchool.save(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.status(201).json({secondarySchool: secondarySchool});
                });
            });
        })
        .patch(function (req, res, next) {
            SecondarySchoolModel.findById(req.params.id, function (err, secondarySchool) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                secondarySchool.name = req.body.secondarySchool.name;
                secondarySchool.student = req.body.secondarySchool.student;
                secondarySchool.courseInfo = req.body.secondarySchool.courseInfo;
                secondarySchool.save(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.status(201).json({secondarySchool: secondarySchool});
                });
            });
        })
        .delete(function (req, res, next) {
            SecondarySchoolModel.findById(req.params.id, function (err, secondarySchool) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                var tmp = secondarySchool;
                SecondarySchoolModel.remove({_id: req.params.id}, function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.json({secondarySchool: tmp});
                });
            });
        });
};
