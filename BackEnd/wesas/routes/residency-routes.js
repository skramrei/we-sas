/*
 This file was created to refactor all the residency route definitions
 out of server.js. It takes an instance of express() and mounts routes
 onto it, so the caller (server.js) should pass a reference to the
 instance of express() used for the server. It also takes a reference
 to the MongoDB model for the residencies collection so it can do
 the appropriate DB operations in responding to requests.
 */

module.exports = function (app, ResidencyModel) {

    app.route('/residencies')
            .get(function (req, res, next) {
                if (req.query.students) {
                    ResidencyModel.find({students: req.query.student}, function (err, residency) {
                        if (err) {
                            res.status(404).json({error: err});
                            return next(err);
                        }
                        res.json({residency: residency});
                    });
                } else {
                    ResidencyModel.find(function (err, residency) {
                        if (err) {
                            res.status(404).json({error: err});
                            return next(err);
                        }
                        res.json({residency: residency});
                    });
                }
            })
            .post(function (req, res, next) {
                var residency = new ResidencyModel({
                    name: req.body.residency.name,
                    students: req.body.residency.students
                });
                residency.save(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.status(201).json({residency: residency});
                });
            })
            .delete(function (req, res, next) {
                ResidencyModel.remove(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.json({});
                });
            });


    app.route('/residencies/:id')
            .get(function (req, res, next) {
                ResidencyModel.findById(req.params.id, function (err, residency) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    res.json({residency: residency});
                });
            })
            .put(function (req, res, next) {
                ResidencyModel.findById(req.params.id, function (err, residency) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    residency.name = req.body.residency.name;
                    residency.students = req.body.residency.students;
                    residency.save(function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.status(201).json({residency: residency});
                    });
                });
            })
            .patch(function (req, res, next) {
                ResidencyModel.findById(req.params.id, function (err, residency) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    residency.name = req.body.residency.name;
                    residency.students = req.body.residency.students;
                    residency.save(function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.status(201).json({residency: residency});
                    });
                })
            })
            .delete(function (req, res, next) {
                ResidencyModel.findById(req.params.id, function (err, residency) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    var tmp = residency;
                    ResidencyModel.remove({_id: req.params.id}, function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.json({residency: tmp});
                    });
                });
            });
};
