module.exports = function (app, LogicalExpressionModel) {
	app.route('/logicalExpressions')
	    .get(function (req, res, next) {
			if (req.query.rule) {
				// Looking for the expressions associated with a particular rule
				// We only reuturn the first links in each chain
				LogicalExpressionModel.find({ rule: req.query.rule, isFirst: true }, (err, logicalExpressions) => {
					if (err) {
						res.status(404).json({ "error": err });
						return next(err);
					}
					res.json({ "logicalExpression": logicalExpressions });
				});
			} else {
				// Looking for all the logical expressions
				LogicalExpressionModel.find(function (err, logicalExpressions) {
		            if (err) {
						res.status(404).json({ "error": err });
						return next(err);
		            }
		            res.json({ "logicalExpression": logicalExpressions });
	        	});
			}
	    })
	    .post(function (req, res, next) {
			// Ember links recursive relationships both ways so this will make sure only the base expression gets the link
			if (req.body.logicalExpression.logicalLink === 'None') {
				req.body.logicalExpression.link = null;
			}
	        var newExpression = new LogicalExpressionModel({
	            "booleanExp": req.body.logicalExpression.booleanExp,
				"logicalLink": req.body.logicalExpression.logicalLink,
				"link": req.body.logicalExpression.link,
				"rule": req.body.logicalExpression.rule,
				"isFirst": req.body.logicalExpression.isFirst,
				"ifFull": req.body.logicalExpression.ifFull,
	        });
	        newExpression.save(function (err) {
	            if (err) {
					res.status(500).json({ "error": err });
					return next(err);
	            }
	            res.json({ "logicalExpression": newExpression });
	        });
	    })
	    .delete(function(req, res, next) {
	        LogicalExpressionModel.remove(function (err) {
	            if (err) {
					res.status(500).json({ "error": err });
					return next(err);
	            }
	            res.json({});
	        });
	    });


	app.route('/logicalExpressions/:id')
	    .get(function (req, res, next) {
	        LogicalExpressionModel.findById(req.params.id, function (err, logicalExpression) {
	            if (err) {
					res.status(404).json({ "error": err });
					return next(err);
	            }
	            res.json({ "logicalExpression": logicalExpression });
	        });
	    })
	    .put(function (req, res, next) {
	        LogicalExpressionModel.findById(req.params.id, function (err, logicalExpressionToModify) {
	            if (err) {
					res.status(404).json({ "error": err });
					return next(err);
	            }
	            logicalExpressionToModify.booleanExp = req.body.logicalExpression.booleanExp;
				logicalExpressionToModify.logicalLink = req.body.logicalExpression.logicalLink;
				logicalExpressionToModify.link = req.body.logicalExpression.link;
				logicalExpressionToModify.rule = req.body.logicalExpression.rule;
				logicalExpressionToModify.isFirst = req.body.logicalExpression.isFirst;
				logicalExpressionToModify.ifFull = req.body.logicalExpression.ifFull;
	            logicalExpressionToModify.save(function (err) {
	                if (err) {
						res.status(500).json({ "error": err });
						return next(err);
	                }
	                res.status(201).json({ "logicalExpression": logicalExpressionToModify });
	            });
	        });
	    })
	    .patch(function (req, res, next) {
	        LogicalExpressionModel.findById(req.params.id, function (err, logicalExpressionToModify) {
	            if (err) {
					res.status(404).json({ "error": err });
					return next(err);
	            }
                logicalExpressionToModify.booleanExp = req.body.logicalExpression.booleanExp;
				logicalExpressionToModify.logicalLink = req.body.logicalExpression.logicalLink;
				logicalExpressionToModify.link = req.body.logicalExpression.link;
				logicalExpressionToModify.rule = req.body.logicalExpression.rule;
				logicalExpressionToModify.isFirst = req.body.logicalExpression.isFirst;
				logicalExpressionToModify.ifFull = req.body.logicalExpression.ifFull;
	            logicalExpressionToModify.save(function (err) {
	                if (err) {
						res.status(500).json({ "error": err });
						return next(err);
	                }
	                res.status(201).json({ "logicalExpression": logicalExpressionToModify });
	            });
	        });
	    })
	    .delete(function (req, res, next) {
	        LogicalExpressionModel.findById(req.params.id, function (err, logicalExpressionToDelete) {
	            if (err) {
					res.status(404).json({ "error": err });
					return next(err);
	            }
	            var removedExpression = logicalExpressionToDelete;
	            LogicalExpressionModel.remove({ "_id": req.params.id }, function (err) {
	                if (err) {
						res.status(500).json({ "error": err });
						return next(err);
	                }
	                res.json({ "logicalExpression": removedExpression });
	            });
	        });
	    });
};
