//
// Created by stefan on 2016-03-12 11:48 AM
//

module.exports = function (app, RoleCodeModel) {

    app.route('/roleCodes')
            .post(function (request, response, next) {
                var roleCode = new RoleCodeModel(request.body.roleCode);
                roleCode.save(function (error) {
                    if (error) {
                        response.status(500).json({ error: error });
                        return next(error);
                    }
                    else {
                        response.json({roleCode: roleCode});
                    }
                });
            })
            .get(function (request, response, next) {
                RoleCodeModel.find(function (error, codes) {
                    if (error) {
                        response.status(404).json({ error: error });
                        return next(error);
                    }
                    else {
                        response.json({roleCode: codes});
                    }
                });
            });

    app.route('/roleCodes/:id')
            .get(function (request, response, next) {
                RoleCodeModel.findById(request.params.id, function (error, role) {
                    if (error) {
                        response.status(404).json({ error: error });
                        return next(error);
                    }
                    else {
                        response.json({roleCode: role});
                    }
                });
            })
            .put(function (request, response, next) {
                RoleCodeModel.findById(request.params.id, function (error, role) {
                    if (error) {
                        response.status(500).json({ error: error });
                        return next(error);
                    }
                    else {
                        // update the role info
                        role.name = request.body.roleCode.name;
                        role.userRoles = request.body.roleCode.userRoles;
                        role.functions = request.body.roleCode.functions;

                        role.save(function (error) {
                            if (error) {
                                response.status(500).json({ error: error });
                                return next(error);
                            }
                            else {
                                response.json({roleCode: role});
                            }
                        });
                    }
                });
            })
            .delete(function (request, response, next) {
                RoleCodeModel.findByIdAndRemove(request.params.id,
                        function (error, deleted) {
                            if (error) {
                                response.status(404).json({ error: error });
                                return next(error);
                            }
                            else {
                                response.json({roleCode: deleted});
                            }
                        }
                );
            });
};
