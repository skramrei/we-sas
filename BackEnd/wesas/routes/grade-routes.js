/*
 This file was created to refactor all the grade route definitions
 out of server.js. It takes an instance of express() and mounts routes
 onto it, so the caller (server.js) should pass a reference to the
 instance of express() used for the server. It also takes a reference
 to the MongoDB model for the grades collection so it can do
 the appropriate DB operations in responding to requests.
 */

module.exports = function (app, GradeModel) {

    app.route('/grades')
        .get(function (req, res, next) {
            if (req.query.student) {
                GradeModel.find({student: req.query.student}, function (err, grades) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    res.json({grade: grades});
                });
            }
            else if (req.query.id) {
                GradeModel.findById(req.query.id, function (err, grade) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    res.json({grade: grade});
                });
            } else {
                GradeModel.find(function (err, grade) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    res.json({grade: grade});
                });
            }
        })
        .post(function (req, res, next) {
            var grade = new GradeModel({
                mark: req.body.grade.mark,
                section: req.body.grade.section,
                courseCode: req.body.grade.courseCode,
                programRecord: req.body.grade.programRecord,
                student: req.body.grade.student
            });
            grade.save(function (err) {
                if (err) {
                    res.status(500).json({error: err});
                    return next(err);
                }
                res.json({grade: grade});
            });
        })
        .delete(function (req, res, next) {
            GradeModel.remove(function (err) {
                if (err) {
                    res.status(500).json({error: err});
                    return next(err);
                }
                res.json({});
            });
        });


    app.route('/grades/:id')
        .get(function (req, res, next) {
            GradeModel.findById(req.params.id, function (err, grade) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                res.json({grade: grade});
            });
        })
        .put(function (req, res, next) {
            GradeModel.findById(req.params.id, function (err, grade) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                grade.mark = req.body.grade.mark;
                grade.section = req.body.grade.section;
                grade.courseCode = req.body.grade.courseCode;
                grade.programRecord = req.body.grade.programRecord;
                grade.student = req.body.grade.student;
                grade.save(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.json({grade: grade});
                });
            });
        })
        .patch(function (req, res, next) {
            GradeModel.findById(req.params.id, function (err, grade) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                grade.mark = req.body.grade.mark;
                grade.section = req.body.grade.section;
                grade.courseCode = req.body.grade.courseCode;
                grade.programRecord = req.body.grade.programRecord;
                grade.student = req.body.grade.student;
                grade.save(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.json({grade: grade});
                });
            });
        })
        .delete(function (req, res, next) {
            GradeModel.findById(req.params.id, function (err, grade) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                var tmp = grade;
                GradeModel.remove({_id: req.params.id}, function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.json({grade: tmp});
                });
            });
        });
};
