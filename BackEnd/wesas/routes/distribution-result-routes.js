/*
	This file was created to refactor all the country route definitions
	out of server.js. It takes an instance of express() and mounts routes
	onto it, so the caller (server.js) should pass a reference to the
	instance of express() used for the server. It also takes a reference
	to the MongoDB model for the countries collection so it can do
	the appropriate DB operations in responding to requests.
*/

function distributeStudent(student, programs, courses, rejectedStudents) {
	var eligible = false;
	for (var j = 0; j < student.itrPrograms.length; j++) { // for each of their choices

		var tempProg = null;
		var tempRule = null; // better readability than x[i].y[j].z[k].rule
		eligible = false;
		for (var k = 0; k < programs.length; k++) { // find program and rule

			if (programs[k].equals(student.itrPrograms[j].program)) {
				tempProg = programs[k];
				tempRule = programs[k].rule;
				break; // no need to keep searching
			} // end if
		} // end program/rule for

		if (tempRule === null) continue; // program has no rule

		eligible = checkRule(student, tempRule, tempProg, courses);

		if (eligible === true) {

			if (tempProg.reachedCap === true) {
				// If we get here student must pass all rules but program is full.
				// If student passes all rules then they can go into that program as long as someone who has already
				// been accepted does not pass the extra rules.
				// Here is where student Arrays in program come into play: we may need to reshuffle the current program

				var doubleChecked = true;
				var found = false;
				var toBeRedistributed = null; // person who might get moved into another program
				// let's iterate through from low to high averages and check if these students meet the extra rules
				for (var k = 0; k < tempProg.students.length; k++) {

					if (tempProg.students[k].average > student.average) break;
					// should never be less than so only equal averages will need consideration
					// higher average wins even if they don't pass extra rules
					// and since they are sorted by average (low to high), if the first one has a higher average, they all do

					doubleChecked = checkRule(tempProg.students[k], tempRule, tempProg, courses); // now that cap has been reached, all rules are checked
					if (doubleChecked === false) { // does not meet first priority conditions
						toBeRedistributed = tempProg.students[k]; // keep track of person getting replaced
						tempProg.students[k] = student; // replace
						found = true;
						break; // no need to keep looking
					}
				} // end for

				if (found == true)  { // redistribution must occur
					tempProg.students.sort(function(a,b) { // sort this by average low to high
				  		return parseFloat(a.average) - parseFloat(b.average);
					});
					distributeStudent(toBeRedistributed, programs, courses, rejectedStudents); // redistribute that person
				} else continue; // check next choice

			} else {
				tempProg.students.push(student);
				tempProg.students.sort(function(a,b) { // sort this by average low to high
			  		return parseFloat(a.average) - parseFloat(b.average);
				});
			}

			if (tempProg.students.length === tempProg.classSize)
				tempProg.reachedCap = true;

			break; // go to next student
		} // else check next choice
	} // end itr for

	if (eligible === false) {
		rejectedStudents.push(student); // keep track of those not accepted into any program
	}
}

function checkRule(student, tempRule, tempProg, courses) {
	// now it's time to check if the student passes the rule's expressions
	/* protocol:
	*	start w/ an expression, send to function (checks type and if student passes)
	*	if passes check logical link, else next choice
	*	if null or OR: already passed, check next expression
	*	if AND: check that one, repeat
	*/
	var eligible = false;
	var doneRule = false;
	var skipThis = false; // certain expressions may be skipped in certain cases

	for (var k = 0; k < tempRule.testExpressions.length; k++) {
		eligible = false;
		doneRule = false;
		skipThis = false;
		var totalRule = ""; // for linked expressions
		var tempExp = tempRule.testExpressions[k];
		var logExp = tempExp.booleanExp;

		if (logExp === null) continue;
		if (tempExp.isFirst === false || (tempExp.ifFull === true && tempProg.reachedCap === false)) {
			// this is a linked expression which will be checked differently
			// or an expression that only makes a difference once the program cap is reached
			skipThis = true;
		}

		if (skipThis === false) { // actually analyze it
			while (true) { // go through linked expressions
				var parsedExp = logExp.split(' ');
				totalRule += checkExp(student, tempProg, courses, logExp, parsedExp);
				if (tempExp.logicalLink === "None") { // evaluate and return
					var passed = eval(totalRule);
					if (passed === false) {
						doneRule = true;
					}
					break; // done this chain of expressions, move on to the next one.
				} else if (tempExp.logicalLink === "...And") { // link &&
					totalRule += "&&";
				} else if (tempExp.logicalLink === "...Or") { // link ||
					totalRule += "||";
				}
				for (var l = 0; l < tempRule.testExpressions.length; l++) { // move on to next linked expression
					if (tempExp.link.equals(tempRule.testExpressions[l]._id)) {
						tempExp = tempRule.testExpressions[l];
						logExp = tempExp.booleanExp;
						break; // leave for, continue in while
					}
				} // end for

			} // end while
		} // end if

		if (doneRule === true) {
			break;
		}

		if (k === tempRule.testExpressions.length - 1) { // got here and never broke out so passed all expressions (is eligible)
			eligible = true;
			break;
		}

	} // end expression for

	return eligible;
}

function checkExp(student, program, courses, logExp, parsedExp) {
	var evalExp;
	if (logExp.includes("size")) { // if is based on class size

		passed = true; // no matter what, there is a check for cap somewhere else that handles this.

	} else if (logExp.includes("average")) { // if is based student average
		evalExp = logExp.replace("average",student.average);

	} else { // based on mark in a single course
		var courseId = parsedExp[0];
		var tempGrade = -1;

		for (var i = 0; i < student.grades.length; i++) { // find the right grade and course combination
			if (student.grades[i].courseCode !== null && student.grades[i].courseCode.equals(courseId)) {
				tempGrade = student.grades[i].mark;
				break;
			}
		}

		evalExp = logExp.replace(courseId,parseFloat(tempGrade));

	}
	return evalExp;
}

function createDistributionResult (CommentCodeModel, DistributionResultModel, program, studentID, date) {

	return new Promise(function (resolve, reject) {
		CommentCodeModel.findById(program.commentCode, function (err, commentCode) {
			var result = new DistributionResultModel({
				date: date,
				student: studentID,
				commentCode: commentCode._id,
				rejected: false
			}).save(function (err) {
				if (err) {
					reject();
					res.status(404).json({ error: err });
					return next(err);
				}
				resolve();
			});
		});
	});

}

function returnResultByDate (DistributionResultModel, res, date) {
	DistributionResultModel.find({ date: date }, function (err, results) {
		if (err) {
			res.status(404).json({ error: err });
			return next(err);
		}
		res.json({ distributionResult: results});
	});
}

function returnResultByDateAndStudent (DistributionResultModel, res, date, student) {
	DistributionResultModel.find({ date: date, student: student }, function (err, results) {
		if (err) {
			res.status(404).json({ error: err });
			return next(err);
		}
		res.json({ distributionResult: results});
	});
}

module.exports = function (app, DistributionResultModel, DistributionInfoModel, StudentModel, GradeModel,
	CourseCodeModel, AcademicProgramCodeModel, ITRProgramModel, AdmissionRuleModel, LogicalExpressionModel, CommentCodeModel) {

	app.route('/distributionResults')
	    .get(function (req, res, next) {
	    	if (req.query.generate) {

	    		// Generate distribution results

				const CURRENT_DATE = new Date().toLocaleString();

                StudentModel.find(function (err, students) {
	            if (err) {
					res.status(404).json({ "error": err });
					return next(err);
	            }

		            GradeModel.find(function (err, grades) {
		            if (err) {
						res.status(404).json({ "error": err });
						return next(err);
		            }

		            CourseCodeModel.find(function (err, courses) {
		            if (err) {
						res.status(404).json({ "error": err });
						return next(err);
		            }

		            AcademicProgramCodeModel.find(function (err, programs) {
			            if (err) {
							res.status(404).json({ "error": err });
							return next(err);
			            }

			            ITRProgramModel.find(function (err, itr) {
				            if (err) {
								res.status(404).json({ "error": err });
								return next(err);
				            }

							for (var i = 0; i < itr.length; i++) { // if we're deleting all previous distributions then let's set eligibilities back to false
				            	itr[i].eligibility = false;
								itr[i].save(function (err) {
									if (err) {
										res.status(404).json({ error: err });
										return next(err);
									}
								});
				            }

				            AdmissionRuleModel.find(function (err, rules) {
					            if (err) {
									res.status(404).json({ "error": err });
									return next(err);
					            }

					            LogicalExpressionModel.find(function (err, expressions) {
						            if (err) {
										res.status(404).json({ "error": err });
										return next(err);
						            }

									var gradeSum, creditCount, tempCourse;
									var rejectedStudents = [];

									// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~// basic set up //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

									for (var i = 0; i < students.length; i++) { // populates student's grades w/ their grades
										// and itrPrograms w/ their itrPrograms and sorts them
										students[i].grades = grades.filter(function (grade) {
											return (grade.student.equals(students[i]._id));
										});

										students[i].itrPrograms = itr.filter(function (itrP) {
											return (itrP.student.equals(students[i]._id));
										});

										students[i].itrPrograms.sort(function(a,b) { // 1 to 10
									  		return parseFloat(a.order) - parseFloat(b.order);
										});

										gradeSum = 0; // reset variables
										creditCount = 0;
										tempCourse = null;
									    for (var sg=0; sg<students[i].grades.length;sg++) { // find cumulative average
									    	for (var c = 0; c < courses.length; c++) { // find correct course
									        	if (students[i].grades[sg].courseCode !== null && students[i].grades[sg].courseCode.equals(courses[c]._id)) {
									        		tempCourse = courses[c];
									        	}
									   		}
									   		if (tempCourse === null) continue;
									   		gradeSum += parseFloat(tempCourse.unit) * parseFloat(students[i].grades[sg].mark);
									   		creditCount += parseFloat(tempCourse.unit);
									   }

									   students[i].average = gradeSum / creditCount;
									}

									// sorting of students by average (high to low)
									students.sort(function(a,b) {
									  return parseFloat(b.average) - parseFloat(a.average);
									});

									// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~// Find Max class sizes //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
									//class size default 1000 (i.e. no cap) but can change later
									// also I'm keeping track of an array of arrays for the students considered for each program

									for (var i=0; i<programs.length;i++) { // assign rule and default class size
										programs[i].rule = rules.filter(function (rule) {
											return (programs[i].rule.equals(rule._id));
										});
									    programs[i].classSize = 1000;
									    programs[i].reachedCap = false;
									    programs[i].students = [];
									}


									for (var i = 0; i < rules.length; i++) { // populates rule's expressions with the correct expressions
										rules[i].testExpressions = expressions.filter(function (expo) {
											return (expo.rule.equals(rules[i]._id));
										});
									}


									var tempRule;
									var count = 0;

									for (var i=0; i < programs.length;i++) { // find program cap
									  // size is only mentioned in the rule so following loop
									  // is to find the rule associated with size
										// first find the corresponding rule
										tempRule = null;
										for (var j = 0; j < rules.length; j++) {
											if (programs[i].rule !== null && programs[i].rule.equals(rules[j]._id)) {
												programs[i].rule =  rules[j]; // assign to program
												tempRule = rules[j];
											}
										}

										if (tempRule === null) { // i.e. not found
											continue;
										}

										for (var j=0; j<tempRule.testExpressions.length;j++) { // go through each expression
											if (tempRule.testExpressions[j].booleanExp !== null && tempRule.testExpressions[j].booleanExp.includes("size")) { // check if this is the appropriate exp.
										        //splits expression to get size by itself
										        var findNumber = tempRule.testExpressions[j].booleanExp.split(" ");

										        programs[i].classSize = parseFloat(findNumber[2]);
										        break;
									    	} // end outer if
								   		} // end expressions for loop
									} // end program cap for loop

									/*~~~~~~~~~~~~~~~~~~~~~~~~~~~START DISTRIBUTION~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
									for (var i = 0; i < students.length; i++) { // for each student
										distributeStudent(students[i], programs, courses, rejectedStudents);
									} // end student for

									// FINAL STEPS: Create the actual distribution results and comment codes,
									// set eligibilities in itrPrograms

									var distributionResultsToMake = students.length;
									var distributionResultsMade = 0;


									for (var i = 0; i < programs.length; i++) {

										for (var j = 0; j < programs[i].students.length; j++) {
											// NOTE: Temporary persistance of values while iterating through all students
											// NOTE: Eligability is already false by default so we just need to set true for the itr program they got accepted to
											studentItrPrograms = itr.filter(function (item) {
												return item.student.equals(programs[i].students[j]._id);
											});
											eligibleItrProgram = studentItrPrograms.filter(function (item) {
												return item.program.equals(programs[i]._id);
											});

                                            // We're not returing this we don't worry about handling fulfillment
                                            ITRProgramModel.findById(eligibleItrProgram[0]._id, function (err, model) {
                                                model.eligibility = true;
												model.save(function (err) {
													if (err) {
														res.status(404).json({ error: err });
														return next(err);
													}
												});
                                            });

											createDistributionResult(CommentCodeModel, DistributionResultModel, programs[i], programs[i].students[j]._id, CURRENT_DATE)
												.then(() => {
													distributionResultsMade++;
												});

										}

									}

									for (var i = 0; i < rejectedStudents.length; i++) {

										var result = new DistributionResultModel({
											date: CURRENT_DATE,
											student: rejectedStudents[i]._id,
											rejected: true
										}).save(function (err) {
											if (err) {
												res.status(404).json({ error: err });
												return next(err);
											}
											distributionResultsMade++;
										});
									}

									// Need to wait until all the distribution results are made
									returnInterval = setInterval(function () {

										if (distributionResultsMade === distributionResultsToMake) {
											// Time to send response
											// Now that all of the distribution results are made we can return them
											// Also need to update the Distribution info
											clearInterval(returnInterval);
											DistributionInfoModel.findOne({ id: 1 }, function (err, info) {
												if (err) {
													res.status(404).json({ error: err });
													return next(err);
												}

												info.date = CURRENT_DATE;
												info.dates.push(CURRENT_DATE);
												info.save(function (err) {
													if (err) {
														res.status(404).json({ error: err });
														return next(err);
													}
													DistributionResultModel.find({ date: CURRENT_DATE }, function (err, results) {
														if (err) {
															res.status(404).json({ error: err });
															return next(err);
											            }
														if (!res.headersSent) {
															// This may get called before the interval is actaully cleared so only send response if it hasent been sent
															res.json({ distributionResult: results});
														}
													});
												});
											});
										}
									}, 50);

									}); // end expression stuff
									}); // end rule stuff
	    						}); // end itrProgram stuff
				    		}); // end program stuff
						}); // end courses stuff
					}); // end grades stuff
	        	}); // end student stuff


	        } else if (req.query.date && !req.query.student) {

				// Date can either be an actual date or the word 'latest'
				if (req.query.date === 'latest') {
					DistributionInfoModel.findOne({ id: 1 }, function (err, info) {
						if (err) {
							res.status(404).json({ error: err });
							return next(err);
						}
						returnResultByDate(DistributionResultModel, res, info.date);
					});
				} else {
					returnResultByDate(DistributionResultModel, res, req.query.date);
				}

			} else if (req.query.date && req.query.student) {

				// Date can either be an actual date or the word 'latest'
				if (req.query.date === 'latest') {
					DistributionInfoModel.findOne({ id: 1 }, function (err, info) {
						if (err) {
							res.status(404).json({ error: err });
							return next(err);
						}
						returnResultByDateAndStudent(DistributionResultModel, res, info.date, req.query.student);
					});
				} else {
					returnResultByDateAndStudent(DistributionResultModel, res, req.query.date, req.query.student);
				}

			} else if (req.query.student) {

	        	DistributionResultModel.find({ student: req.query.student }, function (err,results) {
		            if (err) {
						res.status(404).json({ error: err });
						return next(err);
		            }
		            res.json({ distributionResult: results});
	        	});

	        } else if (req.query.id) {

				DistributionResultModel.findById(req.query.id, function (err, results) {
					if (err) {
						res.status(404).json({ error: err });
						return next(err);
					}
					res.json({ distributionResult: results });
				});

			} else {

	        	// No QS so find all distribution results
	        	DistributionResultModel.find(function (err, results) {
		            if (err) {
						res.status(404).json({ error: err });
						return next(err);
		            }
		            res.json({ distributionResult: results});
	        	});

	        }
	    })
	    .post(function (req, res, next) {
	        var distLoad = new DistributionResultModel({
	        	date: req.body.distributionResult.date,
	            commentCodes: req.body.distributionResult.commentCodes,
				student: req.body.distributionResult.student
	        });
	        distLoad.save(function (err) {
	            if (err) {
					res.status(500).json({ error: err });
					return next(err);
	            }
	            res.json({ distributionResult: distLoad });
	        });
	    })
	    .delete(function(req, res, next) {


            ITRProgramModel.find(function (err, itr) {
	            if (err) {
					res.status(404).json({ "error": err });
					return next(err);
	            }
	            for (var i = 0; i < itr.length; i++) { // if we're deleting all previous distributions then let's set eligibilities back to false
	            	itr[i].eligibility = false;
					itr[i].save(function (err) {
						if (err) {
							res.status(404).json({ error: err });
							return next(err);
						}
					});
	            }
	        });
	        DistributionResultModel.remove(function (err) {
	            if (err) {
					res.status(500).json({ error: err });
					return next(err);
	            }
	            res.json({});
	        });
	    });


	app.route('/distributionResults/:id')
	    .get(function (req, res, next) {
	        DistributionResultModel.findById(req.params.id, function (err, distLoad) {
	            if (err) {
					res.status(404).json({ error: err });
					return next(err);
	            }
	            res.json({ distributionResult: distLoad });
	        });
	    })
	    .put(function (req, res, next) {
	        DistributionResultModel.findById(req.params.id, function (err, distLoadToModify) {
	            if (err) {
	                res.status(404).json({ error: err });
					return next(err);
	            }
	            disLoadToModify.date = req.body.distributionResult.name;
				      disLoadToModify.commentCodes = req.body.distributionResult.commentCodes;
				      disLoadToModify.students = req.body.distributionResult.students;
	            disLoadToModify.save(function (err) {
	                if (err) {
						res.status(500).json({ error: err });
						return next(err);
	                }
	                res.status(201).json({ distributionResult: distLoadToModify });
	            });
	        });
	    })
	    .patch(function (req, res, next) {
	        DistributionResultModel.findById(req.params.id, function (err, distLoad) {
	            if (err) {
					res.status(404).json({ "error": err });
					return next(err);
	            }
                  disLoad.date = req.body.distributionResult.name;
				          disLoad.commentCodes = req.body.distributionResult.commentCodes;
				          disLoad.students = req.body.distributionResult.students;
	            distLoadToModify.save(function (err) {
	                if (err) {
						res.status(500).json({ error: err });
						return next(err);
	                }
	                res.status(201).json({ distributionResult: distLoadToModify });
	            });
	        });
	    })
	    .delete(function (req, res, next) {
	        DistributionResultModel.findById(req.params.id, function (err, distLoad) {
	            if (err) {
					res.status(404).json({ error: err });
					return next(err);
	            }
	            var removedDistributionLoad = distLoad;
	            DistributionResultModel.remove({ _id: req.params.id }, function (err) {
	                if (err) {
						res.status(500).json({ error: err });
						return next(err);
	                }
	                res.json({ distributionResult: removedDistributionLoad });
	            });
	        });
	    });
}
