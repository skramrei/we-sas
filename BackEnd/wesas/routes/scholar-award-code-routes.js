
module.exports = function (app, ScholarAwardCodeModel) {

    app.route('/scholarAwardCodes')
            .get(function (req, res, next) {
                if (req.query.students) {
                    ScholarAwardCodeModel.find({students: req.query.student}, function (err, scholarAwardCode) {
                        if (err) {
                            res.status(404).json({error: err});
                            return next(err);
                        }
                        res.json({scholarAwardCode: scholarAwardCode});
                    });
                } else {
                    ScholarAwardCodeModel.find(function (err, scholarAwardCode) {
                        if (err) {
                            res.status(404).json({error: err});
                            return next(err);
                        }
                        res.json({scholarAwardCode: scholarAwardCode});
                    });
                }
            })
            .post(function (req, res, next) {
                var scholarAwardCode = new ScholarAwardCodeModel({
                    name: req.body.scholarAwardCode.name,
                    students: req.body.scholarAwardCode.students
                });
                scholarAwardCode.save(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.json({scholarAwardCode: scholarAwardCode});
                });
            })
            .delete(function (req, res, next) {
                ScholarAwardCodeModel.remove(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.json({});
                });
            });


    app.route('/scholarAwardCodes/:id')
            .get(function (req, res, next) {
                ScholarAwardCodeModel.findById(req.params.id, function (err, scholarAwardCode) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    res.json({scholarAwardCode: scholarAwardCode});
                });
            })
            .put(function (req, res, next) {
                ScholarAwardCodeModel.findById(req.params.id, function (err, scholarAwardCode) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    scholarAwardCode.name = req.body.scholarAwardCode.name;
                    scholarAwardCode.students = req.body.scholarAwardCode.students;
                    scholarAwardCode.save(function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.status(201).json({scholarAwardCode: scholarAwardCode});
                    });
                });
            })
            .patch(function (req, res, next) {
                ScholarAwardCodeModel.findById(req.params.id, function (err, scholarAwardCode) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    scholarAwardCode.name = req.body.scholarAwardCode.name;
                    scholarAwardCode.students = req.body.scholarAwardCode.students;
                    scholarAwardCode.save(function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.status(201).json({scholarAwardCode: scholarAwardCode});
                    });
                });
            })
            .delete(function (req, res, next) {
                ScholarAwardCodeModel.findById(req.params.id, function (err, scholarAwardCode) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    var tmp = scholarAwardCode;
                    ScholarAwardCodeModel.remove({_id: req.params.id}, function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.json({scholarAwardCode: tmp});
                    });
                });
            });
};
