/*
 This file was created to refactor all the country route definitions
 out of server.js. It takes an instance of express() and mounts routes
 onto it, so the caller (server.js) should pass a reference to the
 instance of express() used for the server. It also takes a reference
 to the MongoDB model for the countries collection so it can do
 the appropriate DB operations in responding to requests.
 */

module.exports = function (app, CountryModel) {

    app.route('/countries')
            .get(function (req, res, next) {
                if (req.query.students) {
                    CountryModel.find({students: req.query.student}, function (err, country) {
                        if (err) {
                            res.status(404).json({ error: err });
                            return next(err);
                        }
                        res.json({country: country});
                    });
                } else {
                    CountryModel.find(function (err, country) {
                        if (err) {
                            res.status(404).json({ error: err });
                            return next(err);
                        }
                        res.json({country: country});
                    });
                }
            })
            .post(function (req, res, next) {
                var country = new CountryModel({
                    name: req.body.country.name,
                    provinces: req.body.country.provinces,
                    students: req.body.country.students
                });
                country.save(function (err) {
                    if (err) {
                        res.status(500).json({ error: err });
                        return next(err);
                    }
                    res.json({country: country});
                });
            })
            .delete(function (req, res, next) {
                CountryModel.remove(function (err) {
                    if (err) {
                        res.status(500).json({ error: err });
                        return next(err);
                    }
                    res.json({});
                });
            });


    app.route('/countries/:id')
            .get(function (req, res, next) {
                CountryModel.findById(req.params.id, function (err, country) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    res.json({country: country});
                });
            })
            .put(function (req, res, next) {
                CountryModel.findById(req.params.id, function (err, country) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    country.name = req.body.country.name;
                    country.provinces = req.body.country.provinces;
                    country.students = req.body.country.students;
                    country.save(function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.status(201).json({country: country});
                    });
                });
            })
            .patch(function (req, res, next) {
                CountryModel.findById(req.params.id, function (err, country) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    country.name = req.body.country.name;
                    country.provinces = req.body.country.provinces;
                    country.students = req.body.country.students;
                    country.save(function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.status(201).json({country: country});
                    });
                });
            })
            .delete(function (req, res, next) {
                CountryModel.findById(req.params.id, function (err, country) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    var tmp = country;
                    CountryModel.remove({_id: req.params.id}, function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.json({country: tmp});
                    });
                });
            });
};
