/*
 This file was created to refactor all the gender route definitions
 out of server.js. It takes an instance of express() and mounts routes
 onto it, so the caller (server.js) should pass a reference to the
 instance of express() used for the server. It also takes a reference
 to the MongoDB model for the genders collection so it can do
 the appropriate DB operations in responding to requests.
 */

module.exports = function (app, GenderModel) {

    app.route('/genders')
            .get(function (req, res, next) {
                if (req.query.students) {
                    GenderModel.find({students: req.query.student}, function (err, gender) {
                        if (err) {
                            res.status(404).json({error: err});
                            return next(err);
                        }
                        res.json({gender: gender});
                    });
                } else {
                    GenderModel.find(function (err, gender) {
                        if (err) {
                            res.status(404).json({error: err});
                            return next(err);
                        }
                        res.json({gender: gender});
                    });
                }
            })
            .post(function (req, res, next) {
                var gender = new GenderModel({
                    name: req.body.gender.name,
                    students: req.body.gender.students
                });
                gender.save(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.json({gender: gender});
                });
            })
            .delete(function (req, res, next) {
                GenderModel.remove(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.json({});
                });
            });


    app.route('/genders/:id')
            .get(function (req, res, next) {
                GenderModel.findById(req.params.id, function (err, gender) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    res.json({gender: gender});
                });
            })
            .put(function (req, res, next) {
                GenderModel.findById(req.params.id, function (err, gender) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    gender.name = req.body.gender.name;
                    gender.students = req.body.gender.students;
                    gender.save(function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.json({gender: gender});
                    });
                });
            })
            .patch(function (req, res, next) {
                GenderModel.findById(req.params.id, function (err, gender) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    gender.name = req.body.gender.name;
                    gender.students = req.body.gender.students;
                    gender.save(function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.status(201).json({gender: gender});
                    });
                });
            })
            .delete(function (req, res, next) {
                GenderModel.findById(req.params.id, function (err, gender) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    var tmp = gender;
                    GenderModel.remove({_id: req.params.id}, function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.json({gender: tmp});
                    });
                });
            });
};
