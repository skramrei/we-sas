/*
 This file was created to refactor all the province route definitions
 out of server.js. It takes an instance of express() and mounts routes
 onto it, so the caller (server.js) should pass a reference to the
 instance of express() used for the server. It also takes a reference
 to the MongoDB model for the programs collection so it can do
 the appropriate DB operations in responding to requests.
 */

module.exports = function (app, ProvinceModel) {

    app.route('/provinces')
            .get(function (req, res, next) {
                if (req.query.country) {
                    ProvinceModel.find({country: req.query.country}, function (err, province) {
                        if (err) {
                            res.status(404).json({error: err});
                            return next(err);
                        }
                        res.json({province: province});
                    });
                } else {
                    ProvinceModel.find(function (err, province) {
                        if (err) {
                            res.status(404).json({error: err});
                            return next(err);
                        }
                        res.json({province: province});
                    });
                }
            })
            .post(function (req, res, next) {
                var province = new ProvinceModel({
                    name: req.body.province.name,
                    cities: req.body.province.cities,
                    country: req.body.province.country,
                    students: req.body.province.students
                });
                province.save(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.json({province: province});
                });
            })
            .delete(function (req, res, next) {
                ProvinceModel.remove(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.json({});
                });
            });


    app.route('/provinces/:id')
            .get(function (req, res, next) {
                ProvinceModel.findById(req.params.id, function (err, province) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    res.json({province: province});
                });
            })
            .put(function (req, res, next) {
                ProvinceModel.findById(req.params.id, function (err, province) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    province.name = req.body.province.name;
                    province.cities = req.body.province.cities;
                    province.country = req.body.province.country;
                    province.students = req.body.province.students;
                    province.save(function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.status(201).json({province: province});
                    });
                });
            })
            .patch(function (req, res, next) {
                ProvinceModel.findById(req.params.id, function (err, province) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    province.name = req.body.province.name;
                    province.cities = req.body.province.cities;
                    province.country = req.body.province.country;
                    province.students = req.body.province.students;
                    province.save(function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.status(201).json({province: province});
                    });
                });
            })
            .delete(function (req, res, next) {
                ProvinceModel.findById(req.params.id, function (err, province) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    var tmp = province;
                    ProvinceModel.remove({_id: req.params.id}, function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.json({province: tmp});
                    });
                });
            });
};
