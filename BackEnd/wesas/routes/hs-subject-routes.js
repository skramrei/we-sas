module.exports = function (app, HSSubjectModel) {

    app.route('/hsSubjects')
        .get(function (req, res, next) {
            HSSubjectModel.find(function (err, subj) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                res.json({hsSubject: subj});
            });
        })
        .post(function (req, res, next) {
            var subj = new HSSubjectModel({
                name: req.body.hsSubject.name,
                description: req.body.hsSubject.description,
                marks: req.body.hsSubject.marks
            });
            subj.save(function (err) {
                if (err) {
                    res.status(500).json({error: err});
                    return next(err);
                }
                res.json({hsSubject: subj});
            });
        })
        .delete(function (req, res, next) {
            HSSubjectModel.remove(function (err) {
                if (err) {
                    res.status(500).json({error: err});
                    return next(err);
                }
                res.json({});
            });
        });


    app.route('/hsSubjects/:id')
        .get(function (req, res, next) {
            HSSubjectModel.findById(req.params.id, function (err, subj) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                res.json({hsSubject: subj});
            });
        })
        .put(function (req, res, next) {
            HSSubjectModel.findById(req.params.id, function (err, subj) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                subj.name = req.body.hsSubject.name;
                subj.description = req.body.hsSubject.description;
                subj.marks = req.body.hsSubject.marks;
                subj.save(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.status(201).json({hsSubject: subj});
                });
            });
        })
        .patch(function (req, res, next) {
            HSSubjectModel.findById(req.params.id, function (err, subj) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                subj.name = req.body.hsSubject.name;
                subj.description = req.body.hsSubject.description;
                subj.marks = req.body.hsSubject.marks;
                subj.save(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.status(201).json({hsSubject: subj});
                });
            });
        })
        .delete(function (req, res, next) {
            HSSubjectModel.findById(req.params.id, function (err, subj) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                var tmp = subj;
                HSSubjectModel.remove({_id: req.params.id}, function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.json({hsSubject: tmp});
                });
            });
        });
};
