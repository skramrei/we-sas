//
// Created by stefan on 2016-03-12 11:53 AM
//

module.exports = function (app, RolePermissionModel) {

    app.route('/rolePermissions')
            .post(function (request, response, next) {
                var rolePermission = new RolePermissionModel(request.body.rolePermission);
                rolePermission.save(function (error) {
                    if (error) {
                        response.status(500).json({ error: error });
                        return next(error);
                    }
                    response.json({rolePermission: rolePermission});
                });
            })
            .get(function (request, response, next) {
                var RoleCode = request.query.filter;
                if (!RoleCode) {
                    RolePermissionModel.find(function (error, rolePermissions) {
                        if (error) {
                            response.status(404).json({ error: error });
                            return next(error);
                        }
                        response.json({rolePermission: rolePermissions});
                    });
                } else {
                    RolePermissionModel.find({"roleCodes": RoleCode.roleCodes}, function (error, roleCodes) {
                        if (error) {
                            response.status(404).json({ error: error });
                            return next(error);
                        }
                        response.json({rolePermission: roleCodes});
                    });
                }
            });

    app.route('/rolePermissions/:id')
            .get(function (request, response, next) {
                RolePermissionModel.findById(request.params.id, function (error, rolePermission) {
                    if (error) {
                        response.status(404).json({ error: error });
                        return next(error);
                    }
                    response.json({rolePermission: rolePermission});
                })
            })
            .put(function (request, response, next) {
                RolePermissionModel.findById(request.params.id, function (error, rolePermission) {
                    if (error) {
                        response.status(404).json({ error: error });
                        return next(error);
                    }
                    else {
                        rolePermission.sysFeature = request.body.rolePermission.sysFeature;
                        rolePermission.roleCodes = request.body.rolePermission.roleCodes;
                        rolePermission.save(function (error) {
                            if (error) {
                                response.status(500).json({ error: error });
                                return next(error);
                            }
                            else {
                                response.json({rolePermission: rolePermission});
                            }
                        });
                    }
                })
            })
            .delete(function (request, response, next) {
                RolePermissionModel.findByIdAndRemove(request.params.id,
                        function (error, deleted) {
                            if (error) {
                                response.status(500).json({ error: error });
                                return next(error);
                            }
                            response.json({rolePermission: deleted});
                        }
                );
            });
};
