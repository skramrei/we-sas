//
// Created by stefan on 2016-02-27 9:26 AM
//

module.exports = function (app, ProgramRecord) {

    app.route('/programRecords')
            .get(function (req, res, next) {
                if (req.query.degreeCode && req.query.termCode) {
                    ProgramRecord.find({
                        degreeCode: req.query.degreeCode,
                        termCode: req.query.termCode
                    }, function (err, programRecord) {
                        if (err) {
                            res.status(404).json({error: err});
                            return next(err);
                        }
                        res.json({programRecord: programRecord});
                    });
                } else {
                    ProgramRecord.find(function (err, programRecord) {
                        if (err) {
                            res.status(404).json({error: err});
                            return next(err);
                        }
                        res.json({programRecord: programRecord});
                    });
                }
            })
            .post(function (req, res, next) {
                var programRecord = new ProgramRecord({
                    level: req.body.programRecord.level,
                    status: req.body.programRecord.status,
                    comment: req.body.programRecord.comment,
                    degreeCode: req.body.programRecord.degreeCode,
                    termCode: req.body.programRecord.termCode,
                    grades: req.body.programRecord.grades
                });
                programRecord.save(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.json({programRecord: programRecord});
                });
            })
            .delete(function (req, res, next) {
                ProgramRecord.remove(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.json({});
                });
            });

    app.route('/programRecords/:id')
            .get(function (req, res, next) {
                ProgramRecord.findById(req.params.id, function (err, programRecord) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    res.json({programRecord: programRecord});
                });
            })
            .put(function (req, res, next) {
                ProgramRecord.findById(req.params.id, function (err, programRecord) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    programRecord.level = req.body.programRecord.level;
                    programRecord.status = req.body.programRecord.status;
                    programRecord.comment = req.body.programRecord.comment;
                    programRecord.degreeCode = req.body.programRecord.degreeCode;
                    programRecord.termCode = req.body.programRecord.termCode;
                    programRecord.grades = req.body.programRecord.grades;
                    programRecord.save(function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.status(201).json({programRecord: programRecord});
                    });
                });
            })
            .patch(function (req, res, next) {
                ProgramRecord.findById(req.params.id, function (err, programRecord) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    programRecord.level = req.body.programRecord.level;
                    programRecord.status = req.body.programRecord.status;
                    programRecord.comment = req.body.programRecord.comment;
                    programRecord.degreeCode = req.body.programRecord.degreeCode;
                    programRecord.termCode = req.body.programRecord.termCode;
                    programRecord.grades = req.body.programRecord.grades;
                    programRecord.save(function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.status(201).json({programRecord: programRecord});
                    });
                });
            })
            .delete(function (req, res, next) {
                ProgramRecord.findById(req.params.id, function (err, programRecord) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    var tmp = programRecord;
                    ProgramRecord.remove({_id: req.params.id}, function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.json({programRecord: tmp});
                    });
                });
            });
};
