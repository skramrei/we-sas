/*
	This file was created to refactor all the ITR Program route definitions
	out of server.js. It takes an instance of express() and mounts routes
	onto it, so the caller (server.js) should pass a reference to the
	instance of express() used for the server. It also takes a reference
	to the MongoDB model for the itrprograms collection so it can do
	the appropriate DB operations in responding to requests.
*/

module.exports = function (app, ITRProgramModel, StudentModel, AcademicProgramCodeModel, DistributionResultModel, CommentCodeModel, DistributionInfoModel) {

    function shuffle(array) {
		var currentIndex = array.length, temporaryValue, randomIndex;
		// While there remain elements to shuffle...

		while (0 !== currentIndex) {

		    // Pick a remaining element...
		    randomIndex = Math.floor(Math.random() * currentIndex);
		    currentIndex -= 1;

		    // And swap it with the current element.
		    temporaryValue = array[currentIndex];
		    array[currentIndex] = array[randomIndex];
		    array[randomIndex] = temporaryValue;
		}

		return array;
	}

	app.route('/itrPrograms')
	    .get(function (req, res, next) {
	    	if (req.query.generate) {
	    		// Generate random itrLists
	    		var rankings = [];
                //var newProgramSavedPromises = [new Promise(() => {})]; // Promise that will never be fulfilled
                var promiseNum = 0;

	    		StudentModel.find(function (err, students) {
		            if (err) {
						res.status(404).json({ "error": err });
						return next(err);
		            }

		            AcademicProgramCodeModel.find(function (err, programs) {
			            if (err) {
							res.status(404).json({ "error": err });
							return next(err);
			            }

                        // Fill array of promises that will never be fulfilled. They will be replace with actaul program save promises as they are created.
                        // This is nessesary becuase otherwise newProgramSavedPromises would be empty when control reaches the call to Promise.all which is like they were all fulfilled
                        /*
                        for (var i=0; i<(students.length*programs.length); i++) {
                            newProgramSavedPromises.push(new Promise(() => {}));
                        }
                        */
                        // Need to remove one extra promise
                        //newProgramSavedPromises.pop();

			            for (var i = 0; i < programs.length; i++) {
			            // this array must be the same size as the number of programs
		        			rankings[i] = i+1; // start at 1, not 0
		        		}

                        var currentPromiseIndex = 0;

						for (var i = 0; i < students.length; i++) {		// go through all students
							for (var j = 0; j < programs.length; j++) { // and assign all programs to an itrProgram

								var newProgram = new ITRProgramModel({
						            "order": rankings[j],				// with a randomized ranking order
									"eligibility": false,
									"student": students[i],
									"program": programs[j]
						        });
						        var oneSavePromise = newProgram.save(function (err) {
						            if (err) {
										res.status(500).json({ "error": err });
										return next(err);
						            }
						        });
                                oneSavePromise.then(() => {console.log('Promise ' + promiseNum++ + ' fulfilled.')});
                                //newProgramSavedPromises[currentPromiseIndex++] = oneSavePromise;
							}

							shuffle(rankings);

                            // Should response once programs have been generated for the last student
                            if (i === students.length-1) {
                                setTimeout(function () {
                                    ITRProgramModel.find(function (err, itrPrograms) {
                                    if (err) {
                                        res.status(404).json({ "error": err });
                                        return next(err);
                                    }
                                    res.json({ "itrProgram": itrPrograms});
                                    console.log('Random ITR Data generated.');
                                    });
                                }, 100);
                            }
						}

		        	}); // end program stuff
	        	}); // end student stuff

	    	} else if (req.query.clearAll) {
	    		// destroy all itrPrograms

		    	ITRProgramModel.remove(function (err) {
		            if (err) {
						res.status(500).json({ "error": err });
						return next(err);
		            }
		            res.json({});
		        });

		        console.log('ITR Data deleted.');

	    	} else if (req.query.student) {
	        	// Find itrProgram for student
	        	ITRProgramModel.find({ "student": req.query.student }, function (err, itrPrograms) {
		            if (err) {
						res.status(404).json({ "error": err });
						return next(err);
		            }
		            res.json({ "itrProgram": itrPrograms });
	        	});
	        } else if (req.query.id) {
				ITRProgramModel.findById(req.query.id, function (err, itrProgram) {
					if (err) {
						res.status(404).json({ "error": err });
						return next(err);
					}
					res.json({ "itrProgram": itrProgram });
				});
			} else {
	        	// No QS so find all itrPrograms
	        	ITRProgramModel.find(function (err, itrPrograms) {
		            if (err) {
						res.status(404).json({ "error": err });
						return next(err);
		            }
		            res.json({ "itrProgram": itrPrograms});
	        	});
	        }
	    })
	    .post(function (req, res, next) {
	        var newProgram = new ITRProgramModel({
	            "order": req.body.itrProgram.order,
				"eligibility": req.body.itrProgram.eligibility,
				"student": req.body.itrProgram.student,
				"program": req.body.itrProgram.program
	        });
	        newProgram.save(function (err) {
	            if (err) {
					res.status(500).json({ "error": err });
					return next(err);
	            }
	            res.json({ "itrProgram": newProgram });
	        });
	    })
	    .delete(function(req, res, next) {
	        ITRProgramModel.remove(function (err) {
	            if (err) {
					res.status(500).json({ "error": err });
					return next(err);
	            }
	            res.json({});
	        });
	    });


	app.route('/itrPrograms/:id')
	    .get(function (req, res, next) {
	        ITRProgramModel.findById(req.params.id, function (err, itrProgram) {
	            if (err) {
					res.status(404).json({ "error": err });
					return next(err);
	            }
	            res.json({ "itrProgram": itrProgram });
	        });
	    })
	    .put(function (req, res, next) {
	        ITRProgramModel.findById(req.params.id, function (err, itrProgramToModify) {
	            if (err) {
					res.status(404).json({ "error": err });
					return next(err);
	            }

                // If eligibility is being set to true then the distribution result for the student needs to be modified
                if (req.body.itrProgram.eligibility) {
					DistributionInfoModel.findOne({id: 1}, function (err, distributionInfo) {
						DistributionResultModel.findOne({student: itrProgramToModify.student, date: distributionInfo.date}, function (err, distributionResultToModify) {
	                        AcademicProgramCodeModel.findById(itrProgramToModify.program, function (err, programCode) {
								CommentCodeModel.findById(programCode.commentCode, function(err, commentCode) {
	                                distributionResultToModify.commentCode = commentCode._id;
	                                distributionResultToModify.rejected = false;
	                                distributionResultToModify.save();
								});
	                        });
	                    });
					});
                }

	            itrProgramToModify.order = req.body.itrProgram.order;
				itrProgramToModify.eligibility = req.body.itrProgram.eligibility;
				itrProgramToModify.student = req.body.itrProgram.student;
				itrProgramToModify.program = req.body.itrProgram.program;
	            itrProgramToModify.save(function (err) {
	                if (err) {
						res.status(500).json({ "error": err });
						return next(err);
	                }
	                res.status(201).json({ "itrProgram": itrProgramToModify });
	            });
	        });
	    })
	    .patch(function (req, res, next) {
	        ITRProgramModel.findById(req.params.id, function (err, itrProgramToModify) {
	            if (err) {
					res.status(404).json({ "error": err });
					return next(err);
	            }
				itrProgramToModify.order = req.body.itrProgram.order;
				itrProgramToModify.eligibility = req.body.itrProgram.eligibility;
				itrProgramToModify.student = req.body.itrProgram.student;
				itrProgramToModify.program = req.body.itrProgram.program;
	            itrProgramToModify.save(function (err) {
	                if (err) {
						res.status(500).json({ "error": err });
						return next(err);
	                }
	                res.status(201).json({ "itrProgram": itrProgramToModify });
	            });
	        });
	    })
	    .delete(function (req, res, next) {
	        ITRProgramModel.findById(req.params.id, function (err, itrProgramToDelete) {
	            if (err) {
					res.status(404).json({ "error": err });
					return next(err);
	            }
	            var removedProgram = itrProgramToDelete;
	            ITRProgramModel.remove({ "_id": req.params.id }, function (err) {
	                if (err) {
						res.status(500).json({ "error": err });
						return next(err);
	                }
	                res.json({ "itrProgram": removedProgram });
	            });
	        });
	    });
};
