//
// Created by stefan on 2016-03-12 3:18 PM
//

module.exports = function (app, UserModel) {

    app.route('/users')
            .post(function (request, response, next) {
                var user = new UserModel(request.body.user);
                user.save(function (error) {
                    if (error) {
                        response.send({error: error});
                    }
                    else {
                        response.json({user: user});
                    }
                });
            })

            .get(function (request, response, next) {
                var USER = request.query.filter;
                if (!USER) {
                    UserModel.find(function (error, users) {
                        if (error) response.send(error);
                        response.json({user: users});
                    });
                } else {
                    UserModel.findOne({"userName": USER.userName}, function (error, User) {
                        if (error) response.send(error);
                        response.json({user: User});
                    });
                }
            });

    app.route('/users/:id')
            .get(function (request, response, next) {
                UserModel.findById(request.params.id, function (error, user) {
                    if (error) {
                        response.send({error: error});
                    }
                    else {
                        response.json({user: user});
                    }
                });
            })
            .put(function (request, response, next) {
                UserModel.findById(request.params.id, function (error, user) {
                    if (error) {
                        response.send({error: error});
                    }
                    else {
                        user.firstName = request.body.user.firstName;
                        user.lastName = request.body.user.lastName;
                        user.email = request.body.user.email;
                        user.enabled = request.body.user.enabled;
                        user.userShadow = request.body.user.userShadow;
                        user.userRoles = request.body.user.userRoles;
                        user.save(function (error) {
                            if (error) {
                                response.send({error: error});
                            }
                            else {
                                response.json({user: user});
                            }
                        });
                    }
                });
            })
            .delete(function (request, response, next) {
                UserModel.findByIdAndRemove(request.params.id,
                        function (error, deleted) {
                            if (!error) {
                                response.json({user: deleted});
                            }
                        }
                );
            });
};
