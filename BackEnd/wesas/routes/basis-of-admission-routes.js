
module.exports = function (app, BasisOfAdmissionModel) {

    app.route('/basisOfAdmissions')
            .get(function (req, res, next) {
                if (req.query.students) {
                    BasisOfAdmissionModel.find({students: req.query.student}, function (err, basisOfAdmission) {
                        if (err) {
                            res.status(404).json({ error: err });
                            return next(err);
                        }
                        res.json({basisOfAdmission: basisOfAdmission});
                    });
                } else {
                    BasisOfAdmissionModel.find(function (err, basisOfAdmission) {
                        if (err) {
                            res.status(404).json({ error: err });
                            return next(err);
                        }
                        res.json({basisOfAdmission: basisOfAdmission});
                    });
                }
            })
            .post(function (req, res, next) {
                var basisOfAdmission = new BasisOfAdmissionModel({
                    date: req.body.basisOfAdmission.date,
                    comment: req.body.basisOfAdmission.comment,
                    code: req.body.basisOfAdmission.code,
                    student: req.body.basisOfAdmission.student
                });
                basisOfAdmission.save(function (err) {
                    if (err) {
                        res.status(500).json({ error: err });
                        return next(err);
                    }
                    res.json({basisOfAdmission: basisOfAdmission});
                });
            })
            .delete(function (req, res, next) {
                BasisOfAdmissionModel.remove(function (err) {
                    if (err) {
                        res.status(500).json({ error: err });
                        return next(err);
                    }
                    res.json({});
                });
            });


    app.route('/basisOfAdmissions/:id')
            .get(function (req, res, next) {
                BasisOfAdmissionModel.findById(req.params.id, function (err, basisOfAdmission) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    res.json({basisOfAdmission: basisOfAdmission});
                });
            })
            .put(function (req, res, next) {
                BasisOfAdmissionModel.findById(req.params.id, function (err, basisOfAdmission) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    basisOfAdmission.name = req.body.basisOfAdmission.name;
                    basisOfAdmission.comment = req.body.basisOfAdmission.comment;
                    basisOfAdmission.code = req.body.basisOfAdmission.code;
                    basisOfAdmission.student = req.body.basisOfAdmission.student;
                    basisOfAdmission.save(function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.status(201).json({basisOfAdmission: basisOfAdmission});
                    });
                });
            })
            .patch(function (req, res, next) {
                BasisOfAdmissionModel.findById(req.params.id, function (err, basisOfAdmission) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    basisOfAdmission.name = req.body.basisOfAdmission.name;
                    basisOfAdmission.comment = req.body.basisOfAdmission.comment;
                    basisOfAdmission.code = req.body.basisOfAdmission.code;
                    basisOfAdmission.student = req.body.basisOfAdmission.student;
                    basisOfAdmission.save(function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.status(201).json({basisOfAdmission: basisOfAdmission});
                    });
                });
            })
            .delete(function (req, res, next) {
                BasisOfAdmissionModel.findById(req.params.id, function (err, basisOfAdmission) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    var tmp = basisOfAdmission;
                    BasisOfAdmissionModel.remove({_id: req.params.id}, function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.json({basisOfAdmission: tmp});
                    });
                });
            });
};
