//
// Modified
//

module.exports = function (app, AdmissionRuleModel, LogicalExpressionModel) {

    app.route('/admissionRules')
            .get(function (req, res, next) {
                if (req.query.code) {
                    // Looking for the rule associated with a certain academic program
                    AdmissionRuleModel.findOne({ academicProgramCodes: req.query.code }, (err, admissionRule) => {
                        if (err) {
                            res.status(404).json({ error: err });
                            return next(err);
                        }
                        res.json({admissionRule: admissionRule});
                    });
                } else {
                    // Looking for all Admission Rules
                    AdmissionRuleModel.find(function (err, admissionRule) {
                        if (err) {
                            res.status(404).json({ error: err });
                            return next(err);
                        }
                        res.json({admissionRule: admissionRule});
                    });
                }

            })
            .post(function (req, res, next) {
                var admissionRule = new AdmissionRuleModel({
                    description: req.body.admissionRule.description,
                    testExpressions: req.body.admissionRule.testExpressions,
                    academicProgramCodes: req.body.admissionRule.academicProgramCodes
                });
                admissionRule.save(function (err) {
                    if (err) {
                        res.status(500).json({ error: err });
                        return next(err);
                    }
                    res.json({admissionRule: admissionRule});
                });
            })
            .delete(function (req, res, next) {
                AdmissionRuleModel.remove(function (err) {
                    if (err) {
                        res.status(500).json({ error: err });
                        return next(err);
                    }
                    res.json({});
                });
            });

    app.route('/admissionRules/:id')
            .get(function (req, res, next) {
                AdmissionRuleModel.findById(req.params.id, function (err, admissionRule) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    res.json({admissionRule: admissionRule});
                });
            })
            .put(function (req, res, next) {
                AdmissionRuleModel.findById(req.params.id, function (err, admissionRule) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    admissionRule.description = req.body.admissionRule.description;
                    admissionRule.testExpressions = req.body.admissionRule.testExpressions;
                    admissionRule.academicProgramCodes = req.body.admissionRule.academicProgramCodes;
                    admissionRule.save(function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.status(201).json({admissionRule: admissionRule});
                    });
                });
            })
            .patch(function (req, res, next) {
                AdmissionRuleModel.findById(req.params.id, function (err, admissionRule) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    admissionRule.description = req.body.admissionRule.description;
                    admissionRule.testExpressions = req.body.admissionRule.testExpressions;
                    admissionRule.academicProgramCodes = req.body.admissionRule.academicProgramCodes;
                    admissionRule.save(function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.status(201).json({admissionRule: admissionRule});
                    });
                });
            })
            .delete(function (req, res, next) {
                AdmissionRuleModel.findById(req.params.id, function (err, admissionRule) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }

                    // Find any related logical expressions and delete them first
                    LogicalExpressionModel.remove({ rule: req.params.id }, function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                    });

                    // Now continue with the deletetion of the rule itself
                    var tmp = admissionRule;
                    AdmissionRuleModel.remove({_id: req.params.id}, function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.json({admissionRule: tmp});
                    });
                });
            });
};
