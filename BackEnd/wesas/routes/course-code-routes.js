/*
 This file was created to refactor all the course code route definitions
 out of server.js. It takes an instance of express() and mounts routes
 onto it, so the caller (server.js) should pass a reference to the
 instance of express() used for the server. It also takes a reference
 to the MongoDB model for the course codes collection so it can do
 the appropriate DB operations in responding to requests.
 */

module.exports = function (app, CourseCodeModel) {

    app.route('/courseCodes')
            .get(function (req, res, next) {
                if (req.query.code && req.query.number && req.query.name && req.query.unit) {
                    CourseCodeModel.find({
                        code: req.query.code,
                        number: req.query.number,
                        name: req.query.name,
                        unit: req.query.unit
                    }, function (err, courseCode) {
                        if (err) {
                            res.status(404).json({error: err});
                            return next(err);
                        }
                        res.json({courseCode: courseCode});
                    });
                } else {
                    CourseCodeModel.find(function (err, courseCode) {
                        if (err) {
                            res.status(404).json({error: err});
                            return next(err);
                        }
                        res.json({courseCode: courseCode});
                    });
                }
            })
            .post(function (req, res, next) {
                var courseCode = new CourseCodeModel({
                    code: req.body.courseCode.code,
                    number: req.body.courseCode.number,
                    name: req.body.courseCode.name,
                    unit: req.body.courseCode.unit,
                    grades: req.body.courseCode.grades
                });
                courseCode.save(function (err) {
                    if (err) {
                        res.status(500).json({ error: err });
                        return next(err);
                    }
                    res.json({courseCode: courseCode});
                });
            })
            .delete(function (req, res, next) {
                CourseCodeModel.remove(function (err) {
                    if (err) {
                        res.status(500).json({ error: err });
                        return next(err);
                    }
                    res.json({});
                });
            });


    app.route('/courseCodes/:id')
            .get(function (req, res, next) {
                CourseCodeModel.findById(req.params.id, function (err, courseCode) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    res.json({courseCode: courseCode});
                });
            })
            .put(function (req, res, next) {
                CourseCodeModel.findById(req.params.id, function (err, courseCode) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    courseCode.code = req.body.courseCode.code;
                    courseCode.number = req.body.courseCode.number;
                    courseCode.name = req.body.courseCode.name;
                    courseCode.unit = req.body.courseCode.unit;
                    courseCode.grades = req.body.courseCode.grades;
                    courseCode.save(function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.json({courseCode: courseCode});
                    });
                });
            })
            .patch(function (req, res, next) {
                CourseCodeModel.findById(req.params.id, function (err, courseCode) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    courseCode.code = req.body.courseCode.code;
                    courseCode.number = req.body.courseCode.number;
                    courseCode.name = req.body.courseCode.name;
                    courseCode.unit = req.body.courseCode.unit;
                    courseCode.grades = req.body.courseCode.grades;
                    courseCode.save(function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.json({courseCode: courseCode});
                    });
                });
            })
            .delete(function (req, res, next) {
                CourseCodeModel.findById(req.params.id, function (err, courseCode) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    var tmp = courseCode;
                    CourseCodeModel.remove({_id: req.params.id}, function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.json({courseCode: tmp});
                    });
                });
            });
};
