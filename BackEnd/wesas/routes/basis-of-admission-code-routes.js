module.exports = function (app, BasisOfAdmissionCodeModel) {

    app.route('/basisOfAdmissionCodes')
        .get(function (req, res, next) {
            BasisOfAdmissionCodeModel.find(function (err, basisOfAdmissionCode) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                res.json({basisOfAdmissionCode: basisOfAdmissionCode});
            });
        })
        .post(function (req, res, next) {
            var basisOfAdmissionCode = new BasisOfAdmissionCodeModel({
                name: req.body.basisOfAdmissionCode.name,
                basis: req.body.basisOfAdmissionCode.basis
            });
            basisOfAdmissionCode.save(function (err) {
                if (err) {
                    res.status(500).json({error: err});
                    return next(err);
                }
                res.json({basisOfAdmissionCode: basisOfAdmissionCode});
            });
        })
        .delete(function (req, res, next) {
            BasisOfAdmissionCodeModel.remove(function (err) {
                if (err) {
                    res.status(500).json({error: err});
                    return next(err);
                }
                res.json({});
            });
        });


    app.route('/basisOfAdmissionCodes/:id')
        .get(function (req, res, next) {
            BasisOfAdmissionCodeModel.findById(req.params.id, function (err, basisOfAdmissionCode) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                res.json({basisOfAdmissionCode: basisOfAdmissionCode});
            });
        })
        .put(function (req, res, next) {
            BasisOfAdmissionCodeModel.findById(req.params.id, function (err, basisOfAdmissionCode) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                basisOfAdmissionCode.name = req.body.basisOfAdmissionCode.name;
                basisOfAdmissionCode.basis = req.body.basisOfAdmissionCode.basis;
                basisOfAdmissionCode.save(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.status(201).json({basisOfAdmissionCode: basisOfAdmissionCode});
                });
            });
        })
        .patch(function (req, res, next) {
            BasisOfAdmissionCodeModel.findById(req.params.id, function (err, basisOfAdmissionCode) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                basisOfAdmissionCode.name = req.body.basisOfAdmissionCode.name;
                basisOfAdmissionCode.basis = req.body.basisOfAdmissionCode.basis;
                basisOfAdmissionCode.save(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.status(201).json({basisOfAdmissionCode: basisOfAdmissionCode});
                });
            });
        })
        .delete(function (req, res, next) {
            BasisOfAdmissionCodeModel.findById(req.params.id, function (err, basisOfAdmissionCode) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                var tmp = basisOfAdmissionCode;
                BasisOfAdmissionCodeModel.remove({_id: req.params.id}, function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.json({basisOfAdmissionCode: tmp});
                });
            });
        });
};
