/*
	This file was created to refactor all the comment code route definitions
	out of server.js. It takes an instance of express() and mounts routes
	onto it, so the caller (server.js) should pass a reference to the
	instance of express() used for the server. It also takes a reference
	to the MongoDB model for the countries collection so it can do
	the appropriate DB operations in responding to requests.
*/

module.exports = function (app, CommentCodeModel) {
	app.route('/commentCodes')
	    .get(function (req, res, next) {
	        if (req.query.code) {
	        	CommentCodeModel.find({code: req.query.code }, function (err, commentCode) {
		            if (err) {
						res.status(404).json({ error: err });
						return next(err);
		            }
		            res.json({ commentCode: commentCode});
	        	});
	        } else {
	        	// No QS so find all comment codes
	        	CommentCodeModel.find(function (err, comment) {
		            if (err) {
						res.status(404).json({ error: err });
						return next(err);
		            }
		            res.json({ commentCode: comment});
	        	});
	        }
	    })
	    .post(function (req, res, next) {
	        var newComment = new CommentCodeModel({
                  code: req.body.commentCode.code,
                  progAction: req.body.commentCode.progAction,
                  description:req.body.commentCode.description,
                  notes: req.body.commentCode.notes,
                  comment: req.body.commentCode.comment,
	        });
	        newComment.save(function (err) {
	            if (err) {
					res.status(500).json({ error: err });
					return next(err);
	            }
	            res.json({ commentCode: newComment});
	        });
	    })
	    .delete(function(req, res, next) {
	        CommentCodeModel.remove(function (err) {
	            if (err) {
					res.status(500).json({ "error": err });
					return next(err);
	            }
	            res.json({});
	        });
	    });


	app.route('/commentCodes/:id')
	    .get(function (req, res, next) {
	        CommentCodeModel.findById(req.params.id, function (err, comment) {
	            if (err) {
					res.status(404).json({ "error": err });
					return next(err);
	            }
	            	res.json({ "commentCode": comment });
	        });
	    })
	    .put(function (req, res, next) {
	        CommentCodeModel.findById(req.params.id, function (err, commentToModify) {
	            if (err) {
	                res.status(404).json({ "error": err });
					return next(err);
	            }
            commentToModify.code  = req.body.commentCode.code;
            commentToModify.progAction  = req.body.commentCode.progAction;
            commentToModify.description  = req.body.commentCode.description;
            commentToModify.notes  = req.body.commentCode.notes;
            commentToModify.comment  = req.body.commentCode.comment;

	            commentToModify.save(function (err) {
	                if (err) {
						res.status(500).json({ "error": err });
						return next(err);
	                }
	                res.status(201).json({ "commentCode": commentToModify });
	            });
	        });
	    })
	    .patch(function (req, res, next) {
	        CommentCodeModel.findById(req.params.id, function (err, comment) {
	            if (err) {
					res.status(404).json({ "error": err });
					return next(err);
	            }
              comment.code  = req.body.commentCode.code;
              comment.progAction  = req.body.commentCode.progAction;
              comment.description  = req.body.commentCode.description;
              comment.notes  = req.body.commentCode.notes;
              comment.comment  = req.body.commentCode.comment;

	            commentToModify.save(function (err) {
	                if (err) {
						res.status(500).json({ error: err });
						return next(err);
	                }
	                res.status(201).json({ commentCode: commentToModify });
	            });
	        });
	    })
	    .delete(function (req, res, next) {
	        commentToModify.findById(req.params.id, function (err, comment) {
	            if (err) {
					res.status(404).json({ error: err });
					return next(err);
	            }
	            var removedCommentCode = comment;
	            CommentCodeModel.remove({ _id: req.params.id }, function (err) {
	                if (err) {
						res.status(500).json({ error: err });
						return next(err);
	                }
	                res.json({ commentCode: removedCommentCode });
	            });
	        });
	    });
}
