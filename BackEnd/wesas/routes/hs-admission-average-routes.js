
module.exports = function (app, HSAdmissionAverageModel) {

    app.route('/hsAdmissionAverages')
            .get(function (req, res, next) {
                if (req.query.students) {
                    HSAdmissionAverageModel.find({students: req.query.student}, function (err, adAvg) {
                        if (err) {
                            res.status(404).json({error: err});
                            return next(err);
                        }
                        res.json({hsAdmissionAverage: adAvg});
                    });
                } else {
                    HSAdmissionAverageModel.find(function (err, adAvg) {
                        if (err) {
                            res.status(404).json({error: err});
                            return next(err);
                        }
                        res.json({hsAdmissionAverage: adAvg});
                    });
                }
            })
            .post(function (req, res, next) {
                var adAvg = new HSAdmissionAverageModel({
                    grade11: req.body.hsAdmissionAverage.grade11,
                    first: req.body.hsAdmissionAverage.first,
                    midYear: req.body.hsAdmissionAverage.midYear,
                    finalVal: req.body.hsAdmissionAverage.finalVal,
                    student: req.body.hsAdmissionAverage.student
                });
                adAvg.save(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.json({hsAdmissionAverage: adAvg});
                });
            })
            .delete(function (req, res, next) {
                HSAdmissionAverageModel.remove(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.json({});
                });
            });


    app.route('/hsAdmissionAverages/:id')
            .get(function (req, res, next) {
                HSAdmissionAverageModel.findById(req.params.id, function (err, adAvg) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    res.json({hsAdmissionAverage: adAvg});
                });
            })
            .put(function (req, res, next) {
                HSAdmissionAverageModel.findById(req.params.id, function (err, adAvg) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    adAvg.first = req.body.hsAdmissionAverage.first;
                    adAvg.midYear = req.body.hsAdmissionAverage.midYear;
                    adAvg.finalVal = req.body.hsAdmissionAverage.finalVal;
                    adAvg.grade11 = req.body.hsAdmissionAverage.grade11;
                    adAvg.student = req.body.hsAdmissionAverage.student;

                    adAvg.save(function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.status(201).json({hsAdmissionAverage: adAvg});
                    });
                });
            })
            .patch(function (req, res, next) {
                HSAdmissionAverageModel.findById(req.params.id, function (err, adAvg) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    adAvg.first = req.body.hsAdmissionAverage.first;
                    adAvg.midYear = req.body.hsAdmissionAverage.midYear;
                    adAvg.finalVal = req.body.hsAdmissionAverage.finalVal;
                    adAvg.grade11 = req.body.hsAdmissionAverage.grade11;
                    adAvg.student = req.body.hsAdmissionAverage.student;

                    adAvg.save(function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.status(201).json({hsAdmissionAverage: adAvg});
                    });
                });
            })
            .delete(function (req, res, next) {
                HSAdmissionAverageModel.findById(req.params.id, function (err, adAvg) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    var tmp = adAvg;
                    HSAdmissionAverageModel.remove({_id: req.params.id}, function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.json({hsAdmissionAverage: tmp});
                    });
                });
            });
};
