/*
 This file was created to refactor all the academic load route definitions
 out of server.js. It takes an instance of express() and mounts routes
 onto it, so the caller (server.js) should pass a reference to the
 instance of express() used for the server. It also takes a reference
 to the MongoDB model for the academicloads collection so it can do
 the appropriate DB operations in responding to requests. TODO: Make
 sure the URIs are correct given how Ember handles model names made up
 of many words and make any appropriate changes to itr-program-routes
 too.
 */

module.exports = function (app, AcademicLoadModel) {

    app.route('/academicLoads')
            .get(function (req, res, next) {
                if (req.query.students) {
                    AcademicLoadModel.find({students: req.query.student}, function (err, academicLoad) {
                        if (err) {
                            res.status(404).json({ error: err });
                            return next(err);
                        }
                        res.json({academicLoad: academicLoad});
                    });
                } else {
                    AcademicLoadModel.find(function (err, academicLoad) {
                        if (err) {
                            res.status(404).json({error: err});
                            return;
                        }
                        res.json({academicLoad: academicLoad});
                    });
                }
            })
            .post(function (req, res, next) {
                var academicLoad = new AcademicLoadModel({
                    name: req.body.academicLoad.name,
                    students: req.body.academicLoad.students
                });
                academicLoad.save(function (err) {
                    if (err) {
                        res.status(500).json({ error: err });
                        return next(err);
                    }
                    res.json({academicLoad: academicLoad});
                });
            })
            .delete(function (req, res, next) {
                AcademicLoadModel.remove(function (err) {
                    if (err) {
                        res.status(500).json({ error: err });
                        return next(err);
                    }
                    res.json({});
                });
            });


    app.route('/academicLoads/:id')
            .get(function (req, res, next) {
                AcademicLoadModel.findById(req.params.id, function (err, academicLoad) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    res.json({academicLoad: academicLoad});
                });
            })
            .put(function (req, res, next) {
                AcademicLoadModel.findById(req.params.id, function (err, academicLoad) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    academicLoad.name = req.body.academicLoad.name;
                    academicLoad.students = req.body.academicLoad.students;
                    academicLoad.save(function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.status(201).json({academicLoad: academicLoad});
                    });
                });
            })
            .patch(function (req, res, next) {
                AcademicLoadModel.findById(req.params.id, function (err, academicLoad) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    academicLoad.name = req.body.academicLoad.name;
                    academicLoad.students = req.body.academicLoad.students;
                    academicLoad.save(function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.status(201).json({academicLoad: academicLoad});
                    });
                });
            })
            .delete(function (req, res, next) {
                AcademicLoadModel.findById(req.params.id, function (err, academicLoad) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    var tmp = academicLoad;
                    AcademicLoadModel.remove({_id: req.params.id}, function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.json({academicLoad: tmp});
                    });
                });
            });
};
