//
// Created by stefan on 2016-03-12 3:22 PM
//

module.exports = function (app, UserRoleModel) {

    app.route('/userRoles')
            .post(function (request, response, next) {
                var userRole = new UserRoleModel(request.body.userRole);
                userRole.save(function (error) {
                    if (error) {
                        response.status(500).json({ error: error });
                        return next(err);
                    }
                    response.json({userRole: userRole});
                });
            })
            .get(function (request, response, next) {
                var User = request.query.filter;
                if (!User) {
                    UserRoleModel.find(function (error, userRoles) {
                        if (error) {
                            response.status(404).json({ error: error });
                            return next(err);
                        }
                        response.json({userRole: userRoles});
                    });
                } else {
                    if (!User.user) {
                        UserRoleModel.find({"role": User.role}, function (error, userRoles) {
                            if (error) {
                                response.status(404).json({ error: error });
                                return next(err);
                            }
                            response.json({userRole: userRoles});
                        });
                    } else {
                        if (!User.role) {
                            UserRoleModel.find({"user": User.user}, function (error, userRoles) {
                                if (error) {
                                    response.status(404).json({ error: error });
                                    return next(err);
                                }
                                response.json({userRole: userRoles});
                            });
                        } else {
                            UserRoleModel.find({"user": User.user, "role": User.role}, function (error, userRoles) {
                                if (error) {
                                    response.status(404).json({ error: error });
                                    return next(err);
                                }
                                response.json({userRole: userRoles});
                            });
                        }
                    }
                }
            });

    app.route('/userRoles/:id')
            .get(function (request, response, next) {
                UserRoleModel.findById(request.params.id, function (error, userRole) {
                    if (error) {
                        response.status(404).json({ error: error });
                        return next(err);
                    }
                    response.json({userRole: userRole});
                })
            })
            .put(function (request, response, next) {
                UserRoleModel.findById(request.params.id, function (error, userRole) {
                    if (error) {
                        response.status(500).json({ error: error });
                        return next(err);
                    }
                    else {
                        userRole.dateAssigned = request.body.userRole.dateAssigned;
                        userRole.user = request.body.userRole.user;
                        userRole.role = request.body.userRole.role;
                        userRole.save(function (error) {
                            if (error) {
                                response.send({error: error});
                            }
                            else {
                                response.json({userRole: userRole});
                            }
                        });
                    }
                })
            })
            .delete(function (request, response, next) {
                UserRoleModel.findByIdAndRemove(request.params.id,
                        function (error, deleted) {
                            if (error) {
                                response.status(500).json({ error: error });
                                return next(err);
                            }
                            response.json({userRole: deleted});
                        }
                );
            });
};
