//
// Created by stefan on 2016-02-27 12:52 PM
//

module.exports = function (app, DegreeCodeModel) {

    app.route('/degreeCodes')
            .get(function (req, res, next) {
                if (req.query.name) {
                    DegreeCodeModel.find({name: req.query.name}, function (err, degreeCode) {
                        if (err) {
                            res.status(404).json({error: err});
                            return next(err);
                        }
                        res.json({degreeCode: degreeCode});
                    });
                } else {
                    DegreeCodeModel.find(function (err, degreeCode) {
                        if (err) {
                            res.status(404).json({error: err});
                            return next(err);
                        }
                        res.json({degreeCode: degreeCode});
                    });
                }
            })
            .post(function (req, res, next) {

                var degreeCode = new DegreeCodeModel({
                    name: req.body.degreeCode.name,
                    programRecords: req.body.degreeCode.programRecords
                });
                degreeCode.save(function (err) {
                    if (err) {
                        res.status(500).json({ error: err });
                        return next(err);
                    }
                    res.json({degreeCode: degreeCode});
                });
            })
            .delete(function (req, res, next) {
                DegreeCodeModel.remove(function (err) {
                    if (err) {
                        res.status(500).json({ error: err });
                        return next(err);
                    }
                    res.json({});
                });
            });

    app.route('/degreeCodes/:id')
            .get(function (req, res, next) {
                DegreeCodeModel.findById(req.params.id, function (err, degreeCode) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    res.json({degreeCode: degreeCode});
                });
            })
            .put(function (req, res, next) {
                DegreeCodeModel.findById(req.params.id, function (err, degreeCode) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    degreeCode.name = req.body.degreeCode.name;
                    degreeCode.programRecords = req.body.degreeCode.programRecords;
                    degreeCode.save(function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.status(201).json({degreeCode: degreeCode});
                    });
                });
            })
            .patch(function (req, res, next) {
                DegreeCodeModel.findById(req.params.id, function (err, degreeCode) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    degreeCode.name = req.body.degreeCode.name;
                    degreeCode.programRecords = req.body.degreeCode.programRecords;
                    degreeCode.save(function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.status(201).json({degreeCode: degreeCode});
                    });
                });
            })
            .delete(function (req, res, next) {
                DegreeCodeModel.findById(req.params.id, function (err, degreeCode) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    var tmp = degreeCode;
                    DegreeCodeModel.remove({_id: req.params.id}, function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.json({degreeCode: tmp});
                    });
                });
            });
};
