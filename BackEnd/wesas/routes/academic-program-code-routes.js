/*
 This file was created to refactor all the academic program code route
 definitions out of server.js. It takes an instance of express() and mounts
 routes onto it, so the caller (server.js) should pass a reference to the
 instance of express() used for the server. It also takes a reference
 to the MongoDB model for the academiccodes collection so it can do
 the appropriate DB operations in responding to requests.
 */

module.exports = function (app, AcademicProgramCodeModel) {

    app.route('/academicProgramCodes')
            .get(function (req, res, next) {
                if (req.query.code) {
                    AcademicProgramCodeModel.find({code: req.query.code}, function (err, academicProgramCode) {
                        if (err) {
                            res.status(404).json({error: err});
                            return next(err);
                        }
                        res.json({academicProgramCode: academicProgramCode});
                    });
                } else {
                    AcademicProgramCodeModel.find(function (err, academicProgramCode) {
                        if (err) {
                            res.status(404).json({error: err});
                            return next(err);
                        }
                        res.json({academicProgramCode: academicProgramCode});
                    });
                }
            })
            .post(function (req, res, next) {
                var academicProgramCode = new AcademicProgramCodeModel({
                    name: req.body.academicProgramCode.name,
                    code: req.body.academicProgramCode.code,
                    subCode: req.body.academicProgramCode.subCode,
                    rule: req.body.academicProgramCode.rule,
                    dept: req.body.academicProgramCode.dept,
                    itrProgram: req.body.academicProgramCode.itrProgram,
                    commentCode: req.body.academicProgramCode.commentCode
                });
                academicProgramCode.save(function (err) {
                    if (err) {
                        res.status(500).json({ error: err });
                        return next(err);
                    }
                    res.json({academicProgramCode: academicProgramCode});
                });
            })
            .delete(function (req, res, next) {
                AcademicProgramCodeModel.remove(function (err) {
                    if (err) {
                        res.status(500).json({ error: err });
                        return next(err);
                    }
                    res.json({});
                });
            });

    app.route('/academicProgramCodes/:id')
            .get(function (req, res, next) {
                AcademicProgramCodeModel.findById(req.params.id, function (err, academicProgramCode) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    res.json({academicProgramCode: academicProgramCode});
                });
            })
            .put(function (req, res, next) {
                AcademicProgramCodeModel.findById(req.params.id, function (err, academicProgramCode) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    academicProgramCode.name = req.body.academicProgramCode.name;
                    academicProgramCode.code = req.body.academicProgramCode.code;
                    academicProgramCode.subCode = req.body.academicProgramCode.subCode;
                    academicProgramCode.rule = req.body.academicProgramCode.rule;
                    academicProgramCode.dept = req.body.academicProgramCode.dept;
                    academicProgramCode.itrProgram = req.body.academicProgramCode.itrProgram;
                    academicProgramCode.commentCode = req.body.academicProgramCode.commentCode;
                    academicProgramCode.save(function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.status(201).json({academicProgramCode: academicProgramCode});
                    });
                });
            })
            .patch(function (req, res, next) {
                AcademicProgramCodeModel.findById(req.params.id, function (err, academicProgramCode) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    academicProgramCode.name = req.body.academicProgramCode.name;
                    academicProgramCode.code = req.body.academicProgramCode.code;
                    academicProgramCode.subCode = req.body.academicProgramCode.subCode;
                    academicProgramCode.rule = req.body.academicProgramCode.rule;
                    academicProgramCode.dept = req.body.academicProgramCode.dept;
                    academicProgramCode.itrProgram = req.body.academicProgramCode.itrProgram;
                    academicProgramCode.commentCode = req.body.academicProgramCode.commentCode;
                    academicProgramCode.save(function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.status(201).json({academicProgramCode: academicProgramCode});
                    });
                });
            })
            .delete(function (req, res, next) {
                AcademicProgramCodeModel.findById(req.params.id, function (err, academicProgramCode) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    var tmp = academicProgramCode;
                    AcademicProgramCodeModel.remove({_id: req.params.id}, function (err) {
                        if (err) {
                            res.status(500);
                            return next(err);
                        }
                        res.json({academicProgramCode: tmp});
                    });
                });
            });
};
