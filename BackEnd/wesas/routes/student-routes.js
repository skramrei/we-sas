/*
 This file was created to refactor all the student route definitions
 out of server.js. It takes an instance of express() and mounts routes
 onto it, so the caller (server.js) should pass a reference to the
 instance of express() used for the server. It also takes a reference
 to the MongoDB model for the students collection so it can do
 the appropriate DB operations in responding to requests.
 */


/*
 Takes an object containing the optional fields for a student that may not be
 known at the time of student creation or updating and adds them to the student if
 they exist. NOTE: Feb. 11 2016: We actually don't need this fuction since
 attributes not passed in the request will equal undefined. We can pass mongoose
 undefined as an attribute value and its smart enough to not modify that attriubte
 in the actual document. Thus, the server can blindly pass all possible attributes
 from the request to mongoose even if they aren't there.
 */
/*
 function addOptionalFieldsToStudent (fields, student) {
 if (fields.resInfo) {
 student.resInfo = fields.resInfo;
 }
 if (fields.gender) {
 student.gender = fields.gender;
 }
 if (fields.studyLoad) {
 student.studyLoad = fields.studyLoad;
 }
 if (fields.country) {
 student.country = fields.country;
 }
 if (fields.province) {
 student.province = fields.province;
 }
 if (fields.city) {
 student.city = fields.city;
 }
 if (fields.ITRList) {
 student.ITRList = fields.ITRList;
 }
 }
 */

module.exports = function (app, StudentModel) {

    app.route('/students')
        .get(function (req, res, next) {
            if (req.query.queryFilter) {
                StudentModel.find({
                    studentNumber: {$regex: new RegExp('\\S*(' + req.query.queryFilter + ')\\S*', "gi")}
                }, function (err, student) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    res.json({student: student});
                });
            } else if (req.query.skip && req.query.limit) {
                StudentModel.find(null, null, {skip: Number(req.query.skip), limit: Number(req.query.limit)}, function (err, student) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    res.json({student: student});
                });
            } else if (req.query.id) {
                StudentModel.findById(req.query.id, function (err, student) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    res.json({student: student});
                });
            } else if (req.query.studentNumber) {
                StudentModel.find({studentNumber: req.query.studentNumber}, function (err, student) {
                   if (err) {
                       res.status(404).json({error: err});
                       return next(err);
                   }
                    res.json({student: student});
                });
            } else {
                StudentModel.find(function (err, student) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    res.json({student: student});
                });
            }
        })
        .post(function (req, res, next) {
            var student = new StudentModel({
                studentNumber: req.body.student.studentNumber,
                firstName: req.body.student.firstName,
                lastName: req.body.student.lastName,
                DOB: req.body.student.DOB,
                residency: req.body.student.residency,
                gender: req.body.student.gender,
                academicLoad: req.body.student.academicLoad,
                country: req.body.student.country,
                province: req.body.student.province,
                city: req.body.student.city,
                grades: req.body.student.grades,
                itrPrograms: req.body.student.itrPrograms,
                distributionResults: req.body.student.distributionResults
            });
            student.save(function (err) {
                if (err) {
                    res.status(500).json({error: err});
                    return next(err);
                }
                res.json({student: student});
            });
        })
        .delete(function (req, res, next) {
            StudentModel.remove(function (err) {
                if (err) {
                    res.status(500).json({error: err});
                    return next(err);
                }
                res.json({});
            });
        });

    app.route('/students/:id')
        .get(function (req, res, next) {
            StudentModel.findById(req.params.id, function (err, student) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                res.json({student: student});
            });
        })
        .put(function (req, res, next) {
            StudentModel.findById(req.params.id, function (err, student) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                student.studentNumber = req.body.student.studentNumber;
                student.firstName = req.body.student.firstName;
                student.lastName = req.body.student.lastName;
                student.DOB = req.body.student.DOB;
                student.residency = req.body.student.residency;
                student.gender = req.body.student.gender;
                student.academicLoad = req.body.student.academicLoad;
                student.country = req.body.student.country;
                student.province = req.body.student.province;
                student.city = req.body.student.city;
                student.grades = req.body.student.grades;
                student.itrPrograms = req.body.student.itrPrograms;
                student.distributionResults = req.body.student.distributionResults;
                student.save(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.status(201).json({student: student});
                });
            });
        })
        .patch(function (req, res, next) {
            StudentModel.findById(req.params.id, function (err, student) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                student.studentNumber = req.body.student.studentNumber;
                student.firstName = req.body.student.firstName;
                student.lastName = req.body.student.lastName;
                student.DOB = req.body.student.DOB;
                student.residency = req.body.student.residency;
                student.gender = req.body.student.gender;
                student.academicLoad = req.body.student.academicLoad;
                student.country = req.body.student.country;
                student.province = req.body.student.province;
                student.city = req.body.student.city;
                student.grades = req.body.student.grades;
                student.itrPrograms = req.body.student.itrPrograms;
                student.distributionResults = req.body.student.distributionResults;
                student.save(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.status(201).json({student: student});
                });
            });
        })
        .delete(function (req, res, next) {
            StudentModel.findById(req.params.id, function (err, student) {
                if (err) {
                    res.status(404).json({error: err});
                    return next(err);
                }
                var tmp = student;
                StudentModel.remove({_id: req.params.id}, function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.json({student: tmp});
                });
            });
        });
};
