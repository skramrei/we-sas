//
// Created by Stefan Kramreither on 2/25/2016 9:08
//

module.exports = function (app, TermCodeModel) {

	app.route('/termCodes')
			.get(function (req, res, next) {
				if (req.query.programRecords) {
					TermCodeModel.find({programRecords: req.query.programRecords}, function (err, termCode) {
						if (err) {
							res.status(404).json({error: err});
							return next(err);
						}
						res.json({termCode: termCode});
					});
				} else if (req.query.name) {
					TermCodeModel.find({name: req.query.name}, function (err, termCode) {
						if (err) {
							res.status(404).json({error: err});
							return next(err);
						}
						res.json({termCode: termCode});
					});
				} else {
					TermCodeModel.find(function (err, termCode) {
						if (err) {
							res.status(404).json({error: err});
							return next(err);
						}
						res.json({termCode: termCode});
					});
				}
			})
			.post(function (req, res, next) {
				var termCode = new TermCodeModel({
					name: req.body.termCode.name,
					programRecords: req.body.termCode.programRecords
				});
				termCode.save(function (err) {
					if (err) {
						res.status(500).json({error: err});
						return next(err);
					}
					res.json({termCode: termCode});
				});
			})
			.delete(function (req, res, next) {
				TermCodeModel.remove(function (err) {
					if (err) {
						res.status(500).json({error: err});
						return next(err);
					}
					res.json({});
				});
			});

	app.route('/termCodes/:id')
			.get(function (req, res, next) {
				TermCodeModel.findById(req.params.id, function (err, termCode) {
					if (err) {
						res.status(404).json({error: err});
						return next(err);
					}
					res.json({termCode: termCode});
				});
			})
			.put(function (req, res, next) {
				TermCodeModel.findById(req.params.id, function (err, termCode) {
					if (err) {
						res.status(404).json({error: err});
						return next(err);
					}
					termCode.name = req.body.termCode.name;
					termCode.programRecords = req.body.termCode.programRecords;
					termCode.save(function (err) {
						if (err) {
							res.status(500).json({error: err});
							return next(err);
						}
						res.status(201).json({termCode: termCode});
					});
				});
			})
			.patch(function (req, res, next) {
				TermCodeModel.findById(req.params.id, function (err, termCode) {
					if (err) {
						res.status(404).json({error: err});
						return next(err);
					}
					termCode.name = req.body.termCode.name;
					termCode.programRecords = req.body.termCode.programRecords;
					termCode.save(function (err) {
						if (err) {
							res.status(500).json({error: err});
							return next(err);
						}
						res.status(201).json({termCode: termCode});
					});
				});
			})
			.delete(function (req, res, next) {
				TermCodeModel.findById(req.params.id, function (err, termCode) {
					if (err) {
						res.status(404).json({error: err});
						return next(err);
					}
					var tmp = termCode;
					TermCodeModel.remove({_id: req.params.id}, function (err) {
						if (err) {
							res.status(500).json({error: err});
							return next(err);
						}
						res.json({termCode: tmp});
					});
				});
			});
};
