/*
 This file was created to refactor all the department route definitions
 out of server.js. It takes an instance of express() and mounts routes
 onto it, so the caller (server.js) should pass a reference to the
 instance of express() used for the server. It also takes a reference
 to the MongoDB model for the programs collection so it can do
 the appropriate DB operations in responding to requests.
 */

module.exports = function (app, DepartmentModel) {

    app.route('/departments')
            .get(function (req, res, next) {
                if (req.query.name) {
                    DepartmentModel.find({name: req.query.name}, function (err, department) {
                        if (err) {
                            res.status(404).json({error: err});
                            return next(err);
                        }
                        res.json({department: department});
                    });
                } else {
                    DepartmentModel.find(function (err, department) {
                        if (err) {
                            res.status(404).json({error: err});
                            return next(err);
                        }
                        res.json({department: department});
                    });
                }
            })
            .post(function (req, res, next) {
                var department = new DepartmentModel({
                    name: req.body.department.name,
                    faculty: req.body.department.faculty,
                    programAdministrations: req.body.department.programAdministrations
                });
                department.save(function (err) {
                    if (err) {
                        res.status(500).json({ error: err });
                        return next(err);
                    }
                    res.json({department: department});
                });
            })
            .delete(function (req, res, next) {
                DepartmentModel.remove(function (err) {
                    if (err) {
                        res.status(500).json({ error: err });
                        return next(err);
                    }
                    res.json({});
                });
            });


    app.route('/departments/:id')
            .get(function (req, res, next) {
                DepartmentModel.findById(req.params.id, function (err, department) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    res.json({department: department});
                });
            })
            .put(function (req, res, next) {
                DepartmentModel.findById(req.params.id, function (err, department) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    department.name = req.body.department.name;
                    department.faculty = req.body.department.faculty;
                    department.programAdministrations = req.body.department.programAdministrations;
                    department.save(function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.status(201).json({department: department});
                    });
                });
            })
            .patch(function (req, res, next) {
                DepartmentModel.findById(req.params.id, function (err, department) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    department.name = req.body.department.name;
                    department.faculty = req.body.department.faculty;
                    department.programAdministrations = req.body.department.programAdministrations;
                    department.save(function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.status(201).json({department: department});
                    });
                });
            })
            .delete(function (req, res, next) {
                DepartmentModel.findById(req.params.id, function (err, department) {
                    if (err) {
                        res.status(404).json({ error: err });
                        return next(err);
                    }
                    var tmp = department;
                    DepartmentModel.remove({_id: req.params.id}, function (err) {
                        if (err) {
                            res.status(500).json({ error: err });
                            return next(err);
                        }
                        res.json({department: tmp});
                    });
                });
            });
};
