/*
 This file was created to refactor all the programAdministration route definitions
 out of server.js. It takes an instance of express() and mounts routes
 onto it, so the caller (server.js) should pass a reference to the
 instance of express() used for the server. It also takes a reference
 to the MongoDB model for the programs collection so it can do
 the appropriate DB operations in responding to requests.
 */

module.exports = function (app, ProgramAdministrationModel) {

    app.route('/programAdministrations')
            .get(function (req, res, next) {
                ProgramAdministrationModel.find(function (err, programAdministration) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    res.json({programAdministration: programAdministration});
                });
            })
            .post(function (req, res, next) {
                var programAdministration = new ProgramAdministrationModel({
                    name: req.body.programAdministration.name,
                    position: req.body.programAdministration.position,
                    department: req.body.programAdministration.department,
                    academicProgramCode: req.body.programAdministration.academicProgramCode,
                });
                programAdministration.save(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.json({programAdministration: programAdministration});
                });
            })
            .delete(function (req, res, next) {
                ProgramAdministrationModel.remove(function (err) {
                    if (err) {
                        res.status(500).json({error: err});
                        return next(err);
                    }
                    res.json({});
                });
            });

    app.route('/programAdministrations/:id')
            .get(function (req, res, next) {
                ProgramAdministrationModel.findById(req.params.id, function (err, programAdministration) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    res.json({programAdministration: programAdministration});
                });
            })
            .put(function (req, res, next) {
                ProgramAdministrationModel.findById(req.params.id, function (err, programAdministration) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    programAdministration.name = req.body.programAdministration.name;
                    programAdministration.position = req.body.programAdministration.position;
                    programAdministration.department = req.body.programAdministration.department;
                    programAdministration.academicProgramCode = req.body.programAdministration.academicProgramCode;
                    programAdministration.save(function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.status(201).json({programAdministration: programAdministration});
                    });
                });
            })
            .patch(function (req, res, next) {
                ProgramAdministrationModel.findById(req.params.id, function (err, programAdministration) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    programAdministration.name = req.body.programAdministration.name;
                    programAdministration.position = req.body.programAdministration.position;
                    programAdministration.department = req.body.programAdministration.department;
                    programAdministration.academicProgramCode = req.body.programAdministration.academicProgramCode;
                    programAdministration.save(function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.status(201).json({programAdministration: programAdministration});
                    });
                });
            })
            .delete(function (req, res, next) {
                ProgramAdministrationModel.findById(req.params.id, function (err, programAdministration) {
                    if (err) {
                        res.status(404).json({error: err});
                        return next(err);
                    }
                    var tmp = programAdministration;
                    ProgramAdministrationModel.remove({_id: req.params.id}, function (err) {
                        if (err) {
                            res.status(500).json({error: err});
                            return next(err);
                        }
                        res.json({programAdministration: tmp});
                    });
                });
            });
}
