/*
    We use different logging middleware depeding on weather we are running
    the server in a production or dev enviornment. Specificlly, we log all
    requests during development but in production, we only log requests that
    result in some error code. Also, response times are logged in dev mode but
	not in production. Note that this module expects the body field of the
    req object to be populated so this middleware should be mounted after any
    body parsing middleware.
*/


var fs = require('fs');

module.exports = function (app) {
	var logStream = fs.createWriteStream('dev_log/log.txt', { "flags": "a" });
	app.use((req, res, next) => {
		var start = +new Date();

		res.on('finish', () => {
			var duration = +new Date() - start;
			logStream.write(Date() + " " + req.method + " " + req.url + "\n" +
				JSON.stringify(req.body, null, 4) + "\n" +
				"Response Time: " + duration + " ms." + "\n\n");
		});
		next();
	});
}
