/*
    This is a proper error handler that must be the last function in the middleware
    stack. It is called using from another function using return next(err) since
    this function's arguments takes an err object in addition to the standard req, res, next
    set. This function will print information about the request and the error to an error log
    and send a response to the client. The previous middleware is responsible for setting the status
    code and the response. This function is only for reporting.
*/

var fs = require('fs');

module.exports = function (err, req, res, next) {

    var errorLogStream = fs.createWriteStream('error_log/error_log.txt', { "flags": "a" });

    errorLogStream.write("From: " + req.ip + " on " + Date() + "\n" +
        "Request:\n" + req.method + " " + req.url + "\n\n" +
        JSON.stringify(req.body, null, 4) + "\n" +
        "Response:\n" + res._header +
        "Server error:\n" + err + "\n\n\n\n");
}
