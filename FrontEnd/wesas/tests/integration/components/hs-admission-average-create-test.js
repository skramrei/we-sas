import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('hs-admission-average-create', 'Integration | Component | hs admission average create', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{hs-admission-average-create}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#hs-admission-average-create}}
      template block text
    {{/hs-admission-average-create}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
