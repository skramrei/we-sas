import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('itr-program-generate', 'Integration | Component | itr program generate', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{itr-program-generate}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#itr-program-generate}}
      template block text
    {{/itr-program-generate}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
