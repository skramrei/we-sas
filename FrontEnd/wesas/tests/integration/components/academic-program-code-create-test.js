import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('academic-program-code-create', 'Integration | Component | academic program code create', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{academic-program-code-create}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#academic-program-code-create}}
      template block text
    {{/academic-program-code-create}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
