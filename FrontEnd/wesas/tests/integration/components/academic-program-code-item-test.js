import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('academic-program-code-item', 'Integration | Component | academic program code item', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{academic-program-code-item}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#academic-program-code-item}}
      template block text
    {{/academic-program-code-item}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
