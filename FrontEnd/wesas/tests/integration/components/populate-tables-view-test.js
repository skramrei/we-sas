import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('populate-tables-view', 'Integration | Component | populate tables view', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{populate-tables-view}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#populate-tables-view}}
      template block text
    {{/populate-tables-view}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
