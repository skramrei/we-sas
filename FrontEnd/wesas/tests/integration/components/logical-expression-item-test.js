import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('logical-expression-item', 'Integration | Component | logical expression item', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{logical-expression-item}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#logical-expression-item}}
      template block text
    {{/logical-expression-item}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
