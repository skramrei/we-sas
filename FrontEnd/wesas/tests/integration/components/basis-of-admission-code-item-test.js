import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('basis-of-admission-code-item', 'Integration | Component | basis of admission code item', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{basis-of-admission-code-item}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#basis-of-admission-code-item}}
      template block text
    {{/basis-of-admission-code-item}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
