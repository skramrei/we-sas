import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('scholar-award-code-item', 'Integration | Component | scholar award code item', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{scholar-award-code-item}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#scholar-award-code-item}}
      template block text
    {{/scholar-award-code-item}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
