import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('logical-expression-item-linked', 'Integration | Component | logical expression item linked', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{logical-expression-item-linked}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#logical-expression-item-linked}}
      template block text
    {{/logical-expression-item-linked}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
