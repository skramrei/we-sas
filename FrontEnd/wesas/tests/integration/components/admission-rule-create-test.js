import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('admission-rule-create', 'Integration | Component | admission rule create', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{admission-rule-create}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#admission-rule-create}}
      template block text
    {{/admission-rule-create}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
