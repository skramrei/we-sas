import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('admission-rule-item', 'Integration | Component | admission rule item', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{admission-rule-item}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#admission-rule-item}}
      template block text
    {{/admission-rule-item}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
