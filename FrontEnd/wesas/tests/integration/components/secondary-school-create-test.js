import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('secondary-school-create', 'Integration | Component | secondary school create', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{secondary-school-create}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#secondary-school-create}}
      template block text
    {{/secondary-school-create}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
