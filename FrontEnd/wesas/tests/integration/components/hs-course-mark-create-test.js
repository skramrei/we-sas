import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('hs-course-mark-create', 'Integration | Component | hs course mark create', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{hs-course-mark-create}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#hs-course-mark-create}}
      template block text
    {{/hs-course-mark-create}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
