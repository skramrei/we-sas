import { fastEach } from 'wesas/helpers/fast-each';
import { module, test } from 'qunit';

module('Unit | Helper | fast each');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = fastEach([42]);
  assert.ok(result);
});
