import { getValues } from 'wesas/helpers/get-values';
import { module, test } from 'qunit';

module('Unit | Helper | get values');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = getValues([42]);
  assert.ok(result);
});
