import { getName } from 'wesas/helpers/get-name';
import { module, test } from 'qunit';

module('Unit | Helper | get name');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = getName([42]);
  assert.ok(result);
});
