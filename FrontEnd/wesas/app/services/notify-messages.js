import Ember from 'ember';

export default Ember.Service.extend({
  createSuccess: function (model, name) {
    return model + ': ' + name + ' created.';
  },
  genCreateSuccess: function (model) {
    return model + ' created.';
  },
  createFail: function (name) {
    return 'Something went wrong, ' + name + ' could not be created. Try refeshing the page.';
  },
  editSuccess: function (model, name) {
    return model + ': ' + name + ' edited.';
  },
  editFail: function (name) {
    return 'Something went wrong, ' + name + ' could not be edited. Try refreshing the page.';
  },
  deleteSuccess: function (model) {
    return model + ' deleted';
  },
  deleteFail: function (model) {
    return 'Something went wrong, ' + model + ' could not be deleted. Try refreshing the page.';
  },
  noChanges: function (model) {
    return 'You did not make any changes to that ' + model + '.';
  },
  login: function () {
    return 'Please log in to view that content.';
  }
});
