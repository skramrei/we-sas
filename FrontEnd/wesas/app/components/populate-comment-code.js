import Ember from 'ember';
import _ from 'npm:underscore';
import Sequence from 'npm:sequence';


export default Ember.Component.extend({

    store: Ember.inject.service(),

    change: function (e) {
        e.stopPropagation();
        e.preventDefault();

        var files = e.target.files;
        var errors = [];
        this.$('.ui.error.message').remove();

        outer:
            for (let i = 0; i < files.length; i++) {
                if (files[i].type !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                    errors.push('<li>The file ' + files[i].name + ' is not an excel spreadsheet</li>');
                    continue;
                }

                var fileNames = this.get('fileNames');
                var correctFileName = false;
                for (let j = 0; j < fileNames.length; j++) {
                    if (fileNames[j] === files[i].name.toLowerCase()) {
                        correctFileName = true;
                    }
                }
                if (!correctFileName) {
                    errors.push('<li>The excel spreadsheet ' + files[i].name + ' does not contain associated comment code data</li>');
                    continue;
                }

                var existingFiles = this.get('files');
                for (let j = 0; j < existingFiles.length; j++) {
                    if (existingFiles[j].name.toLowerCase() === files[i].name.toLowerCase()) {
                        errors.push('<li>The excel spreadsheet ' + files[i].name + ' has already been uploaded</li>');
                        continue outer;
                    }
                }
                this.$('.ui.file-upload.grid .fourteen.wide.column').hide();
                this.$('.ui.file-upload.grid').append('<div class="column file">' +
                    '<div class="ui segment">' +
                    '<div class="ui item">' +
                    '<i class="file excel outline icon"></i>' + files[i].name + '</div></div></div>');
                this.get('files').push(files[i]);
            }

        if (errors.length) {
            var message = '<div class="ui error message">' +
                '<div class="header">Error: There was a problem with one or more files uploaded</div>' +
                '<ul class="list">';
            for (let i = 0; i < errors.length; i++) {
                message += errors[i];
            }
            message += '</ul></div>';
            this.$('.ui.file-upload.grid').after(message);
        }
    },
    dragover: function (e) {
        e.stopPropagation();
        e.preventDefault();
        e.dataTransfer.dropEffect = 'copy';
    },
    drop: function (e) {
        e.stopPropagation();
        e.preventDefault();

        var files = e.dataTransfer.files;
        var errors = [];
        this.$('.ui.error.message').remove();

        outer:
            for (let i = 0; i < files.length; i++) {
                if (files[i].type !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                    errors.push('<li>The file ' + files[i].name + ' is not an excel spreadsheet</li>');
                    continue;
                }

                var fileNames = this.get('fileNames');
                var correctFileName = false;
                for (let j = 0; j < fileNames.length; j++) {
                    if (fileNames[j] === files[i].name.toLowerCase()) {
                        correctFileName = true;
                    }
                }
                if (!correctFileName) {
                    errors.push('<li>The excel spreadsheet ' + files[i].name + ' does not contain associated comment code data</li>');
                    continue;
                }

                var existingFiles = this.get('files');
                for (let j = 0; j < existingFiles.length; j++) {
                    if (existingFiles[j].name.toLowerCase() === files[i].name.toLowerCase()) {
                        errors.push('<li>The excel spreadsheet ' + files[i].name + ' has already been uploaded</li>');
                        continue outer;
                    }
                }
                this.$('.ui.file-upload.grid .fourteen.wide.column').hide();
                this.$('.ui.file-upload.grid').append('<div class="column file">' +
                    '<div class="ui segment">' +
                    '<div class="ui item">' +
                    '<i class="file excel outline icon"></i>' + files[i].name + '</div></div></div>');
                this.get('files').push(files[i]);
            }

        if (errors.length) {
            var message = '<div class="ui error message">' +
                '<div class="header">Error: There was a problem with one or more files uploaded</div>' +
                '<ul class="list">';
            for (let i = 0; i < errors.length; i++) {
                message += errors[i];
            }
            message += '</ul></div>';
            this.$('.ui.file-upload.grid').after(message);
        }
    },

    fileNames: ['commentcodes.xlsx'],
    files: [],
    uploadPercent: 0,

    actions: {
        file: function () {
            this.$('#comment-code-file').trigger('click');
        },
        upload: function () {

            var files = this.get('files');
            let commentCodeCount = 0;

            if (files.length !== 1) {
                this.$('.ui.error.basic.modal').modal('show');
                return;
            }

            var csvData = [];
            var self = this;

            Sequence.Sequence.create()
                .then(function (next) {
                    self.$('.ui.upload.modal').modal('show');
                    self.set('uploadPercent', 100);
                    self.$('.ui.upload.progress .label').html('Parsing Excel Spreadsheets');
                    setTimeout(function () {
                        next();
                    }, 1000);
                })
                .then(function (next) {

                    for (let i = 0; i < files.length; i++) {

                        var reader = new FileReader();

                        reader.onload = (function (file) {

                            var name = file.name.toLowerCase();

                            return function (e) {

                                var data = e.target.result;
                                var workbook = XLSX.read(data, {type: 'binary'});
                                var sheet = workbook.Sheets[workbook.SheetNames[0]];

                                csvData.push({
                                    name: name,
                                    data: XLSX.utils.sheet_to_csv(sheet)
                                });
                                if (csvData.length === files.length) {
                                    self.$('.ui.upload.progress .bar').html('<div class="progress"></div>');
                                    self.set('uploadPercent', 0);
                                    self.$('.ui.upload.progress .label').html('Creating Comment Codes');
                                    setTimeout(function () {
                                        next();
                                    }, 1000);
                                }
                            };
                        })(files[i]);
                        reader.readAsBinaryString(files[i]);
                    }
                })
                .then(function () {
                  Papa.parse(_.findWhere(csvData, {name: 'commentcodes.xlsx'}).data, {
                        header: true,
                        complete: function (results) {
                            let count = 0;
                            for (let i = 0; i < results.data.length; i++) {

                                var data = results.data[i];

                                if (data.code) {
                                    self.get('store').createRecord('comment-code', {
                                        code: data.code,
                                        progAction: data.progAction,
                                        description: data.description,
                                        notes: data.notes
                                    }).save().then(function () {
                                        ++commentCodeCount;
                                        if (++count === results.data.length) {
                                            self.set('uploadPercent', 100);
                                            self.$('.ui.upload.progress .label').html('Done');
                                            self.$('.ui.small.upload.modal .actions').html(
                                                '<button class="ui green deny button">' +
                                                '<i class="checkmark icon"></i>Ok' +
                                                '</button>');
                                            self.$('.ui.small.upload .content').append(
                                                '<div class="ui info message">' +
                                                '<div class="header">Results of Uploading Data</div>' +
                                                '<ul>' +
                                                '<li>Number of comment codes uploaded: ' + commentCodeCount + '</li>' +
                                                '</ul>' +
                                                '</div>'
                                            );
                                            self.$('.ui.error.message').remove();
                                            self.$('.ui.file-upload.grid .column.file').remove();
                                            self.$('.ui.file-upload.grid .fourteen.wide.column').show();
                                            self.set('files', []);
                                        }
                                    }, function () {
                                        self.$('.ui.upload.progress .label').html('Error Creating Provinces');
                                        self.$('.ui.upload.progress').addClass('error');
                                    });
                                } else {
                                    ++count;
                                }
                            }
                        }
                    });
                });
        },
        clear: function () {
            this.$('.ui.error.message').remove();
            this.$('.ui.file-upload.grid .column.file').remove();
            this.$('.ui.file-upload.grid .fourteen.wide.column').show();
            this.set('files', []);
        },
        closeError: function () {
            this.$('.ui.error.basic.modal').modal('hide');
        }
    }

});
