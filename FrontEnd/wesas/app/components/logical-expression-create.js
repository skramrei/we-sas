import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    classNames: ['fullWidth'], // CSS class added this this component's div in the DOM

    showExpression: false,
    notCanceled: true,
    leftOperatorIndex: null,
    booleanOperator: null,
    logicalLink: null,
    leftOperatorError: false,
    booleanError: false,
    valError: false,
    valErrorMessage: "",
    logicalLinkError: false,
    leftOperatorText: "",
    booleanOperatorText: "",
    valueText: "",
    logicalLinkText: "",

    courses: function () {
        return this.get('store').findAll('course-code');
    }.property(),

    actions: {
        setLeftOperator: function (component, index, value) {
            this.set('showExpression', true);
            this.set('leftOperatorIndex', index);
            this.set('leftOperatorError', false);
            this.$('#left-operator-field').removeClass('field-error');

            if (index === 'size') {
                this.$('#boolean-operator-field').addClass('disabled');
                this.set('booleanError', false);
                this.$('#boolean-operator-field').removeClass('field-error');
            } else {
                this.$('#boolean-operator-field').removeClass('disabled');
            }

            // Update expression building view
            if (index === 'size' || index === 'average') {
                this.set('leftOperatorText', value); // value contains the text displayed for the option selected
            } else {
                // Left operator must be a course id
                this.set('leftOperatorText', this.get('store').peekRecord('course-code', index).get('name') + ' Mark');
            }
        },
        setBooleanOperator: function (component, index, value) {
            this.set('showExpression', true);
            this.set('booleanOperator', index);
            this.set('booleanError', false);
            this.$('#boolean-operator-field').removeClass('field-error');

            // Update expression building view
            this.set('booleanOperatorText', value);
        },
        setLogicalLink: function (component, index, value) {
            this.set('showExpression', true);
            this.set('logicalLink', value);
            this.set('logicalLinkError', false);
            this.$('#link-field').removeClass('field-error');

            // Update expression building view only if there is a logical link selected
            if (index !== 'none') {
                this.set('logicalLinkText', value);
            }
        },
        onNameInput: function (value) {
            // This just removes the error indicators once a person enters a number, the value is handled in submitExpression() as val
            if (!isNaN(value)) {
                this.set('valError', false);
                this.$('#value-field').removeClass('field-error');
                this.set('showExpression', true);
                this.set('valueText', value);
            } else {
                this.set('valError', true);
                this.$('#value-field').addClass('field-error');
                this.set('valErrorMessage', 'Please enter a number');
            }
        },
        submitExpression: function (val) {

            // Turn off the warnings if they were on and remove the feild-error class from all fields
            // TODO: Try removing this since error indicators are already removed when correct values are entered in input event handlers
            this.set('leftOperatorError', false);
            this.$('#left-operator-field').removeClass('field-error');
            this.set('booleanError', false);
            this.$('#boolean-operator-field').removeClass('field-error');
            this.set('valError', false);
            this.$('#value-field').removeClass('field-error');
            this.set('logicalLinkError', false);
            this.$('#link-field').removeClass('field-error');

            // All the values in the form fields should have been filled in. End function if any are empty.
            // Also mark any fields as having an error using custom css class.
            var foundInvalidFields = false;
            if (this.get('leftOperatorIndex') === null) {
                this.set('leftOperatorError', true);
                this.$('#left-operator-field').addClass('field-error');
                foundInvalidFields = true;
            }
            if (this.get('booleanOperator') === null && this.get('leftOperatorIndex') !== 'size') {
                this.set('booleanError', true);
                this.$('#boolean-operator-field').addClass('field-error');
                foundInvalidFields = true;
            }
            if (val === undefined) {
                this.set('valError', true);
                this.$('#value-field').addClass('field-error');
                this.set('valErrorMessage', 'Please enter a value');
                foundInvalidFields = true;
            }
            if (this.get('logicalLink') === null && this.get('isLinked')) {
                this.set('logicalLinkError', true);
                this.$('#link-field').addClass('field-error');
                foundInvalidFields = true;
            }
            if (foundInvalidFields) {
                return;
            }

         //   var store = this.get('store');
            var leftOperatorIndex = this.get('leftOperatorIndex');

            // Make sure val is a valid number
            if (leftOperatorIndex !== "size") {
                // val should be in the range 0-100 because it is a mark or weighted average
                if (isNaN(val)) {
                    this.set('valError', true);
                    this.$('#value-field').addClass('field-error');
                    this.set('valErrorMessage', 'Plase enter a number');
                    return;
                } else {
                    if (val < 0 || val > 100) {
                        //this.set('badVal', true);
                        this.set('valError', true);
                        this.$('#value-field').addClass('field-error');
                        this.set('valErrorMessage', 'Average must be between 0 and 100');
                        return;
                    }
                }
            } else {
                // val should be a positive number
                if (isNaN(val)) {
                    //this.set('badVal', true);
                    this.set('valError', true);
                    this.$('#value-field').addClass('field-error');
                    this.set('valErrorMessage', 'Please enter a number of students');
                    return;
                } else {
                    // val is a number, now just make sure it is positive
                    if (val < 0) {
                        //this.set('badVal', true);
                        this.set('valError', true);
                        this.$('#value-field').addClass('field-error');
                        this.set('valErrorMessage', 'Number of students needs to be positive');
                        return;
                    }
                }
            }

            // All data entered in valid so build the expression and related data
            var booleanExpression = leftOperatorIndex + " " + this.get('booleanOperator') + " " + val;
            var link = this.get('logicalLink');

            // Call the onConfirm property that a parent component sets passing the expression data and reset values.
            if (this.get('isLinked')) {
                this.get('onConfirm')(booleanExpression, link, this.get('isForFull'), false);
            }
            else {
                this.get('onConfirm')(booleanExpression, 'None', this.get('isForFull'), false);
            }
            this.set('notCanceled', false);
            this.set('leftOperatorText', '');
            this.set('booleanOperatorText', '');
            this.set('valueText', '');
            this.set('logicalLinkText', '');
        },

        cancelExpressionCreate: function () {
            this.set('notCanceled', false);
            this.set('leftOperatorError', false);
            this.$('#left-operator-field').removeClass('field-error');
            this.set('booleanError', false);
            this.$('#boolean-operator-field').removeClass('field-error');
            this.set('logicalLinkError', false);
            this.$('#link-field').removeClass('field-error');
            this.set('logicalLinkError', false);
            this.$('#link-field').removeClass('field-error');
            this.set('leftOperatorText', '');
            this.set('booleanOperatorText', '');
            this.set('valueText', '');
            this.set('logicalLinkText', '');

            this.get('onConfirm')(null, null, null, true);
        }

    }
});
