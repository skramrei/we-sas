import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    isCreating: false,
    inputError: false,

    actions: {
        onNameInput: function (value) {
            // Remove error indications on input
          value = value;
            this.set('inputError', false);
            this.$('#country-create-name').removeClass('field-error');
        },
        create: function () {
            this.set('isCreating', true);
        },
        cancel: function () {
            this.set('isCreating', false);
            this.set('name', '');
            this.set('inputError', false);
            this.$('#country-create-name').removeClass('field-error');
        },
        submit: function (name) {

            this.set('inputError', false);
            this.$('#country-create-name').removeClass('field-error');

            if (name === undefined || name === null || name === "") {
                this.set('inputError', true);
                this.$('#country-create-name').addClass('field-error');
                return;
            }

            var self = this;

            this.get('store').createRecord('country', {
                name: name
            }).save().then(function () {
                self.get('notify').success(self.get('notifyMessages').createSuccess('Country', name));
            }, function () {
                self.get('notify').alert(self.get('notifyMessages').createFail(name), {closeAfter: null});
            });

            this.set('name', '');
            this.set('isCreating', false);
        }
    }
});
