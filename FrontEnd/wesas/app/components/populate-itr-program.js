import Ember from 'ember';
import _ from 'npm:underscore';
import Sequence from 'npm:sequence';

export default Ember.Component.extend({

    store: Ember.inject.service(),

    change: function (e) {
        e.stopPropagation();
        e.preventDefault();

        var files = e.target.files;
        var errors = [];
        this.$('.ui.error.message').remove();

        outer:
            for (let i = 0; i < files.length; i++) {
                if (files[i].type !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                    errors.push('<li>The file ' + files[i].name + ' is not an excel spreadsheet</li>');
                    continue;
                }

                var fileNames = this.get('fileNames');
                var correctFileName = false;
                for (let j = 0; j < fileNames.length; j++) {
                    if (fileNames[j] === files[i].name.toLowerCase()) {
                        correctFileName = true;
                    }
                }
                if (!correctFileName) {
                    errors.push('<li>The excel spreadsheet ' + files[i].name + ' does not contain associated ITR program data</li>');
                    continue;
                }

                var existingFiles = this.get('files');
                for (let j = 0; j < existingFiles.length; j++) {
                    if (existingFiles[j].name.toLowerCase() === files[i].name.toLowerCase()) {
                        errors.push('<li>The excel spreadsheet ' + files[i].name + ' has already been uploaded</li>');
                        continue outer;
                    }
                }
                this.$('.ui.file-upload.grid .fourteen.wide.column').hide();
                this.$('.ui.file-upload.grid').append('<div class="column file">' +
                    '<div class="ui segment">' +
                    '<div class="ui item">' +
                    '<i class="file excel outline icon"></i>' + files[i].name + '</div></div></div>');
                this.get('files').push(files[i]);
            }

        if (errors.length) {
            var message = '<div class="ui error message">' +
                '<div class="header">Error: There was a problem with one or more files uploaded</div>' +
                '<ul class="list">';
            for (let i = 0; i < errors.length; i++) {
                message += errors[i];
            }
            message += '</ul></div>';
            this.$('.ui.file-upload.grid').after(message);
        }
    },
    dragover: function (e) {
        e.stopPropagation();
        e.preventDefault();
        e.dataTransfer.dropEffect = 'copy';
    },
    drop: function (e) {
        e.stopPropagation();
        e.preventDefault();

        var files = e.dataTransfer.files;
        var errors = [];
        this.$('.ui.error.message').remove();

        outer:
            for (let i = 0; i < files.length; i++) {
                if (files[i].type !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                    errors.push('<li>The file ' + files[i].name + ' is not an excel spreadsheet</li>');
                    continue;
                }

                var fileNames = this.get('fileNames');
                var correctFileName = false;
                for (let j = 0; j < fileNames.length; j++) {
                    if (fileNames[j] === files[i].name.toLowerCase()) {
                        correctFileName = true;
                    }
                }
                if (!correctFileName) {
                    errors.push('<li>The excel spreadsheet ' + files[i].name + ' does not contain associated ITR program data</li>');
                    continue;
                }

                var existingFiles = this.get('files');
                for (let j = 0; j < existingFiles.length; j++) {
                    if (existingFiles[j].name.toLowerCase() === files[i].name.toLowerCase()) {
                        errors.push('<li>The excel spreadsheet ' + files[i].name + ' has already been uploaded</li>');
                        continue outer;
                    }
                }
                this.$('.ui.file-upload.grid .fourteen.wide.column').hide();
                this.$('.ui.file-upload.grid').append('<div class="column file">' +
                    '<div class="ui segment">' +
                    '<div class="ui item">' +
                    '<i class="file excel outline icon"></i>' + files[i].name + '</div></div></div>');
                this.get('files').push(files[i]);
            }

        if (errors.length) {
            var message = '<div class="ui error message">' +
                '<div class="header">Error: There was a problem with one or more files uploaded</div>' +
                '<ul class="list">';
            for (let i = 0; i < errors.length; i++) {
                message += errors[i];
            }
            message += '</ul></div>';
            this.$('.ui.file-upload.grid').after(message);
        }
    },

    fileNames: ['faculties.xlsx', 'departments.xlsx', 'programadministrations.xlsx', 'academicprogramcodes.xlsx', 'admissionrules.xlsx', 'itrprograms.xlsx'],
    files: [],
    uploadPercent: 0,

    actions: {
        file: function () {
            this.$('#itr-program-file').trigger('click');
        },
        upload: function () {

            var files = this.get('files');
            let facultyCount = 0,
                departmentCount = 0,
                programAdministrationCount = 0,
                academicProgramCodeCount = 0,
                admissionRuleCount = 0,
                itrProgramCount = 0;

            if (files.length !== 6) {
                this.$('.ui.error.basic.modal').modal('show');
                return;
            }

            var csvData = [];
            var self = this;

            Sequence.Sequence.create()
                .then(function (next) {
                    self.get('store').findAll('commentCode').then((commentCodes) => {
                        next();
                    });
                })
                .then(function (next) {
                    self.$('.ui.upload.modal').modal('show');
                    self.set('uploadPercent', 100);
                    self.$('.ui.upload.progress .label').html('Parsing Excel Spreadsheets');
                    setTimeout(function () {
                        next();
                    }, 1000);
                })
                .then(function (next) {

                    for (let i = 0; i < files.length; i++) {

                        var reader = new FileReader();

                        reader.onload = (function (file) {

                            var name = file.name.toLowerCase();

                            return function (e) {

                                var data = e.target.result;
                                var workbook = XLSX.read(data, {type: 'binary'});
                                var sheet = workbook.Sheets[workbook.SheetNames[0]];

                                csvData.push({
                                    name: name,
                                    data: XLSX.utils.sheet_to_csv(sheet)
                                });
                                if (csvData.length === files.length) {
                                    self.$('.ui.upload.progress .bar').html('<div class="progress"></div>');
                                    self.set('uploadPercent', 4);
                                    self.$('.ui.upload.progress .label').html('Creating Faculties');
                                    setTimeout(function () {
                                        next();
                                    }, 1000);
                                }
                            };
                        })(files[i]);
                        reader.readAsBinaryString(files[i]);
                    }
                })
                .then(function (next) {
                  Papa.parse(_.findWhere(csvData, {name: 'faculties.xlsx'}).data, {
                        header: true,
                        complete: function (results) {
                            let count = 0;
                            for (let i = 0; i < results.data.length; i++) {

                                var data = results.data[i];

                                if (data.name) {
                                    self.get('store').createRecord('faculty', {
                                        name: data.name
                                    }).save().then(function () {
                                        ++facultyCount;
                                        if (++count === results.data.length) {
                                            self.set('uploadPercent', 20);
                                            self.$('.ui.upload.progress .label').html('Creating Departments');
                                            setTimeout(function () {
                                                next();
                                            }, 1000);
                                        }
                                    }, function () {
                                        self.$('.ui.upload.progress .label').html('Error Creating Faculties');
                                        self.$('.ui.upload.progress').addClass('error');
                                    });
                                } else {
                                    ++count;
                                }
                            }
                        }
                    });
                })
                .then(function (next) {
                  Papa.parse(_.findWhere(csvData, {name: 'departments.xlsx'}).data, {
                        header: true,
                        complete: function (results) {
                            let count = 0;
                            for (let i = 0; i < results.data.length; i++) {

                                var data = results.data[i];

                                if (data.name && data.faculty) {
                                    (function (data) {

                                        let faculty = null;

                                        Sequence.Sequence.create()
                                            .then(function (next) {
                                                self.get('store').queryRecord('faculty', {name: data.faculty}).then(function (res) {
                                                    faculty = res;
                                                    next();
                                                });
                                            })
                                            .then(function () {
                                                self.get('store').createRecord('department', {
                                                    name: data.name,
                                                    faculty: faculty
                                                }).save().then(function () {
                                                    ++departmentCount;
                                                    if (++count === results.data.length) {
                                                        self.set('uploadPercent', 36);
                                                        self.$('.ui.upload.progress .label').html('Creating Admission Rules');
                                                        setTimeout(function () {
                                                            next();
                                                        }, 1000);
                                                    }
                                                }, function () {
                                                    self.$('.ui.upload.progress .label').html('Error Creating Departments');
                                                    self.$('.ui.upload.progress').addClass('error');
                                                });
                                            });
                                    })(data);
                                } else {
                                    ++count;
                                }
                            }
                        }
                    });
                })
                .then(function (next) {
                  Papa.parse(_.findWhere(csvData, {name: 'admissionrules.xlsx'}).data, {
                        header: true,
                        complete: function (results) {
                            let count = 0;
                            for (let i = 0; i < results.data.length; i++) {

                                var data = results.data[i];

                                if (data.description) {
                                    self.get('store').createRecord('admission-rule', {
                                        description: data.description
                                    }).save().then(function () {
                                        ++admissionRuleCount;
                                        if (++count === results.data.length) {
                                            self.set('uploadPercent', 52);
                                            self.$('.ui.upload.progress .label').html('Creating Academic Program Codes');
                                            setTimeout(function () {
                                                next();
                                            }, 1000);
                                        }
                                    }, function () {
                                        self.$('.ui.upload.progress .label').html('Error Creating Admission Rules');
                                        self.$('.ui.upload.progress').addClass('error');
                                    });
                                } else {
                                    ++count;
                                }
                            }
                        }
                    });
                })
                .then(function (next) {
                  Papa.parse(_.findWhere(csvData, {name: 'academicprogramcodes.xlsx'}).data, {
                        header: true,
                        complete: function (results) {
                            let count = 0;
                            for (let i = 0; i < results.data.length; i++) {

                                var data = results.data[i];
                                var commentCode;
                                self.get('store').peekAll('commentCode').forEach((cc) => {
                                    if (data.commentCode === cc.get('code')) {
                                        commentCode = cc;
                                    }
                                });
                                if (data.code) {
                                    self.get('store').createRecord('academic-program-code', {
                                        code: data.code + '',
                                        subCode: data.subCode + '',
                                        name: data.name,
                                        commentCode: commentCode
                                    }).save().then(function () {
                                        ++academicProgramCodeCount;
                                        if (++count === results.data.length) {
                                            self.set('uploadPercent', 68);
                                            self.$('.ui.upload.progress .label').html('Creating ITR Programs');
                                            setTimeout(function () {
                                                next();
                                            }, 1000);
                                        }
                                    }, function () {
                                        self.$('.ui.upload.progress .label').html('Error Creating Academic Program Codes');
                                        self.$('.ui.upload.progress').addClass('error');
                                    });
                                } else {
                                    ++count;
                                }
                            }
                        }
                    });
                })
                .then(function (next) {
                  Papa.parse(_.findWhere(csvData, {name: 'itrprograms.xlsx'}).data, {
                      header: true,
                      complete: function (results) {
                          let count = 0;
                          for (let i = 0; i < results.data.length; i++) {

                              var data = results.data[i];

                              if (data.student && data.code && data.choice) {
                                  (function (data) {

                                      let studentObj = null,
                                          academicProgramCodeObj = null;

                                      Sequence.Sequence.create()
                                          .then(function (next) {
                                              self.get('store').queryRecord('student', {studentNumber: data.student}).then(function (res) {
                                                  studentObj = res;
                                                  next();
                                              });
                                          })
                                          .then(function (next) {
                                              let code = data.subCode ? data.code + data.subCode : data.code;
                                              self.get('store').queryRecord('academic-program-code', {code: code}).then(function (res) {
                                                  academicProgramCodeObj = res;
                                                  next();
                                              });
                                          })
                                          .then(function () {
                                              self.get('store').createRecord('itr-program', {
                                                  order: Number(data.choice),
                                                  eligibility: false,
                                                  student: studentObj,
                                                  program: academicProgramCodeObj
                                              }).save().then(function () {
                                                  ++itrProgramCount;
                                                  if (++count === results.data.length) {
                                                      self.set('uploadPercent', 84);
                                                      self.$('.ui.upload.progress .label').html('Creating Program Administrations');
                                                      setTimeout(function () {
                                                          next();
                                                      }, 1000);
                                                  }
                                              }, function () {
                                                  self.$('.ui.upload.progress .label').html('Error Creating ITR programs');
                                                  self.$('.ui.upload.progress').addClass('error');
                                              })
                                          });
                                  })(data);
                              } else {
                                  ++count;
                              }
                          }
                      }
                  });
                })
                .then(function () {
                  Papa.parse(_.findWhere(csvData, {name: 'programadministrations.xlsx'}).data, {
                        header: true,
                        complete: function (results) {
                            let count = 0;
                            for (let i = 0; i < results.data.length; i++) {

                                var data = results.data[i];

                                if (data.name && data.position) {
                                    (function (data) {

                                        let departmnetObj = null,
                                            academicProgramCodeObj = null;

                                        Sequence.Sequence.create()
                                            .then(function (next) {
                                                self.get('store').queryRecord('department', {name: data.department}).then(function (res) {
                                                    departmnetObj = res;
                                                    next();
                                                });
                                            })
                                            .then(function (next) {
                                                self.get('store').queryRecord('academic-program-code', {code: data.academicProgramCode}).then(function (res) {
                                                    academicProgramCodeObj = res;
                                                    next();
                                                });
                                            })
                                            .then(function () {
                                                self.get('store').createRecord('program-administration', {
                                                    name: data.name,
                                                    position: data.position,
                                                    department: departmnetObj,
                                                    academicProgramCode: academicProgramCodeObj
                                                }).save().then(function () {
                                                    ++programAdministrationCount;
                                                    if (++count === results.data.length) {
                                                        self.set('uploadPercent', 100);
                                                        self.$('.ui.upload.progress .label').html('Done');
                                                        self.$('.ui.small.upload.modal .actions').html(
                                                            '<button class="ui green deny button">' +
                                                            '<i class="checkmark icon"></i>Ok' +
                                                            '</button>');
                                                        self.$('.ui.small.upload .content').append(
                                                            '<div class="ui info message">' +
                                                            '<div class="header">Results of Uploading Data</div>' +
                                                            '<ul>' +
                                                            '<li>Number of faculties uploaded: ' + facultyCount + '</li>' +
                                                            '<li>Number of departments uploaded: ' + departmentCount + '</li>' +
                                                            '<li>Number of academic program codes uploaded: ' + academicProgramCodeCount + '</li>' +
                                                            '<li>Number of admission rules uploaded: ' + admissionRuleCount + '</li>' +
                                                            '<li>Number of ITR programs uploaded: ' + itrProgramCount + '</li>' +
                                                            '<li>Number of program administrations uploaded: ' + programAdministrationCount + '</li>' +
                                                            '</ul>' +
                                                            '</div>'
                                                        );
                                                        self.$('.ui.error.message').remove();
                                                        self.$('.ui.file-upload.grid .column.file').remove();
                                                        self.$('.ui.file-upload.grid .fourteen.wide.column').show();
                                                        self.set('files', []);
                                                    }
                                                }, function () {
                                                    self.$('.ui.upload.progress .label').html('Error Creating Academic Program Codes');
                                                    self.$('.ui.upload.progress').addClass('error');
                                                });
                                            });
                                    })(data);
                                } else {
                                    ++count;
                                }
                            }
                        }
                    });
                });
        },
        clear: function () {
            this.$('.ui.error.message').remove();
            this.$('.ui.file-upload.grid .column.file').remove();
            this.$('.ui.file-upload.grid .fourteen.wide.column').show();
            this.set('files', []);
        },
        closeError: function () {
            this.$('.ui.error.basic.modal').modal('hide');
        }
    }

});
