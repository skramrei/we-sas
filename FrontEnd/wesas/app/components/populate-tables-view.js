import Ember from 'ember';

export default Ember.Component.extend({

  didRender: function () {
    var self = this;
    this.$(document).ready(function () {
      self.$('.vertical.menu .item').tab({history: false});
    });
  }

});
