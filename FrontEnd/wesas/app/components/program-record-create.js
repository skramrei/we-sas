import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    degreeCodes: function () {
        return this.get('store').findAll('degree-code');
    }.property(),
    termCodes: function () {
        return this.get('store').findAll('term-code');
    }.property(),

    isCreating: false,
    degreeCodeId: null,
    termCodeId: null,

    actions: {
        setDegreeCodeId: function (comp, id) {
            this.set('degreeCodeId', id);
        },
        setTermCodeId: function (comp, id) {
            this.set('termCodeId', id);
        },
        create: function () {
            this.set('isCreating', true);
        },
        cancel: function () {
            this.set('isCreating', false);
        },
        submit: function (level, status, comment) {

            var self = this;

            this.get('store').createRecord('program-record', {
                level: level,
                status: status,
                comment: comment,
                degreeCode: self.get('store').peekRecord('degree-code', self.get('degreeCodeId')),
                termCode: self.get('store').peekRecord('term-code', self.get('termCodeId'))
            }).save().then(function () {
                self.get('notify').success(self.get('notifyMessages').createSuccess('Program Record', level));
            }, function () {
                self.get('notify').alert(self.get('notifyMessages').createFail(level), {closeAfter: null});
            });

            this.set('level', '');
            this.set('status', '');
            this.set('comment', '');
            this.set('degreeCodeId', null);
            this.set('termCodeId', null);
            this.set('isCreating', false);
        }
    }
});
