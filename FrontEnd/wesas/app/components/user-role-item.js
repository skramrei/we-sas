import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),
    tagName: 'tr',

    willDestroyElement: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },
    didRender: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },

    mouseEnter: function () {
        this.$('td.right.aligned > div > button').css('visibility', 'visible');
    },
    mouseLeave: function () {
        this.$('td.right.aligned > div > button').css('visibility', 'hidden');
    },

    getDateAssigned: function () {

        var userRoles = this.get('userRoles');
        var userRole = userRoles.objectAt(this.get('i'));

        if (userRole) {
            return userRole.get('dateAssigned');
        } else {
            return new Date().toString();
        }
    }.property(),

    actions: {
        warning: function () {
            this.$('.ui.view.modal').modal('hide');
            this.$('.ui.small.user-role-remove.modal').modal({
                onDeny: function () {
                    return false;
                },
                onApprove: function () {
                    return false;
                }
            }).modal('show');
        },
        remove: function (id) {
            var userID = this.get('userID');
            var self = this;

            this.get('store').queryRecord('userRole', {filter: {user: userID, role: id}}).then(function (userRole) {
                userRole.user = null;
                userRole.role = null;
                userRole.save().then(function (toDelete) {
                    toDelete.destroyRecord().then(function () {
                        self.get('userRoleCodes').popObject(self.get('userRoleCode'));
                        self.get('notify').success(self.get('notifyMessages').deleteSuccess('User Role'));
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').deleteFail('User Role'), {closeAfter: null});
                    });
                });
            });
        }
    }
});
