import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),

    loadHSCourseMarks: function () {
        this.get('store').query('hs-course-mark', {}).then((hsCourseMarks) => {
          hsCourseMarks=hsCourseMarks;
          this.set('hsCourseMarks', this.get('store').findAll('hs-course-mark'));
            this.$('.ui.active.inverted.dimmer.view').hide();
        });
    }.property()

});
