import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),
    tagName: 'tr',

    mouseEnter: function () {
        if (!this.get('isEditing') && !this.get('isRemoving')) {
            this.$('td.right.aligned > button').css('visibility', 'visible');
        }
    },
    mouseLeave: function () {
        this.$('td.right.aligned > button').css('visibility', 'hidden');
    },

    willDestroyElement: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },
    didRender: function () {
        // NOTE: The content of the popup is the button's title element by default
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },

    isEditing: false,
    isRemoving: false,

    actions: {
        edit: function (name) {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('name', name);
            this.set('isEditing', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        warning: function () {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('isRemoving', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        cancel: function () {
            this.set('isEditing', false);
            this.set('isRemoving', false);
        },
        submit: function (id, name) {

            var self = this;

            this.get('store').find('faculty', id).then(function (faculty) {
                faculty.set('name', name);
                faculty.save().then(function () {
                    self.get('notify').success(self.get('notifyMessages').editSuccess('Faculty', name));
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').editFail(name), {closeAfter: null});
                });
            });

            this.set('name', '');
            this.set('isEditing', false);
        },
        remove: function (id) {

            var self = this;

            this.get('store').find('faculty', id).then(function (faculty) {

                for (var i = 0; i < faculty.departments.length; i++) {
                    faculty.departments[i] = null;
                }

                faculty.save().then(function () {
                    faculty.destroyRecord().then(function () {
                        self.get('notify').success(self.get('notifyMessages').deleteSuccess('Faculty'));
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').deleteFail('Faculty'), {closeAfter: null});
                    });
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').deleteFail('Faculty'), {closeAfter: null});
                });
            });

            this.set('isRemoving', false);
        }
    }
});
