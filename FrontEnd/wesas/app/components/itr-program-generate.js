import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    isGeneratingItrProgram: false,

    actions: {
    	generateItrData: function() {
            this.set('isGeneratingItrProgram', true);
        },
        generate: function() { 
            this.get('store').query('itr-program', {generate: true});
            this.set('isGeneratingItrProgram', false);
        },
        cancel: function() {
            this.set('isGeneratingItrProgram', false);
        }
    }
});
