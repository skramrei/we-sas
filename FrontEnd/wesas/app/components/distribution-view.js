import Ember from 'ember';

function loadData(comp, showUpdateNotification, showGenerateNotification) {

    comp.get('store').query('distribution-result', {date: comp.get('date')})
        .then((results) => {

            comp.get('store').queryRecord('distribution-info', {})
                .then((info) => {

                    comp.set('results', results);
                    comp.set('dates', info.get('dates').reverse());
                    comp.$('#distribution-result-loading').removeClass('active');

                    if (showUpdateNotification) {
                        if (comp.get('date') === 'latest') {
                            comp.get('notify').success('Showing latest distribution results');
                        } else {
                            comp.get('notify').success('Showing distibution results for ' + comp.get('date'));
                        }
                    }

                    if (showGenerateNotification) {
                        comp.get('notify').success('Distribution complete');
                    }

                });

        });

}

function makeGraph(comp) {
    var count = 0;
    comp.get('store').query('distribution-result', {date: comp.get('date')}).then((results) => {
        if (results.get('length')) {
            var students = {
                rejected: {
                    title: 'Not Eligible For Any Faculty',
                    value: 0
                }
            };

            results.forEach((distributionResult) => {
                if (distributionResult.get('rejected')) {
                    students.rejected.value++;
                    count++;
                } else {
                    distributionResult.get('commentCode').then((commentCode) => {
                        if (students[commentCode.get('code')]) {
                            students[commentCode.get('code')].value++;
                            count++;
                        } else {
                            students[commentCode.get('code')] = {
                                title: commentCode.get('description'),
                                value: 1
                            };
                            count++;
                        }
                    });
                }
            });

            var chartMade = false;
            var dataProvider = [];

            var interval = setInterval(() => {
                if (count === results.get('length')) {
                    clearInterval(interval);
                    if (!chartMade) {

                        for (var property in students) {
                            if (students.hasOwnProperty(property)) {
                                dataProvider.push({
                                    program: students[property].title,
                                    students: students[property].value
                                });
                            }
                        }

                        AmCharts.makeChart('chartdiv', {
                            type: 'serial',
                            theme: 'light',
                            dataProvider: dataProvider,
                            valueAxes: [{
                                "gridColor": "#FFFFFF",
                                "gridAlpha": 0.2,
                                "dashLength": 0
                            }],
                            gridAboveGraphs: true,
                            startDuration: 1,
                            valueAxes: [{
                                gridColor: "#FFFFFF",
                                gridAlpha: 0.2,
                                dashLength: 0
                            }],
                            graphs: [{
                                balloonText: "[[category]]: <b>[[value]]</b>",
                                fillAlphas: 0.8,
                                lineAlpha: 0.2,
                                type: "column",
                                valueField: "students"
                            }],
                            chartCursor: {
                                categoryBalloonEnabled: false,
                                cursorAlpha: 0,
                                zoomable: false
                            },
                            categoryField: "program",
                            categoryAxis: {
                                gridPosition: "start",
                                gridAlpha: 0,
                                tickPosition: "start",
                                tickLength: 20,
                                autoWrap: true
                            },
                            export: {
                                enabled: true
                            }
                        });
                        chartMade = true;
                    }
                }

            }, 100);

        } else {
            comp.set('viewingGraph', false);
            comp.get('notify').warning("There has not been a distribution yet. Please use the button above to distribute students");
            hideGraph(comp);
        }
    });
}

function showGraph(comp) {
    comp.set('viewingGraph', true);
    $('#view-graph-btn').html('<i class="bar chart icon"></i>Close Distribution Graph');
    makeGraph(comp);
}

function hideGraph(comp) {
    comp.set('viewingGraph', false);
    $('#view-graph-btn').html('<i class="bar chart icon"></i>View Distribution Graph');
}

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    date: 'latest',
    lastDropDownClickTime: 0,
    viewingGraph: false,

    didInsertElement: function () {
        loadData(this, false, false);
    },

    actions: {

        generateResult: function () {
            hideGraph(this);
            $('#distribution-result-loading').addClass('active');
            this.get('store').query('distribution-result', {generate: true}).then((results) => {
              results = results;
              this.set('date', 'latest');
                this.get('store').unloadAll('distribution-result');
                this.get('store').unloadAll('distribution-info');
                loadData(this, false, true);
            });
        },

        setTime: function (comp, value) {

            hideGraph(this);

            // On some computers this event is fired twice in a row only update the view once
            var currentTime = + new Date();

            if (currentTime - this.get('lastDropDownClickTime') > 100) {
                // Only act when clicks comes 100ms appart
                $('#distribution-result-loading').addClass('active');
                this.set('date', value);
                loadData(this, true, false);
            }

            this.set('lastDropDownClickTime', currentTime);

        },

        graphBtnClicked: function () {
            if(this.get('viewingGraph')) {
                hideGraph(this);
            } else {
                showGraph(this);
            }
        },

        createPDF: function () {

            var pdf = {
                content: [
                    {
                        text: 'Distribution Results of Students\n\n', style: 'header'
                    },
                    'The below table displays all students and the faculties that they are distributed into.\n\n',
                    {
                        table: {
                            headerRows: 1,
                            body: [
                                [{
                                    text: 'Date', style: 'tableHeader'
                                }, {
                                    text: 'Student Number', style: 'tableHeader'
                                }, {
                                    text: 'Student Name', style: 'tableHeader'
                                }, {
                                    text: 'Comment Code', style: 'tableHeader'
                                }, {
                                    text: 'Comment Description', style: 'tableHeader'
                                }]
                            ]
                        }
                    }
                ],
                styles: {
                    header: {
                        fontSize: 18,
                        bold: true
                    },
                    tableHeader: {
                        bold: true
                    }
                }
            };
            var self = this;
            $.each($('#distribution-res tr'), function (i, row) {
                var arr = [];
              $.each($(row).find('td'), function (j, cell) {
                    arr.push($(cell).text());
                });
                if (arr.length) {
                    pdf.content[2].table.body.push(arr);
                }
            });

            pdfMake.createPdf(pdf).open();
        }
    }

});
