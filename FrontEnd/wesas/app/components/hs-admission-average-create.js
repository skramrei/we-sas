import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    students: function () {
        return this.get('store').findAll('student');
    }.property(),

    isCreating: false,
    studentId: null,

    studentError: false,

    g11Error: false,
    g11ErrorMessage: '',
    firstError: false,
    firstErrorMessage: '',
    MYError: false,
    MYErrorMessage: '',
    finalError: false,
    finalErrorMessage: '',

    actions: {
        setStudentId: function (comp, id) {
            this.set('studentId', id);
            this.set('studentError', false);
            this.$('#student-field').removeClass('field-error');
        },
        onG11Input: function(value) {
            if (!isNaN(value) && value >= 0 && value <= 100) {
                this.set('g11Error', false);
                this.$('#g11-field').removeClass('field-error');
                //this.set('showExpression', true);
                //this.set('valueText', value);
            } else {
                this.set('g11Error', true);
                this.$('#g11-field').addClass('field-error');
                this.set('g11ErrorMessage', 'Please enter a number between 0 and 100');
            }
        },
        onFirstInput: function(value) {
            if (!isNaN(value) && value >= 0 && value <= 100) {
                this.set('firstError', false);
                this.$('#first-field').removeClass('field-error');
                //this.set('showExpression', true);
                //this.set('valueText', value);
            } else {
                this.set('firstError', true);
                this.$('#first-field').addClass('field-error');
                this.set('firstErrorMessage', 'Please enter a number between 0 and 100');
            }
        },
        onMYInput: function(value) {
            if (!isNaN(value) && value >= 0 && value <= 100) {
                this.set('MYError', false);
                this.$('#m-y-field').removeClass('field-error');
                //this.set('showExpression', true);
                //this.set('valueText', value);
            } else {
                this.set('MYError', true);
                this.$('#m-y-field').addClass('field-error');
                this.set('MYErrorMessage', 'Please enter a number between 0 and 100');
            }
        },
        onFinalInput: function(value) {
            if (!isNaN(value) && value >= 0 && value <= 100) {
                this.set('finalError', false);
                this.$('#final-field').removeClass('field-error');
                //this.set('showExpression', true);
                //this.set('valueText', value);
            } else {
                this.set('finalError', true);
                this.$('#final-field').addClass('field-error');
                this.set('finalErrorMessage', 'Please enter a number between 0 and 100');
            }
        },
        create: function () {
            this.set('isCreating', true);
        },
        cancel: function () {
            this.set('isCreating', false);

            this.set('studentError', false);
            this.set('g11Error', false);
            this.set('MYError', false);
            this.set('finalError', false);
            this.set('firstError', false);
        },
        submit: function (first, midYear, finalVal, grade11) {

            var self = this;
            var foundInvalidFields = false;

            // student must be set
            if (this.get('studentId')!==null) {
                this.set('studentError', false);
                this.$('#student-field').removeClass('field-error');
            } else {
                this.set('studentError', true);
                this.$('#student-field').addClass('field-error');
                foundInvalidFields = true;
            }

             // values should be in the range 0-100 because they are averages
            if (!isNaN(grade11) && grade11 >= 0 && grade11 <= 100 && grade11 !== '') {
                this.set('g11Error', false);
                this.$('#g11-field').removeClass('field-error');
                //this.set('showExpression', true);
                //this.set('valueText', value);
            } else {
                this.set('g11Error', true);
                this.$('#g11-field').addClass('field-error');
                this.set('g11ErrorMessage', 'Please enter a number between 0 and 100');
                foundInvalidFields = true;
            }
            if (!isNaN(first) && first >= 0 && first <= 100 && first !== '') {
                this.set('firstError', false);
                this.$('#first-field').removeClass('field-error');
                //this.set('showExpression', true);
                //this.set('valueText', value);
            } else {
                this.set('firstError', true);
                this.$('#first-field').addClass('field-error');
                this.set('firstErrorMessage', 'Please enter a number between 0 and 100');
                foundInvalidFields = true;
            }

            if (!isNaN(midYear) && midYear >= 0 && midYear <= 100 && midYear !== '') {
                this.set('MYError', false);
                this.$('#m-y-field').removeClass('field-error');
                //this.set('showExpression', true);
                //this.set('valueText', value);
            } else {
                this.set('MYError', true);
                this.$('#m-y-field').addClass('field-error');
                this.set('MYErrorMessage', 'Please enter a number between 0 and 100');
                foundInvalidFields = true;
            }

            if (!isNaN(finalVal) && finalVal >= 0 && finalVal <= 100 && finalVal !== '') {
                this.set('finalError', false);
                this.$('#final-field').removeClass('field-error');
                //this.set('showExpression', true);
                //this.set('valueText', value);
            } else {
                this.set('finalError', true);
                this.$('#final-field').addClass('field-error');
                this.set('finalErrorMessage', 'Please enter a number between 0 and 100');
                foundInvalidFields = true;
            }

            if (foundInvalidFields) {
                return; // if the variable is true, do not continue
            }

            this.get('store').createRecord('hsAdmissionAverage', {
                first: first,
                midYear: midYear,
                finalVal: finalVal,
                grade11: grade11,
                student: self.get('store').peekRecord('student', this.get('studentId'))
            }).save().then(function () {
                self.get('notify').success(self.get('notifyMessages').createSuccess('High School Admission Average', ' '));
            }, function () {
                self.get('notify').alert(self.get('notifyMessages').createFail(' '), {closeAfter: null});
            });

            this.set('first', '');
            this.set('midYear', '');
            this.set('grade11', '');
            this.set('finalVal', '');
            this.set('studentId', null);
            this.set('studentError', false);
            this.set('g11Error', false);
            this.set('MYError', false);
            this.set('finalError', false);
            this.set('firstError', false);
            this.set('isCreating', false);
        }
    }
});
