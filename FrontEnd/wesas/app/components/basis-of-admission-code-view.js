import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),

    load: function () {
        this.get('store').query('basis-of-admission-code', {}).then((codes) => {
          codes = codes;
            this.set('codes', this.get('store').findAll('basis-of-admission-code'));
            this.$('.ui.active.inverted.dimmer.view').hide();
        });
    }.property()

});
