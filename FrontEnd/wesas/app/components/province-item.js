import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),
    tagName: 'tr',

    mouseEnter: function () {
        if (!this.get('isEditing') && !this.get('isRemoving')) {
            this.$('td.right.aligned > button').css('visibility', 'visible');
        }
    },
    mouseLeave: function () {
        this.$('td.right.aligned > button').css('visibility', 'hidden');
    },

    willDestroyElement: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },
    didRender: function () {
        // NOTE: The content of the popup is the button's title element by default
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },

    countries: function () {
        return this.get('store').findAll('country');
    }.property(),

    isEditing: false,
    isRemoving: false,
    countryId: null,

    actions: {
        setCountryId: function (comp, id) {
            this.set('countryId', id);
        },
        edit: function (name) {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('name', name);
            this.set('isEditing', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        warning: function () {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('isRemoving', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        cancel: function () {
            this.set('isEditing', false);
            this.set('isRemoving', false);
        },
        submit: function (id, name) {

            var self = this;
            var country = this.get('store').peekRecord('country', this.get('countryId'));

            this.get('store').find('province', id).then(function (province) {
                province.set('name', name);
                province.set('country', country);
                province.save().then(function () {
                    self.get('notify').success(self.get('notifyMessages').editSuccess('Province', name));
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').editFail(name), {closeAfter: null});
                });
            });

            this.set('name', '');
            this.set('countryId', null);
            this.set('isEditing', false);
        },
        remove: function (id) {

            var self = this;

            this.get('store').find('province', id).then(function (province) {

                for (var i = 0; i < province.students.length; i++) {
                    province.students[i] = null;
                }
                for (i = 0; i < province.cities.length; i++) {
                    province.cities[i] = null;
                }
                province.set('country', null);

                province.save().then(function () {
                    province.destroyRecord().then(function () {
                        self.get('notify').success(self.get('notifyMessages').deleteSuccess('Province'));
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').deleteFail('Province'), {closeAfter: null});
                    });
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').deleteFail('Province'), {closeAfter: null});
                });
            });

            this.set('isRemoving', false);
        }
    }
});
