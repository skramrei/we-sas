import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    admissionRules: function () {
        return this.get('store').findAll('admission-rule');
    }.property(),

    programAdministrations: function () {
        return this.get('store').findAll('program-administration');
    }.property(),

    commentCodes: function () {
        return this.get('store').findAll('comment-code');
    }.property(),

    isCreating: false,
    ruleId: null,
    deptIndexes: [],
    commentCodeId: null,
    nameError: false,
    ruleError: false,
    programAdministrationError: false,
    commentCodeError: false,

    actions: {
        setRule: function (comp, id) {
            this.set('ruleId', id);
            this.set('ruleError', false);
            this.$('#academic-program-code-create-rule').removeClass('field-error');
        },
        setDept: function (comp, ids) {
            this.set('deptIndexes', ids.split(','));
            this.set('programAdministrationError', false);
            this.$('#academic-program-code-create-program-administration').removeClass('field-error');
        },
        setCommentCode: function (comp, id) {
            this.set('commentCodeId', id);
            this.set('commentCodeError', false);
            this.$('#academic-program-code-create-comment-code').removeClass('field-error');
        },
        create: function () {
            this.set('isCreating', true);
        },
        cancel: function () {
            this.set('isCreating', false);
            this.set('deptIndexes', []);
            this.set('ruleId', null);
            this.set('nameError', false);
            this.$('#academic-program-code-item-name').removeClass('field-error');
            this.set('programAdministrationError', false);
            this.$('#academic-program-code-create-program-administration').removeClass('field-error');
            this.set('ruleError', false);
            this.$('#academic-program-code-create-rule').removeClass('field-error');
            this.set('commentCodeError', false);
            this.$('#academic-program-code-create-comment-code').removeClass('field-error');
        },
        onNameInput: function () {
            // Remove all error indicators
            this.set('nameError', false);
            this.$('#academic-program-code-create-name').removeClass('field-error');
        },
        submit: function (name) {

            // Display any errors for invalid inputs
            var foundInvalidFields = false;
            if (name === undefined || name === null || name === "") {
                this.set('nameError', true);
                this.$('#academic-program-code-create-name').addClass('field-error');
                foundInvalidFields = true;
            }
            if (this.get('ruleId') === null) {
                this.set('ruleError', true);
                this.$('#academic-program-code-create-rule').addClass('field-error');
                foundInvalidFields = true;
            }
            if (this.get('deptIndexes').length === 0) {
                this.set('programAdministrationError', true);
                this.$('#academic-program-code-create-program-administration').addClass('field-error');
                foundInvalidFields = true;
            }
            if (this.get('commentCodeId') === null) {
                this.set('commentCodeError', true);
                this.$('#academic-program-code-create-comment-code').addClass('field-error');
                foundInvalidFields = true;
            }
            if (foundInvalidFields) {
                return;
            }

            // Form inputs are valid so continue with creation.
            var self = this;
            var store = this.get('store');
            var rule = this.get('store').peekRecord('admission-rule', this.get('ruleId'));
            var commentCode = this.get('store').peekRecord('comment-code', this.get('commentCodeId'));

            var newProgram = this.get('store').createRecord('academic-program-code', {
                name: name,
                rule: rule,
                commentCode: commentCode
            });

            newProgram.save().then(() => {
                // Now save the departments
                var deptPromises = [];
                // Link the new program to the appropriate program administrations
                for (var i = 0; i < this.get('deptIndexes').length; i++) {
                    var oneDept = store.peekRecord('program-administration', this.get('deptIndexes')[i]);
                    oneDept.set('academicProgramCode', newProgram);
                    deptPromises.push(oneDept.save());
                }
                // Make sure all the Program Administrations saved correctly
              Ember.RSVP.Promise.all(deptPromises).then(() => {
                        // All department promises fulfilled
                        self.get('notify').success(self.get('notifyMessages').createSuccess('Academic Program Code', name));
                        this.set('deptIndexes', []);
                    }, () => {
                        // At least one department promise rejected
                        self.get('notify').alert(self.get('notifyMessages').createFail(name), {closeAfter: null});
                        this.set('deptIndexes', []);
                    });
            }, () => {
                self.get('notify').alert(self.get('notifyMessages').createFail(name), {closeAfter: null});
            });

            // Reset values for another creation
            this.set('name', '');
            this.set('ruleId', null);
            this.set('commentCodeId', null);
            this.set('isCreating', false);
        }
    }
});
