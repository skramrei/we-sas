import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    actions: {

        create: function () {
            this.$('.ui.create.modal').modal('show');
        },
        close: function () {
            this.$('.ui.create.modal').modal('hide');
        },
        submit: function () {
            this.get('store').createRecord('hs-subject', {
                name: this.get('name'),
                description: this.get('description')
            }).save().then(() => {
                this.get('notify').success(this.get('notifyMessages').createSuccess('High School Subject', this.get('name')));
            }, () => {
                this.get('notify').alert(this.get('notifyMessages').createFail(this.get('name')), {
                    closeAfter: null
                });
            });
            this.set('name', '');
            this.set('description', '');
        }
    }

});
