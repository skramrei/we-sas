import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    tagName: 'tr',

    isEdittingRule: false,
    isRemoving: false,
    notAddingRule: true,

    expressions: function () {
        return this.get('store').query('logical-expression', { rule: this.get('admission_rule.id') });
    }.property(),

    willDestroyElement: function () {
        // Remove the popup explicitly or it may stay after the row is gone
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },
    didRender: function () {
        // NOTE: The content of the popup is the button's title element by default
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },

    mouseEnter: function () {
        if (!this.get('isEditing') && !this.get('isRemoving')) {
            this.$('td.right.aligned > button').css('visibility', 'visible');
        }
    },
    mouseLeave: function () {
        this.$('td.right.aligned > button').css('visibility', 'hidden');
    },

    actions: {
        onExpressionCreated: function (booleanExpression, link, isForFull, canceled) {
            if (canceled) {
                this.set('notAddingRule', true);
                return;
            }

            var self = this;

            // These expressions must be the first in their chain so set isFirst as true
            this.get('store').createRecord('logical-expression', {
                booleanExp: booleanExpression,
                logicalLink: link,
                rule: this.get('admission_rule'),
                isFirst: true,
                ifFull: isForFull
            }).save().then(() => {
                    // Update the view of this admission rule so show the new expression
                    self.notifyPropertyChange('expressions');
                }, () => {
                    // Something went wrong
                    this.get('notify').alert(this.get('notifyMessages').editFail("Rule"), {
                        closeAfter: null
                    });
                });

            this.set('notAddingRule', true);
        },
        addLogicalExpression: function () {
            this.set('notAddingRule', false);
        },
        editRule: function (id) {
          id = id;
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('isEdittingRule', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        submitAdmissionRule: function (id, description) {

            var store = this.get('store');

            store.find('admission-rule', id).then((rule) => {
                rule.set('description', description);
                rule.save().then(() => {
                    this.get('notify').success(this.get('notifyMessages').editSuccess('Rule', ""));
                }, () => {
                    this.get('notify').alert(this.get('notifyMessages').editFail("Rule"), {
                        closeAfter: null
                    });
                });
            });

            this.set('isEdittingRule', false);
        },
        cancel: function () {
            this.set('isEdittingRule', false);
            this.set('isRemoving', false);
        },
        warning: function () {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('isRemoving', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        remove: function (id) {

            var store = this.get('store');
            var self = this;


            store.find('admission-rule', id).then(function (admissionRule) {

                for (var i = 0; i < admissionRule.testExpressions.length; i++) {
                    admissionRule.testExpressions[i]=null;
                }

                // Note that the back-end will take care of deleting any associated logical expressions
                admissionRule.save().then(() => {
                    // On save fulfilled
                    admissionRule.destroyRecord().then(() => {
                        // On destroy fulfilled
                        self.get('notify').success(self.get('notifyMessages').deleteSuccess('Rule'));
                    }, () => {
                        // On destroy rejected
                        self.get('notify').alert(self.get('notifyMessages').deleteFail('rule'), {
                            closeAfter: null
                        });
                    });
                }, () => {
                    // On save rejected
                    self.get('notify').alert(self.get('notifyMessages').deleteFail('rule'), {
                        closeAfter: null
                    });
                });
            });

        }
    }
});
