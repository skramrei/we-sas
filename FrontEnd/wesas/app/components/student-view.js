import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),

    MST01IsPermitted: function () {
        return this.get('oudaAuth').getName !== "Root" ? this.get('oudaAuth').get('userCList').indexOf("MST01") >= 0 : true;
    }.property(),

    loadStudents: function () {
        this.get('store').query('student', {skip: 0, limit: 10}).then((students) => {
            if (students.get('length') >= 10) {
                this.set('anyLeft', true);
            } else {
                this.set('anyLeft', false);
            }
            this.set('students', students);
            this.$('.ui.active.inverted.dimmer.view').hide();
        });
    }.property(),

    /*input: function () {

        this.$('.ui.active.inverted.dimmer.view').show();

        setTimeout(() => {

            var query = this.$('#student-search-text').val().trim();
            var regex = new RegExp(query, "gi");

            var results = this.get('store')
                .peekAll('student')
                .filter((student) => {
                    return student.get('studentNumber').match(regex) ||
                        student.get('firstName').match(regex) ||
                        student.get('lastName').match(regex) ||
                        (student.get('firstName') + " " + student.get('lastName')).match(regex);
                });

            this.set('students', results);
            this.$('.ui.active.inverted.dimmer.view').hide();

        }, 50);
    },*/

    next: 10,
    prev: -10,
    anyLeft: false,
    paginate: true,

    actions: {
        search: function () {
            this.set('paginate', false);
            this.$('.ui.active.inverted.dimmer.view').show();
            this.get('store').query('student', {queryFilter: this.$('#student-search-text').val().trim()}).then((students) => {
                this.set('students', students);
                this.$('.ui.active.inverted.dimmer.view').hide();
            });
        },
        getNext: function () {

            this.$('.ui.active.inverted.dimmer.view').show();

            let next = this.get('next');
            let prev = this.get('prev');
            this.get('store').query('student', {skip: next, limit: 10}).then((students) => {
                if (students.get('length') >= 10) {
                    this.set('anyLeft', true);
                } else {
                    this.set('anyLeft', false);
                }
                this.set('students', students);
                this.$('.ui.active.inverted.dimmer.view').hide();
            });

            this.set('next', next + 10);
            this.set('prev', prev + 10);
        },
        getPrev: function () {

            this.$('.ui.active.inverted.dimmer.view').show();

            let next = this.get('next');
            let prev = this.get('prev');
            this.get('store').query('student', {skip: prev, limit: 10}).then((students) => {
                if (students.get('length') >= 10) {
                    this.set('anyLeft', true);
                } else {
                    this.set('anyLeft', false);
                }
                this.set('students', students);
                this.$('.ui.active.inverted.dimmer.view').hide();
            });

            this.set('next', next - 10);
            this.set('prev', prev - 10);
        }
    }
});
