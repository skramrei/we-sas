import Ember from 'ember';

export default Ember.Component.extend({

    ADM01IsPermitted: Ember.computed(function () {
        var authentication = this.get('oudaAuth');
        if (authentication.getName === "Root") {
            return true;
        } else {
            return (authentication.get('userCList').indexOf("ADM01") >= 0);
        }
    }),

  didRender: function () {
    var self = this;
    this.$(document).ready(function () {
      self.$('.vertical.menu .item').tab({history: false});
    });
  }

});
