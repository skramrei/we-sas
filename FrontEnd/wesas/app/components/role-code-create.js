import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    rolePermissions: function () {
        return this.get('store').findAll('role-permission');
    }.property(),

    rolePermissionId: null,

    actions: {
        setRolePermissionId: function (comp, id) {
            this.set('rolePermissionId', id);
        },
        create: function () {
            this.$('.ui.small.modal').modal('show');
        },
        submit: function () {

            var name = this.get('name');
            var rolePermissionId = this.get('rolePermissionId');
            var self = this;

            this.get('store').createRecord('role-code', {
                name: name
            }).save().then(function (roleCode) {

                self.get('notify').success(self.get('notifyMessages').createSuccess('Role Code', self.get('name')));

                var rolePermissions = rolePermissionId.split(',');
                for (var i = 0; i < rolePermissions.length; i++) {
                    var rolePermission = self.get('store').peekRecord('role-permission', rolePermissions[i]);
                    rolePermission.get('roleCodes').pushObject(roleCode);
                    rolePermission.save();
                }
            }, function () {
                self.get('notify').alert(self.get('notifyMessages').createFail(self.get('name')), {closeAfter: null});
            });

            this.$('form').form('clear');
        },
        close: function () {
            this.$('form').form('clear');
        }
    }
});
