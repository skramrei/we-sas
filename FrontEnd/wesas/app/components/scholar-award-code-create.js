import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    students: function () {
        return this.get('store').findAll('student');
    }.property(),

    isCreating: false,
    inputError: false,

    actions: {
        onNameInput: function (value) {
            // Remove error indications on input
          value = value;
            this.set('inputError', false);
            this.$('#school-award-code-create-name').removeClass('field-error');
        },
        create: function () {
            this.set('isCreating', true);
        },
        cancel: function () {
            this.set('isCreating', false);
            this.set('name', '');
            this.set('inputError', false);
            this.$('#school-award-code-create-name').removeClass('field-error');
            this.set('studentError', false);
            this.$('#student-field').removeClass('field-error');
        },
        submit: function (name) {

            var foundInvalidFields = false;

            if (name === undefined || name === null || name === "") {
                this.set('inputError', true);
                this.$('#school-award-code-create-name').addClass('field-error');
                foundInvalidFields =true;
            } else {
                this.set('inputError', false);
                this.$('#school-award-code-create-name').removeClass('field-error');
            }

            var self = this;

            this.get('store').createRecord('scholar-award-code', {
                name: name
            }).save().then(function () {
                self.get('notify').success(self.get('notifyMessages').createSuccess('Scholar Award Code', name));
            }, function () {
                self.get('notify').alert(self.get('notifyMessages').createFail(name), {closeAfter: null});
            });

            this.set('name', '');
            this.set('isCreating', false);
            this.set('inputError', false);
            this.$('#school-award-code-create-name').removeClass('field-error');
            this.set('studentError', false);
            this.$('#student-field').removeClass('field-error');
        }
    }
});
