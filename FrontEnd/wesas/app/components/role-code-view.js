import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),

    roleCodes: function () {
        return this.get('store').findAll('role-code');
    }.property(),

    MSR01IsPermitted: Ember.computed(function () {
        var authentication = this.get('oudaAuth');
        if (authentication.getName === "Root") {
            return true;
        } else {
            return (authentication.get('userCList').indexOf("MSR01") >= 0);
        }
    })
});
