import Ember from 'ember';
import _ from 'npm:underscore';
import Sequence from 'npm:sequence';

export default Ember.Component.extend({

    store: Ember.inject.service(),

    change: function (e) {
        e.stopPropagation();
        e.preventDefault();

        var files = e.target.files;
        var errors = [];
        this.$('.ui.error.message').remove();

        outer:
            for (let i = 0; i < files.length; i++) {
                if (files[i].type !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                    errors.push('<li>The file ' + files[i].name + ' is not an excel spreadsheet</li>');
                    continue;
                }

                var fileNames = this.get('fileNames');
                var correctFileName = false;
                for (let j = 0; j < fileNames.length; j++) {
                    if (fileNames[j] === files[i].name.toLowerCase()) {
                        correctFileName = true;
                    }
                }
                if (!correctFileName) {
                    errors.push('<li>The excel spreadsheet ' + files[i].name + ' does not contain associated student data</li>');
                    continue;
                }

                var existingFiles = this.get('files');
                for (let j = 0; j < existingFiles.length; j++) {
                    if (existingFiles[j].name.toLowerCase() === files[i].name.toLowerCase()) {
                        errors.push('<li>The excel spreadsheet ' + files[i].name + ' has already been uploaded</li>');
                        continue outer;
                    }
                }
                this.$('.ui.file-upload.grid .fourteen.wide.column').hide();
                this.$('.ui.file-upload.grid').append('<div class="column file">' +
                    '<div class="ui segment">' +
                    '<div class="ui item">' +
                    '<i class="file excel outline icon"></i>' + files[i].name + '</div></div></div>');
                this.get('files').push(files[i]);
            }

        if (errors.length) {
            var message = '<div class="ui error message">' +
                '<div class="header">Error: There was a problem with one or more files uploaded</div>' +
                '<ul class="list">';
            for (let i = 0; i < errors.length; i++) {
                message += errors[i];
            }
            message += '</ul></div>';
            this.$('.ui.file-upload.grid').after(message);
        }
    },
    dragover: function (e) {
        e.stopPropagation();
        e.preventDefault();
        e.dataTransfer.dropEffect = 'copy';
    },
    drop: function (e) {
        e.stopPropagation();
        e.preventDefault();

        var files = e.dataTransfer.files;
        var errors = [];
        this.$('.ui.error.message').remove();

        outer:
            for (let i = 0; i < files.length; i++) {
                if (files[i].type !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                    errors.push('<li>The file ' + files[i].name + ' is not an excel spreadsheet</li>');
                    continue;
                }

                var fileNames = this.get('fileNames');
                var correctFileName = false;
                for (let j = 0; j < fileNames.length; j++) {
                    if (fileNames[j] === files[i].name.toLowerCase()) {
                        correctFileName = true;
                    }
                }
                if (!correctFileName) {
                    errors.push('<li>The excel spreadsheet ' + files[i].name + ' does not contain associated student data</li>');
                    continue;
                }

                var existingFiles = this.get('files');
                for (let j = 0; j < existingFiles.length; j++) {
                    if (existingFiles[j].name.toLowerCase() === files[i].name.toLowerCase()) {
                        errors.push('<li>The excel spreadsheet ' + files[i].name + ' has already been uploaded</li>');
                        continue outer;
                    }
                }
                this.$('.ui.file-upload.grid .fourteen.wide.column').hide();
                this.$('.ui.file-upload.grid').append('<div class="column file">' +
                    '<div class="ui segment">' +
                    '<div class="ui item">' +
                    '<i class="file excel outline icon"></i>' + files[i].name + '</div></div></div>');
                this.get('files').push(files[i]);
            }

        if (errors.length) {
            var message = '<div class="ui error message">' +
                '<div class="header">Error: There was a problem with one or more files uploaded</div>' +
                '<ul class="list">';
            for (let i = 0; i < errors.length; i++) {
                message += errors[i];
            }
            message += '</ul></div>';
            this.$('.ui.file-upload.grid').after(message);
        }
    },

    fileNames: ['students.xlsx', 'residencies.xlsx', 'genders.xlsx', 'academicloads.xlsx', 'countries.xlsx', 'provinces.xlsx', 'cities.xlsx'],
    files: [],
    uploadPercent: 0,

    actions: {
        file: function () {
            this.$('#student-file').trigger('click');
        },
        upload: function () {

            var files = this.get('files');
            let studentCount = 0,
                residencyCount = 0,
                genderCount = 0,
                academicLoadCount = 0,
                countryCount = 0,
                provinceCount = 0,
                cityCount = 0;

            if (files.length !== 7) {
                this.$('.ui.error.basic.modal').modal('show');
                return;
            }

            var csvData = [];
            var self = this;

            Sequence.Sequence.create()
                .then(function (next) {
                    self.$('.ui.upload.modal').modal('show');
                    self.set('uploadPercent', 100);
                    self.$('.ui.upload.progress .label').html('Parsing Excel Spreadsheets');
                    setTimeout(function () {
                        next();
                    }, 1000);
                })
                .then(function (next) {
                    for (let i = 0; i < files.length; i++) {

                        var reader = new FileReader();

                        reader.onload = (function (file) {

                            var name = file.name.toLowerCase();

                            return function (e) {

                                var data = e.target.result;
                                var workbook = XLSX.read(data, {type: 'binary'});
                                var sheet = workbook.Sheets[workbook.SheetNames[0]];

                                csvData.push({
                                    name: name,
                                    data: XLSX.utils.sheet_to_csv(sheet)
                                });
                                if (csvData.length === files.length) {
                                    self.$('.ui.upload.progress .bar').html('<div class="progress"></div>');
                                    self.set('uploadPercent', 2);
                                    self.$('.ui.upload.progress .label').html('Creating Residencies');
                                    setTimeout(function () {
                                        next();
                                    }, 1000);
                                }
                            };
                        })(files[i]);
                        reader.readAsBinaryString(files[i]);
                    }
                })
                .then(function (next) {
                  Papa.parse(_.findWhere(csvData, {name: 'residencies.xlsx'}).data, {
                        header: true,
                        complete: function (results) {
                            let count = 0;
                            for (let i = 0; i < results.data.length; i++) {

                                var data = results.data[i];

                                if (data.name) {
                                    self.get('store').createRecord('residency', {
                                        name: data.name
                                    }).save().then(function () {
                                        ++residencyCount;
                                        if (++count === results.data.length) {
                                            self.set('uploadPercent', 14);
                                            self.$('.ui.upload.progress .label').html('Creating Genders');
                                            setTimeout(function () {
                                                next();
                                            }, 1000);
                                        }
                                    }, function () {
                                        self.$('.ui.upload.progress .label').html('Error Creating Residencies');
                                        self.$('.ui.upload.progress').addClass('error');
                                    });
                                } else {
                                    ++count;
                                }
                            }
                        }
                    });
                })
                .then(function (next) {
                  Papa.parse(_.findWhere(csvData, {name: 'genders.xlsx'}).data, {
                        header: true,
                        complete: function (results) {
                            let count = 0;
                            for (let i = 0; i < results.data.length; i++) {

                                var data = results.data[i];

                                if (data.name) {
                                    self.get('store').createRecord('gender', {
                                        name: data.name
                                    }).save().then(function () {
                                        ++genderCount;
                                        if (++count === results.data.length) {
                                            self.set('uploadPercent', 28);
                                            self.$('.ui.upload.progress .label').html('Creating Academic Loads');
                                            setTimeout(function () {
                                                next();
                                            }, 1000);
                                        }
                                    }, function () {
                                        self.$('.ui.upload.progress .label').html('Error Creating Genders');
                                        self.$('.ui.upload.progress').addClass('error');
                                    });
                                } else {
                                    ++count;
                                }
                            }
                        }
                    });
                })
                .then(function (next) {
                  Papa.parse(_.findWhere(csvData, {name: 'academicloads.xlsx'}).data, {
                        header: true,
                        complete: function (results) {
                            let count = 0;
                            for (let i = 0; i < results.data.length; i++) {

                                var data = results.data[i];

                                if (data.name) {
                                    self.get('store').createRecord('academic-load', {
                                        name: data.name
                                    }).save().then(function () {
                                        ++academicLoadCount;
                                        if (++count === results.data.length) {
                                            self.set('uploadPercent', 42);
                                            self.$('.ui.upload.progress .label').html('Creating Countries');
                                            setTimeout(function () {
                                                next();
                                            }, 1000);
                                        }
                                    }, function () {
                                        self.$('.ui.upload.progress .label').html('Error Creating Academic Loads');
                                        self.$('.ui.upload.progress').addClass('error');
                                    });
                                } else {
                                    ++count;
                                }
                            }
                        }
                    });
                })
                .then(function (next) {
                  Papa.parse(_.findWhere(csvData, {name: 'countries.xlsx'}).data, {
                        header: true,
                        complete: function (results) {
                            let count = 0;
                            for (let i = 0; i < results.data.length; i++) {

                                var data = results.data[i];

                                if (data.name) {
                                    self.get('store').createRecord('country', {
                                        name: data.name
                                    }).save().then(function () {
                                        ++countryCount;
                                        if (++count === results.data.length) {
                                            self.set('uploadPercent', 56);
                                            self.$('.ui.upload.progress .label').html('Creating Provinces');
                                            setTimeout(function () {
                                                next();
                                            }, 1000);
                                        }
                                    }, function () {
                                        self.$('.ui.upload.progress .label').html('Error Creating Countries');
                                        self.$('.ui.upload.progress').addClass('error');
                                    });
                                } else {
                                    ++count;
                                }
                            }
                        }
                    });
                })
                .then(function (next) {
                  Papa.parse(_.findWhere(csvData, {name: 'provinces.xlsx'}).data, {
                        header: true,
                        complete: function (results) {
                            let count = 0;
                            for (let i = 0; i < results.data.length; i++) {

                                var data = results.data[i];

                                if (data.name && data.country) {
                                    (function (data) {
                                        self.get('store').queryRecord('country', {name: data.country}).then(function (country) {
                                            self.get('store').createRecord('province', {
                                                name: data.name,
                                                country: country
                                            }).save().then(function () {
                                                ++provinceCount;
                                                if (++count === results.data.length) {
                                                    self.set('uploadPercent', 70);
                                                    self.$('.ui.upload.progress .label').html('Creating Cities');
                                                    setTimeout(function () {
                                                        next();
                                                    }, 1000);
                                                }
                                            }, function () {
                                                self.$('.ui.upload.progress .label').html('Error Creating Provinces');
                                                self.$('.ui.upload.progress').addClass('error');
                                            });
                                        });
                                    })(data);
                                } else {
                                    ++count;
                                }
                            }
                        }
                    });
                })
                .then(function (next) {
                  Papa.parse(_.findWhere(csvData, {name: 'cities.xlsx'}).data, {
                        header: true,
                        complete: function (results) {
                            let count = 0;
                            for (let i = 0; i < results.data.length; i++) {

                                var data = results.data[i];

                                if (data.name && data.province) {
                                    (function (data) {
                                        self.get('store').queryRecord('province', {name: data.province}).then(function (province) {
                                            self.get('store').createRecord('city', {
                                                name: data.name,
                                                province: province
                                            }).save().then(function () {
                                                ++cityCount;
                                                if (++count === results.data.length) {
                                                    self.set('uploadPercent', 84);
                                                    self.$('.ui.upload.progress .label').html('Creating Students');
                                                    setTimeout(function () {
                                                        next();
                                                    }, 1000);
                                                }
                                            }, function () {
                                                self.$('.ui.upload.progress .label').html('Error Creating Cities');
                                                self.$('.ui.upload.progress').addClass('error');
                                            });
                                        });
                                    })(data);
                                } else {
                                    ++count;
                                }
                            }
                        }
                    });
                })
                .then(function () {
                  Papa.parse(_.findWhere(csvData, {name: 'students.xlsx'}).data, {
                        header: true,
                        complete: function (results) {
                            let count = 0;
                            for (let i = 0; i < results.data.length; i++) {

                                var data = results.data[i];

                                if (data.studentNumber) {
                                    (function (data) {

                                        var gender = null,
                                            residency = null,
                                            academicLoad = null,
                                            country = null,
                                            province = null,
                                            city = null;

                                        Sequence.Sequence.create()
                                            .then(function (next) {
                                                self.get('store').queryRecord('gender', {name: data.gender}).then(function (res) {
                                                    gender = res;
                                                    next();
                                                });
                                            })
                                            .then(function (next) {
                                                self.get('store').queryRecord('residency', {name: data.residency}).then(function (res) {
                                                    residency = res;
                                                    next();
                                                });
                                            })
                                            .then(function (next) {
                                                self.get('store').queryRecord('academic-load', {name: data.academicLoad}).then(function (res) {
                                                    academicLoad = res;
                                                    next();
                                                });
                                            })
                                            .then(function (next) {
                                                self.get('store').queryRecord('country', {name: data.country}).then(function (res) {
                                                    country = res;
                                                    next();
                                                });
                                            })
                                            .then(function (next) {
                                                self.get('store').queryRecord('province', {name: data.province}).then(function (res) {
                                                    province = res;
                                                    next();
                                                });
                                            })
                                            .then(function (next) {
                                                self.get('store').queryRecord('city', {name: data.city}).then(function (res) {
                                                    city = res;
                                                    next();
                                                });
                                            })
                                            .then(function () {
                                                self.get('store').createRecord('student', {
                                                    studentNumber: data.studentNumber,
                                                    firstName: data.firstName,
                                                    lastName: data.lastName,
                                                    gender: gender,
                                                    DOB: new Date(data.DOB),
                                                    residency: residency,
                                                    academicLoad: academicLoad,
                                                    country: country,
                                                    province: province,
                                                    city: city
                                                }).save().then(function () {
                                                    ++studentCount;
                                                    if (++count === results.data.length) {
                                                        self.set('uploadPercent', 100);
                                                        self.$('.ui.upload.progress .label').html('Done');
                                                        self.$('.ui.small.upload.modal .actions').html(
                                                            '<button class="ui green deny button">' +
                                                            '<i class="checkmark icon"></i>Ok' +
                                                            '</button>');
                                                        self.$('.ui.small.upload .content').append(
                                                            '<div class="ui info message">' +
                                                            '<div class="header">Results of Uploading Data</div>' +
                                                            '<ul>' +
                                                            '<li>Number of students uploaded: ' + studentCount + '</li>' +
                                                            '<li>Number of residencies uploaded: ' + residencyCount + '</li>' +
                                                            '<li>Number of genders uploaded: ' + genderCount + '</li>' +
                                                            '<li>Number of countries uploaded: ' + countryCount + '</li>' +
                                                            '<li>Number of provinces uploaded: ' + provinceCount + '</li>' +
                                                            '<li>Number of cities uploaded: ' + cityCount + '</li>' +
                                                            '<li>Number of academic loads: ' + academicLoadCount + '</li>' +
                                                            '</ul>' +
                                                            '</div>'
                                                        );
                                                        self.$('.ui.error.message').remove();
                                                        self.$('.ui.file-upload.grid .column.file').remove();
                                                        self.$('.ui.file-upload.grid .fourteen.wide.column').show();
                                                        self.set('files', []);
                                                    }
                                                }, function () {
                                                    self.$('.ui.upload.progress .label').html('Error Creating Cities');
                                                    self.$('.ui.upload.progress').addClass('error');
                                                });
                                            });
                                    })(data);
                                } else {
                                    ++count;
                                }
                            }
                        }
                    });
                });
        },
        clear: function () {
            this.$('.ui.error.message').remove();
            this.$('.ui.file-upload.grid .column.file').remove();
            this.$('.ui.file-upload.grid .fourteen.wide.column').show();
            this.set('files', []);
        },
        closeError: function () {
            this.$('.ui.error.basic.modal').modal('hide');
        }
    }

});
