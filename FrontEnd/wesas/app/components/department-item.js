import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),
    tagName: 'tr',

    mouseEnter: function () {
        if (!this.get('isEditing') && !this.get('isRemoving')) {
            this.$('td.right.aligned > button').css('visibility', 'visible');
        }
    },
    mouseLeave: function () {
        this.$('td.right.aligned > button').css('visibility', 'hidden');
    },

    willDestroyElement: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },
    didRender: function () {
        // NOTE: The content of the popup is the button's title element by default
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },

    faculties: function () {
        return this.get('store').findAll('faculty');
    }.property(),

    isEditing: false,
    isRemoving: false,
    facultyId: null,

    actions: {
        setFacultyId: function (comp, id) {
            this.set('facultyId', id);
        },
        edit: function (name) {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('name', name);
            this.set('isEditing', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        warning: function () {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('isRemoving', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        cancel: function () {
            this.set('isEditing', false);
            this.set('isRemoving', false);
        },
        submit: function (id, name) {

            var self = this;
            var faculty = this.get('store').peekRecord('faculty', this.get('facultyId'));

            this.get('store').find('department', id).then(function (department) {
                department.set('name', name);
                department.set('faculty', faculty);
                department.save().then(function () {
                    self.get('notify').success(self.get('notifyMessages').editSuccess('Department', name));
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').editFail(name), {closeAfter: null});
                });
            });

            this.set('name', '');
            this.set('facultyId', null);
            this.set('isEditing', false);
        },
        remove: function (id) {

            var self = this;

            this.get('store').find('department', id).then(function (department) {

                for (var i = 0; i < department.programAdministrations.length; i++) {
                    department.programAdministrations[i] = null;
                }
                department.set('faculty', null);

                department.save().then(function () {
                    department.destroyRecord().then(function () {
                        self.get('notify').success(self.get('notifyMessages').deleteSuccess('Department'));
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').deleteFail('Department'), {closeAfter: null});
                    });
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').deleteFail('Department'), {closeAfter: null});
                });
            });

            this.set('isRemoving', false);
        }
    }
});
