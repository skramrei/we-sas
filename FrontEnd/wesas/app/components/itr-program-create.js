import Ember from 'ember';

export default Ember.Component.extend({

  store: Ember.inject.service(),
  notify: Ember.inject.service('notify'),
  notifyMessages: Ember.inject.service('notify-messages'),

  isCreatingItrProgram: false,
  eligibility: false,
  studentId: null,
  studentError: false,
  programId: null,
  programError: false,

  students: function () {
    return this.get('store').findAll('student');
  }.property(),

  programs: function () {
    return this.get('store').findAll('academicProgramCode');
  }.property(),

  actions: {
    createItrProgram: function() {
      this.set('isCreatingItrProgram', true);
    },
    cancelCreateItrProgram: function() {
      this.set('isCreatingItrProgram', false);
      this.set('studentError', false);
      this.set('studentId', null);
      this.$('#itr-program-create-student').removeClass('field-error');
      this.set('programError', false);
      this.set('programId', null);
      this.$('#itr-program-create-program').removeClass('field-error');
    },
    setStudentId: function (comp, id) {
      this.set('studentId', id);
      this.set('studentError', false);
      this.$('#itr-program-create-student').removeClass('field-error');
    },
    setProgramId: function (comp, id) {
      this.set('programId', id);
      this.set('programError', false);
      this.$('#itr-program-create-program').removeClass('field-error');
    },
    submitItrProgram: function(order) {

      var store = this.get('store');
      var eligibility = this.get('eligibility');

      var foundInvalidFields = false;
      if (this.get('studentId') === null) {
        this.set('studentError', true);
        this.$('#itr-program-create-student').addClass('field-error');
        foundInvalidFields = true;
      }

      if (this.get('programId') === null) {
        this.set('programError', true);
        this.$('#itr-program-create-program').addClass('field-error');
        foundInvalidFields = true;
      }

      if (foundInvalidFields) {
        return;
      }

      store.createRecord('itr-program', {
        student: store.peekRecord('student', this.get('studentId')),
        program: store.peekRecord('academicProgramCode', this.get('programId')),
        order: order,
        eligibility: eligibility
      }).save().then(() => {
        this.get('notify').success(this.get('notifyMessages').createSuccess('ITR Program', ''));
      }, () => {
        this.get('notify').alert(this.get('notifyMessages').createFail('ITR Program'), {
          closeAfter: null
        });
      });

      this.set('order', '');
      this.set('eligibility', null);
      this.set('isCreatingItrProgram', false);
      this.set('studentError', false);
      this.set('studentId', null);
      this.$('#itr-program-create-student').removeClass('field-error');
      this.set('programError', false);
      this.set('programId', null);
      this.$('#itr-program-create-program').removeClass('field-error');
    }
  }
});
