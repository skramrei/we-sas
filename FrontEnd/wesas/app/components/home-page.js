import Ember from 'ember';

//import Sequence from 'npm:sequence';

export default Ember.Component.extend({

    store: Ember.inject.service(),

    showGraph: function () {

        this.get('store').query('distribution-result', {date: 'latest'}).then((results) => {
            if (results.get('length')) {

                var students = {
                    software: 0,
                    electrical: 0,
                    chemical: 0,
                    civil: 0,
                    computer: 0,
                    mechanical: 0,
                    greenProcess: 0,
                    integrated: 0,
                    mechatronics: 0,
                    rejected: 0
                };

                let count = 0;
                for (let i = 0; i < results.get('length'); i++) {
                    results.objectAt(i).get('commentCode').then((commentCode) => {
                        if (commentCode) {
                            let code = commentCode.get('code');

                            if (code === "EV") {
                                students.civil++;
                                count++;
                            } else if (code === "EM") {
                                students.mechanical++;
                                count++;
                            } else if (code === "EI") {
                                students.integrated++;
                                count++;
                            } else if (code === "EG") {
                                students.greenProcess++;
                                count++;
                            } else if (code === "EF") {
                                students.software++;
                                count++;
                            } else if (code === "EE") {
                                students.electrical++;
                                count++;
                            } else if (code === "EC") {
                                students.computer++;
                                count++;
                            } else if (code === "EB") {
                                students.chemical++;
                                count++;
                            } else if (code === "ED") {
                                students.mechatronics++;
                                count++;
                            } else {
                                students.rejected++;
                                count++;
                            }
                        } else {
                            students.rejected++;
                            ++count;
                        }

                    });
                }

                var chartMade = false;

                var interval = setInterval(function () {
                    if (count === results.get('length')) {
                        clearInterval(interval);

                        if (!chartMade) {
                            this.AmCharts.makeChart("chartdiv", {
                                "type": "serial",
                                "theme": "light",
                                "dataProvider": [{
                                    "Program": "Software",
                                    "Students": students.software
                                }, {
                                    "Program": "Electrical",
                                    "Students": students.electrical
                                }, {
                                    "Program": "Chemical",
                                    "Students": students.chemical
                                }, {
                                    "Program": "Civil",
                                    "Students": students.civil
                                }, {
                                    "Program": "Computer",
                                    "Students": students.computer
                                }, {
                                    "Program": "Mechanical",
                                    "Students": students.mechanical
                                }, {
                                    "Program": "Green",
                                    "Students": students.greenProcess
                                }, {
                                    "Program": "Integrated",
                                    "Students": students.integrated
                                }, {
                                    "Program": "Mechatronic",
                                    "Students": students.mechatronics
                                }, {
                                    "Program": "None",
                                    "Students": students.rejected
                                }],
                                "valueAxes": [{
                                    "gridColor": "#FFFFFF",
                                    "gridAlpha": 0.2,
                                    "dashLength": 0
                                }],
                                "gridAboveGraphs": true,
                                "startDuration": 1,
                                "graphs": [{
                                    "balloonText": "[[category]]: <b>[[value]]</b>",
                                    "fillAlphas": 0.8,
                                    "lineAlpha": 0.2,
                                    "type": "column",
                                    "valueField": "Students"
                                }],
                                "chartCursor": {
                                    "categoryBalloonEnabled": false,
                                    "cursorAlpha": 0,
                                    "zoomable": false
                                },
                                "categoryField": "Program",
                                "categoryAxis": {
                                    "gridPosition": "start",
                                    "gridAlpha": 0,
                                    "tickPosition": "start",
                                    "tickLength": 20
                                },
                                "export": {
                                    "enabled": true
                                }
                            });
                            chartMade = true;
                        }
                    }
                }, 75);



            } else {
                this.$('#dist .ui.header').after(
                    '<div class="ui warning message">' +
                    '<div class="header">Students have not been distributed</div>' +
                    'Distribute students by going to the distribution page to see the results' +
                    '</div>');
            }
        });
    }.property()

});
