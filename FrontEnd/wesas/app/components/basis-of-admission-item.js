import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    isEditing: false,
    isRemoving: false,
    dateError: false,
    commentError: false,


    tagName: 'tr',

    mouseEnter: function () {
        if (!this.get('isEditing') && !this.get('isRemoving')) {
            this.$('td.right.aligned > button').css('visibility', 'visible');
        }
    },
    mouseLeave: function () {
        this.$('td.right.aligned > button').css('visibility', 'hidden');
    },

    willDestroyElement: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },
    didRender: function () {
        // NOTE: The content of the popup is the button's title element by default
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },

    actions: {
        resetFormFields: function () {
            this.set('date', null);
            this.set('comment', null);

        },

        edit: function (comment, date) {
            this.set('isEditing', true);
            this.set('comment',comment);
            this.set('date',date);
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        cancel: function () {
            this.set('isEditing', false);
            this.set('isRemoving', false);
            this.send('resetFormFields');
        },
        submit: function (id,date,comment) {

            // Set flags for the fields that need to be updated.
            var updateDate = false;
            var updateComment = false;
            var updateAnyField = false;

            if (date !== undefined && date !== null && date !== "") {
                updateDate = true;
                updateAnyField = true;
            }
            if (comment !== undefined && comment!== null && comment !== "") {
                updateComment = true;
                updateAnyField = true;
            }

            if (!updateAnyField) {
                // There is actually nothing to update so do nothing
                this.get('notify').warning(this.get('notifyMessages').noChanges('Basis of Admission'));
                this.set('isEditing', false);
                return;
            }

            // There must be something to update so continue
            var store = this.get('store');
            var self = this;

            store.find('basis-of-admission', id).then(function (basis_of_admission) {
                if (updateDate) {
                    basis_of_admission.set('date', date);
                }
                if (updateComment) {
                    basis_of_admission.set('comment', comment);
                }


                basis_of_admission.save().then(() => {
                    self.get('notify').success(self.get('notifyMessages').editSuccess('Basis of Admission', ''));
                }, () => {
                    self.get('notify').alert(self.get('notifyMessages').editFail('Basis of Admission'), {
                        closeAfter: null
                });
            });
        });

            this.send('resetFormFields');
            this.set('isEditing', false);
        },
        warning: function () {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('isRemoving', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        remove: function (id) {

            var self = this;

            this.get('store').find('basis-of-admission', id).then(function (basisOfAdmission) {
                basisOfAdmission.set('student',null);
                basisOfAdmission.save().then(function () {
                  basisOfAdmission.destroyRecord().then(function () {
                        self.get('notify').success(self.get('notifyMessages').deleteSuccess('Basis Of Admission'));
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').deleteFail('Basis Of Admission'), {closeAfter: null});
                    });
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').deleteFail('Basis Of Admission'), {closeAfter: null});
                });
            });

            this.set('isRemoving', false);
        }
  }
});
