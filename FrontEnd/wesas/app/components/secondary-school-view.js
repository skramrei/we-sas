import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),

    loadSecondarySchools: function () {
        this.get('store').query('secondary-school', {}).then((secondarySchools) => {
          secondarySchools=secondarySchools;
          this.set('secondarySchools', this.get('store').findAll('secondary-school'));
            this.$('.ui.active.inverted.dimmer.view').hide();
        });
    }.property()

});
