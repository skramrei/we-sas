import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    isCreating: false,
    nameError: false,

    actions: {
        create: function () {
            this.set('isCreating', true);
        },
        cancel: function () {
            this.set('isCreating', false);
            this.set('nameError', false);
            this.$('#academic-load-item-name').removeClass('field-error');
        },
        onNameInput: function () {
            this.set('nameError', false);
            this.$('#academic-load-create-name').removeClass('field-error');
        },
        submit: function (name) {

            // Remove the name error
            this.set('nameError', false);
            this.$('#academic-load-create-name').removeClass('field-error');

            if (name === null || name === undefined || name === "")  {
                this.set('nameError',true);
                this.$('#academic-load-create-name').addClass('field-error');
                return;
            }

            var self = this;

            this.get('store').createRecord('academic-load', {
                name: name
            }).save().then(function () {
                self.get('notify').success(self.get('notifyMessages').createSuccess('Academic Load', name));
            }, function () {
                self.get('notify').alert(self.get('notifyMessages').createFail(name), {closeAfter: null});
            });

            this.set('name', '');
            this.set('isCreating', false);
        }
    }
});
