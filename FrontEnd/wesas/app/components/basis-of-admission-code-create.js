import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    basisOfAdmissions: function () {
        return this.get('store').findAll('basis-of-admission');
    }.property(),

    actions: {

        setAdmission: function (comp, id) {
            this.set('admission', this.get('store').find('basis-of-admission', id));
        },
        create: function () {
            this.$('.ui.create.modal').modal('show');
        },
        close: function () {
            this.$('.ui.create.modal').modal('hide');
        },
        submit: function () {
            this.get('store').createRecord('basis-of-admission-code', {
                name: this.get('name'),
                basis: this.get('admission')
            }).save().then(() => {
                this.get('notify').success(this.get('notifyMessages').createSuccess('Basis of Admission Code', this.get('name')));
            }, () => {
                this.get('notify').alert(this.get('notifyMessages').createFail(this.get('name')), {
                    closeAfter: null
                });
            });
            this.set('name', '');
        }
    }

    /*store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    isCreatingBasisOfAdmissionCode: false,

    nameError:false,

    actions: {
        createBasisOfAdmissionCode: function () {
            this.set('isCreatingBasisOfAdmissionCode', true);
        },
        cancelCreateBasisAdmissionCode: function () {
            this.set('isCreatingBasisOfAdmissionCode', false);
            this.set('nameError', false);
            this.$('#basis-of-admission-code-name').removeClass('field-error');

        },

        submitBasisOfAdmissionCode: function (name) {

            // Display any errors for invalid inputs
            var foundInvalidFields = false;
            if (name === undefined || name === null || name === "") {
                this.set('nameError', true);
                this.$('#basis-of-admission-code-name').addClass('field-error');
                foundInvalidFields = true;
            }
            if (foundInvalidFields) {
                return;
            }

            // All inputs are valid so conintue creating comment code
            var store = this.get('store');

            store.createRecord('basis-of-admission-code', {
                name: name,

            }).save()
                .then(() => {
                    this.get('notify').success(this.get('notifyMessages').createSuccess('Basis of Admission Code', name));
                }, () => {
                    this.get('notify').alert(this.get('notifyMessages').createFail(name), {
                        closeAfter: null
                    });
                }
            );


            this.set('name', null);
            this.set('isCreatingBasisOfAdmissionCode', false);
        }
    }*/
});
