import Ember from 'ember';
//import Sequence from 'npm:sequence';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),
    tagName: 'tr',

    willDestroyElement: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },
    didRender: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },

    mouseEnter: function () {
        if (!this.get('isViewing')) {
            this.$('td.right.aligned > button').css('visibility', 'visible');
        }
    },
    mouseLeave: function () {
        this.$('td.right.aligned > button').css('visibility', 'hidden');
    },

    isViewing: false,

    actions: {

        view: function (id) {
            this.$('.ui.view.modal').modal('show');
            this.$('.vertical.menu .item').tab();
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
            this.set('isViewing', true);

            var self = this;
            this.get('store').queryRecord('student', {id: id}).then(function (student) {

                self.set('studentNumber', student.get('studentNumber'));
                self.set('firstName', student.get('firstName'));
                self.set('lastName', student.get('lastName'));
                self.set('DOB', new Date(student.get('DOB')).toISOString().split("T")[0]);

                student.get('residency').then(function (residency) {
                    if (residency) {
                        self.set('residency', residency);
                        self.$('.residency.selection').dropdown('set selected', residency.get('name'));
                    }
                });
                student.get('gender').then(function (gender) {
                    if (gender) {
                        self.set('gender', gender);
                        self.$('.gender.selection').dropdown('set selected', gender.get('name'));
                    }
                });
                student.get('academicLoad').then(function (academicLoad) {
                    if (academicLoad) {
                        self.set('academicLoad', academicLoad);
                        self.$('.academicLoad.selection').dropdown('set selected', academicLoad.get('name'));
                    }
                });
                student.get('city').then(function (city) {
                    if (city) {
                        self.set('city', city);
                        self.$('.city.selection').dropdown('set selected', city.get('name'));
                    }
                });
                student.get('province').then(function (province) {
                    if (province) {
                        self.set('province', province);
                        self.$('.province.selection').dropdown('set selected', province.get('name'));
                    }
                });
                student.get('country').then(function (country) {
                    if (country) {
                        self.set('country', country);
                        self.$('.country.selection').dropdown('set selected', country.get('name'));
                    }
                    self.$('.ui.active.inverted.dimmer.student').hide();
                });

                self.set('grades', []);
                self.get('store').query('grade', {student: student.get('id')}).then(function (grades) {
                    grades.forEach(function (grade) {
                        self.get('store').queryRecord('grade', {id: grade.get('id')}).then(function (res) {
                            res.get('courseCode').then(function (courseCode) {
                                self.get('grades').pushObject({
                                    grade: grade,
                                    courseCode: courseCode
                                });
                            });
                        });
                    });
                });
                self.set('itrPrograms', []);
                self.get('store').query('itr-program', {student: student.get('id')}).then(function (itrPrograms) {
                    itrPrograms.forEach(function (itrProgram) {
                        self.get('store').queryRecord('itr-program', {id: itrProgram.get('id')}).then(function (res) {
                            res.get('program').then(function (program) {
                                self.get('itrPrograms').pushObject({
                                    itrProgram: itrProgram,
                                    program: program
                                });
                            });
                        });
                    });
                });
                self.set('distributionResults', []);
                self.get('store').query('distribution-result', { student: student.get('id'), date: 'latest'} ).then(function (distributionResults) {
                    distributionResults.forEach(function (result) {
                        self.get('store').queryRecord('distribution-result', {id: result.get('id')}).then(function (res) {
                            res.get('commentCode').then(function (comment) {
                                self.get('distributionResults').pushObject({
                                    result: result,
                                    comment: comment
                                });
                            });
                        });
                    });
                });
            });

            this.set('residencies', this.get('store').findAll('residency'));
            this.set('genders', this.get('store').findAll('gender'));
            this.set('academicLoads', this.get('store').findAll('academic-load'));
            this.set('countries', this.get('store').findAll('country'));
            this.set('provinces', this.get('store').findAll('province'));
            this.set('cities', this.get('store').findAll('city'));
        },
        close: function () {
            this.$('.ui.view.modal').modal('hide');
            this.set('isViewing', false);
        },

        setResidency: function (comp, id) {
            this.set('residency', this.get('store').peekRecord('residency', id));
        },
        setGender: function (comp, id) {
            this.set('gender', this.get('store').peekRecord('gender', id));
        },
        setAcademicLoad: function (comp, id) {
            this.set('academicLoad', this.get('store').peekRecord('academic-load', id));
        },
        setCountry: function (comp, id) {
            this.set('country', this.get('store').peekRecord('country', id));
        },
        setProvince: function (comp, id) {
            this.set('province', this.get('store').peekRecord('province', id));
        },
        setCity: function (comp, id) {
            this.set('city', this.get('store').peekRecord('city', id));
        },

        edit: function (id) {

            var self = this;

            this.get('store').find('student', id).then(function (student) {
                student.set('studentNumber', self.get('studentNumber'));
                student.set('firstName', self.get('firstName'));
                student.set('lastName', self.get('lastName'));
                student.set('DOB', new Date(self.get('DOB')));
                student.set('residency', self.get('residency'));
                student.set('gender', self.get('gender'));
                student.set('academicLoad', self.get('academicLoad'));
                student.set('country', self.get('country'));
                student.set('province', self.get('province'));
                student.set('city', self.get('city'));
                student.save().then(function () {
                    self.get('notify').success(self.get('notifyMessages').editSuccess('Student', self.get('firstName') + ' ' + self.get('lastName')));
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').editFail('Student'), {closeAfter: null});
                });
            });
        },

        warning: function () {
            this.$('.ui.small.basic.delete.modal').modal('show');
        },
        cancelRemove: function () {
            this.$('.ui.view.modal').modal('show');
        },
        remove: function (id) {
            var self = this;
            this.get('store').find('student', id).then(function (student) {
                student.set('residency', null);
                student.set('gender', null);
                student.set('academicLoad', null);
                student.set('country', null);
                student.set('province', null);
                student.set('city', null);
                student.save().then(() => {
                    student.destroyRecord().then(() => {
                        self.get('notify').success(self.get('notifyMessages').deleteSuccess('Student'));
                    }, () => {
                        self.get('notify').alert(self.get('notifyMessages').deleteFail('Student'), {closeAfter: null});
                    });
                }, () => {
                    self.get('notify').alert(self.get('notifyMessages').deleteFail('Student'), {closeAfter: null});
                });
            });
            this.set('isViewing', false);
        },

        editGrade: function (id, courseCodeId, mark, code, number, section) {
            this.set('gradeId', id);
            this.set('courseCodeId', courseCodeId);
            this.set('gradeMark', mark);
            this.set('gradeCode', code);
            this.set('gradeNumber', number);
            this.set('gradeSection', section);

            this.$('.ui.edit-grade.modal').modal('show');
        },
        submitGrade: function () {

            var self = this;
            this.get('store').find('grade', this.get('gradeId')).then((grade) => {
                this.get('store').find('course-code', this.get('courseCodeId')).then((courseCode) => {
                    grade.set('mark', this.get('gradeMark'));
                    grade.set('section', this.get('gradeSection'));
                    grade.set('courseCode', courseCode);
                    grade.save().then(function () {
                        self.get('notify').success(self.get('notifyMessages').editSuccess('Grade', 'for ' + self.get('firstName') + ' ' + self.get('lastName')));
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').editFail('Grade'), {closeAfter: null});
                    });
                });
            });

            this.$('.ui.edit-grade.modal').modal('hide');
            this.$('.ui.view.modal').modal('show');
        },
        cancelGrade: function () {
            this.$('.ui.edit-grade.modal').modal('hide');
            this.$('.ui.view.modal').modal('show');
        },

        editItrProgram: function (id, programId, order, name, eligibility) {
            this.set('itrProgramId', id);
            this.set('programCodeId', programId);
            this.set('itrProgramOrder', order);
            this.set('itrProgramName', name);
            this.set('itrProgramEligibility', eligibility);

            this.$('.ui.edit-itr-program.modal').modal('show');
        },
        submitItrProgram: function () {

            var self = this;
            this.get('store').find('itr-program', this.get('itrProgramId')).then((itrProgram) => {
                this.get('store').find('academic-program-code', this.get('programCodeId')).then((programCode) => {
                    itrProgram.set('order', this.get('itrProgramOrder'));
                    itrProgram.set('eligibility', this.get('itrProgramEligibility'));
                    itrProgram.set('program', programCode);
                    itrProgram.save().then(() => {
                        self.set('distributionResults', []);
                        self.get('store').query('distribution-result', {student: self.get('student.id')}).then(function (distributionResults) {
                            distributionResults.forEach(function (result) {
                                self.get('store').queryRecord('distribution-result', {id: result.get('id')}).then(function (res) {
                                    res.get('commentCode').then(function (comment) {
                                        self.get('distributionResults').pushObject({
                                            result: result,
                                            comment: comment
                                        });
                                    });
                                });
                            });
                        });
                        self.get('notify').success(self.get('notifyMessages').editSuccess('ITR Program', 'for ' + self.get('firstName') + ' ' + self.get('lastName')));
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').editFail('ITR Program'), {closeAfter: null});
                    });
                });
            });

            this.$('.ui.edit-itr-program.modal').modal('hide');
            this.$('.ui.view.modal').modal('show');
        },
        cancelItrProgram: function () {
            this.$('.ui.edit-itr-program.modal').modal('hide');
            this.$('.ui.view.modal').modal('show');
        }
    }
});
