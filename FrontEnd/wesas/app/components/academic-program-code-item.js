import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    tagName: 'tr',

    willDestroyElement: function() {
        // Remove the popup explicitly or it may stay after the row is gone
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },
    didRender: function () {
        // NOTE: The content of the popup is the button's title element by default
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },

    isEditingProgram: false,
    isRemoving: false,
    ruleId: null,
    commentCodeId: null,
    rulesNotFound: true,

    rules: function() {
        var rules = this.get('store').findAll('admissionRule');
        if (!rules) {
            this.set('rulesNotFound', true);
            return rules;
        } else {
            this.set('rulesNotFound', false);
            return rules;
        }
    }.property(),

    commentCodes: function() {
        return this.get('store').findAll('commentCode');
    }.property(),

    mouseEnter: function () {
        if (!this.get('isEditingProgram') && !this.get('isRemoving')) {
            this.$('td.right.aligned > button').css('visibility', 'visible');
        }
    },
    mouseLeave: function () {
        this.$('td.right.aligned > button').css('visibility', 'hidden');
    },

    actions: {
        resetFormFields: function () {
            this.set('name', null);
            this.set('ruleId', null);
            this.set('commentCodeId', null);
        },
        editAcademicProgramCode: function () {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('isEditingProgram', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        setRuleId: function (component, value) {
            this.set('ruleId', value);
        },
        setCommentCodeId: function (component, value) {
            this.set('commentCodeId', value);
        },
        warning: function () {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('isRemoving', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        submitEditProgram: function (id, name) {

            // Set flags for the fields that need to be updated and/or are invalid
            var updateName = false;
            var updateRule = false;
            var updateCommentCode = false;
            var updateAnyField = false;
            if (name !== undefined && name !== null && name !== "") {
                updateName= true;
                updateAnyField = true;
            }
            if (this.get('ruleId') !== null) {
                updateRule = true;
                updateAnyField = true;
            }
            if (this.get('commentCodeId') !== null) {
                updateCommentCode = true;
                updateAnyField = true;
            }
            if (!updateAnyField) {
                // Actually don't need to update anything so do nothing
                this.get('notify').warning(this.get('notifyMessages').noChanges('Academic Program'));
                this.set('isEditingProgram', false);
                return;
            }

            var store = this.get('store');
            var self = this;

            store.find('academic-program-code', id).then((academicProgramCode) => {
                if (updateName) {
                    academicProgramCode.set('name', name);
                }
                if (updateRule) {
                    academicProgramCode.set('rule', store.peekRecord('admission-rule', this.get('ruleId')));
                }
                if (updateCommentCode) {
                    academicProgramCode.set('commentCode', store.peekRecord('comment-code', this.get('commentCodeId')));
                }

                academicProgramCode.save().then(() => {
                    if (updateName) {
                        self.get('notify').success(self.get('notifyMessages').editSuccess('Academic Program', name));
                    }
                    else {
                        self.get('notify').success(self.get('notifyMessages').editSuccess('Academic Program', ''));
                    }
                    this.send('resetFormFields');
                }, () => {
                    self.get('notify').alert(self.get('notifyMessages').editFail(name), {
                        closeAfter: null
                    });
                    this.send('resetFormFields');
                });
            });

            this.set('isEditingProgram', false);
        },
        cancelEditProgram: function () {
            this.send('resetFormFields');
            this.set('isEditingProgram', false);
            this.set('isRemoving', false);
        },
        remove: function (id) {

            this.get('store').find('academic-program-code', id).then((code) => {

                code.set('rule', null);

                code.save().then(() => {
                    code.destroyRecord().then(() => {
                        this.get('notify').success(this.get('notifyMessages').deleteSuccess('Academic Program'));
                    }, () => {
                        this.get('notify').alert(this.get('notifyMessages').deleteFail('Academic Program'), {closeAfter: null});
                    });
                }, () => {
                    this.get('notify').alert(this.get('notifyMessages').deleteFail('Academic Program'), {closeAfter: null});
                });
            });
        }
    }
});
