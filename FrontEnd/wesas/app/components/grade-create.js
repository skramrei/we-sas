import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    courseCodes: function () {
        return this.get('store').findAll('course-code');
    }.property(),
    programRecords: function () {
        return this.get('store').findAll('program-record');
    }.property(),
    students: function () {
        return this.get('store').findAll('student');
    }.property(),

    isCreating: false,
    courseCodeId: null,
    programRecordId: null,
    studentId: null,

    actions: {
        setCourseCodeId: function (comp, id) {
            this.set('courseCodeId', id);
        },
        setProgramRecordId: function (comp, id) {
            this.set('programRecordId', id);
        },
        setStudentId: function (comp, id) {
            this.set('studentId', id);
        },
        create: function () {
            this.set('isCreating', true);
        },
        cancel: function () {
            this.set('isCreating', false);
        },
        submit: function (mark, section) {

            var self = this;

            this.get('store').createRecord('grade', {
                mark: mark,
                section: section,
                courseCode: self.get('store').peekRecord('course-code', self.get('courseCodeId')),
                programRecord: self.get('store').peekRecord('program-record', self.get('programRecordId')),
                student: self.get('store').peekRecord('student', self.get('studentId'))
            }).save().then(function () {
                self.get('notify').success(self.get('notifyMessages').createSuccess('Grade', mark));
            }, function () {
                self.get('notify').alert(self.get('notifyMessages').createFail(mark), {closeAfter: null});
            });

            this.set('mark', '');
            this.set('section', '');
            this.set('courseCodeId', null);
            this.set('programRecordId', null);
            this.set('studentId', null);
            this.set('isCreating', false);
        }
    }
});
