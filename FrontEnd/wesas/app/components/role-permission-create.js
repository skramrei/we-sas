import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    actions: {
        create: function () {
            this.$('.ui.small.modal').modal('show');
        },
        submit: function () {

            var self = this;

            this.get('store').createRecord('role-permission', {
                code: this.get('code'),
                sysFeature: this.get('sysFeature')
            }).save().then(function () {
                self.get('notify').success(self.get('notifyMessages').createSuccess('Role Permission', self.get('code')));
            }, function () {
                self.get('notify').alert(self.get('notifyMessages').createFail(self.get('code')), {closeAfter: null});
            });

            this.set('code', '');
            this.set('sysFeature', '');
        },
        close: function () {
            this.set('code', '');
            this.set('sysFeature', '');
        }
    }
});
