import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),
    tagName: 'tr',

    mouseEnter: function () {
        if (!this.get('isEditing') && !this.get('isRemoving')) {
            this.$('td.right.aligned > button').css('visibility', 'visible');
        }
    },
    mouseLeave: function () {
        this.$('td.right.aligned > button').css('visibility', 'hidden');
    },

    willDestroyElement: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },
    didRender: function () {
        // NOTE: The content of the popup is the button's title element by default
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },

    degreeCodes: function () {
        return this.get('store').findAll('degree-code');
    }.property(),
    termCodes: function () {
        return this.get('store').findAll('term-code');
    }.property(),

    isEditing: false,
    isRemoving: false,
    degreeCodeId: null,
    termCodeId: null,

    actions: {
        setDegreeCodeId: function (comp, id) {
            this.set('degreeCodeId', id);
        },
        setTermCodeId: function (comp, id) {
            this.set('termCodeId', id);
        },
        edit: function (level, status, comment) {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('level', level);
            this.set('status', status);
            this.set('comment', comment);
            this.set('isEditing', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        warning: function () {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('isRemoving', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        cancel: function () {
            this.set('isEditing', false);
            this.set('isRemoving', false);
        },
        submit: function (id, level, status, comment) {

            var self = this;
            var degreeCode = this.get('store').peekRecord('degree-code', this.get('degreeCodeId'));
            var termCode = this.get('store').peekRecord('term-code', this.get('termCodeId'));

            this.get('store').find('program-record', id).then(function (programRecord) {
                programRecord.set('level', level);
                programRecord.set('status', status);
                programRecord.set('comment', comment);
                programRecord.set('degreeCode', degreeCode);
                programRecord.set('termCode', termCode);
                programRecord.save().then(function () {
                    self.get('notify').success(self.get('notifyMessages').editSuccess('Program Record', level));
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').editFail(level), {closeAfter: null});
                });
            });

            this.set('level', '');
            this.set('status', '');
            this.set('comment', '');
            this.set('degreeCode', null);
            this.set('termCode', null);
            this.set('isEditing', false);
        },
        remove: function (id) {

            var self = this;

            this.get('store').find('program-record', id).then(function (programRecord) {

                for (var i = 0; i < programRecord.grades.length; i++) {
                    programRecord.grades[i] = null;
                }
                programRecord.set('degreeCode', null);
                programRecord.set('termCode', null);

                programRecord.save().then(function () {
                    programRecord.destroyRecord().then(function () {
                        self.get('notify').success(self.get('notifyMessages').deleteSuccess('Program Record'));
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').deleteFail('Program Record'), {closeAfter: null});
                    });
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').deleteFail('Program Record'), {closeAfter: null});
                });
            });

            this.set('isRemoving', false);
        }
    }
});
