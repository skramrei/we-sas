import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),

    loadHSSubjects: function () {
        this.get('store').query('hs-subject', {}).then((hsSubjects) => {
          hsSubjects=hsSubjects;
            this.set('hsSubjects', this.get('store').findAll('hs-subject'));
            this.$('.ui.active.inverted.dimmer.view').hide();
        });
    }.property()

});
