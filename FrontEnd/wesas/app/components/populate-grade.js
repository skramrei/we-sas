import Ember from 'ember';
import _ from 'npm:underscore';
import Sequence from 'npm:sequence';

export default Ember.Component.extend({

    store: Ember.inject.service(),

    change: function (e) {
        e.stopPropagation();
        e.preventDefault();

        var files = e.target.files;
        var errors = [];
        this.$('.ui.error.message').remove();

        outer:
            for (let i = 0; i < files.length; i++) {
                if (files[i].type !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                    errors.push('<li>The file ' + files[i].name + ' is not an excel spreadsheet</li>');
                    continue;
                }

                var fileNames = this.get('fileNames');
                var correctFileName = false;
                for (let j = 0; j < fileNames.length; j++) {
                    if (fileNames[j] === files[i].name.toLowerCase()) {
                        correctFileName = true;
                    }
                }
                if (!correctFileName) {
                    errors.push('<li>The excel spreadsheet ' + files[i].name + ' does not contain associated grade data</li>');
                    continue;
                }

                var existingFiles = this.get('files');
                for (let j = 0; j < existingFiles.length; j++) {
                    if (existingFiles[j].name.toLowerCase() === files[i].name.toLowerCase()) {
                        errors.push('<li>The excel spreadsheet ' + files[i].name + ' has already been uploaded</li>');
                        continue outer;
                    }
                }
                this.$('.ui.file-upload.grid .fourteen.wide.column').hide();
                this.$('.ui.file-upload.grid').append('<div class="column file">' +
                    '<div class="ui segment">' +
                    '<div class="ui item">' +
                    '<i class="file excel outline icon"></i>' + files[i].name + '</div></div></div>');
                this.get('files').push(files[i]);
            }

        if (errors.length) {
            var message = '<div class="ui error message">' +
                '<div class="header">Error: There was a problem with one or more files uploaded</div>' +
                '<ul class="list">';
            for (let i = 0; i < errors.length; i++) {
                message += errors[i];
            }
            message += '</ul></div>';
            this.$('.ui.file-upload.grid').after(message);
        }
    },
    dragover: function (e) {
        e.stopPropagation();
        e.preventDefault();
        e.dataTransfer.dropEffect = 'copy';
    },
    drop: function (e) {
        e.stopPropagation();
        e.preventDefault();

        var files = e.dataTransfer.files;
        var errors = [];
        this.$('.ui.error.message').remove();

        outer:
            for (let i = 0; i < files.length; i++) {
                if (files[i].type !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                    errors.push('<li>The file ' + files[i].name + ' is not an excel spreadsheet</li>');
                    continue;
                }

                var fileNames = this.get('fileNames');
                var correctFileName = false;
                for (let j = 0; j < fileNames.length; j++) {
                    if (fileNames[j] === files[i].name.toLowerCase()) {
                        correctFileName = true;
                    }
                }
                if (!correctFileName) {
                    errors.push('<li>The excel spreadsheet ' + files[i].name + ' does not contain associated grade data</li>');
                    continue;
                }

                var existingFiles = this.get('files');
                for (let j = 0; j < existingFiles.length; j++) {
                    if (existingFiles[j].name.toLowerCase() === files[i].name.toLowerCase()) {
                        errors.push('<li>The excel spreadsheet ' + files[i].name + ' has already been uploaded</li>');
                        continue outer;
                    }
                }
                this.$('.ui.file-upload.grid .fourteen.wide.column').hide();
                this.$('.ui.file-upload.grid').append('<div class="column file">' +
                    '<div class="ui segment">' +
                    '<div class="ui item">' +
                    '<i class="file excel outline icon"></i>' + files[i].name + '</div></div></div>');
                this.get('files').push(files[i]);
            }

        if (errors.length) {
            var message = '<div class="ui error message">' +
                '<div class="header">Error: There was a problem with one or more files uploaded</div>' +
                '<ul class="list">';
            for (let i = 0; i < errors.length; i++) {
                message += errors[i];
            }
            message += '</ul></div>';
            this.$('.ui.file-upload.grid').after(message);
        }
    },

    fileNames: ['degreecodes.xlsx', 'termcodes.xlsx', 'coursecodes.xlsx', 'grades+programrecords.xlsx'],
    files: [],
    uploadPercent: 0,

    actions: {
        file: function () {
            this.$('#grade-file').trigger('click');
        },
        upload: function () {

            var files = this.get('files');
            let degreeCodeCount = 0,
                termCodeCount = 0,
                courseCodeCount = 0,
                programRecordCount = 0,
                gradeCount = 0;

            if (files.length !== 4) {
                this.$('.ui.error.basic.modal').modal('show');
                return;
            }

            var csvData = [];
            var self = this;

            Sequence.Sequence.create()
                .then(function (next) {
                    self.$('.ui.upload.modal').modal('show');
                    self.set('uploadPercent', 100);
                    self.$('.ui.upload.progress .label').html('Parsing Excel Spreadsheets');
                    setTimeout(function () {
                        next();
                    }, 1000);
                })
                .then(function (next) {

                    for (let i = 0; i < files.length; i++) {

                        var reader = new FileReader();

                        reader.onload = (function (file) {

                            var name = file.name.toLowerCase();

                            return function (e) {

                                var data = e.target.result;
                                var workbook = XLSX.read(data, {type: 'binary'});
                                var sheet = workbook.Sheets[workbook.SheetNames[0]];

                                csvData.push({
                                    name: name,
                                    data: XLSX.utils.sheet_to_csv(sheet)
                                });
                                if (csvData.length === files.length) {
                                    self.$('.ui.upload.progress .bar').html('<div class="progress"></div>');
                                    self.set('uploadPercent', 0);
                                    self.$('.ui.upload.progress .label').html('Creating Degree Codes');
                                    setTimeout(function () {
                                        next();
                                    }, 1000);
                                }
                            };
                        })(files[i]);
                        reader.readAsBinaryString(files[i]);
                    }
                })
                .then(function (next) {
                  Papa.parse(_.findWhere(csvData, {name: 'degreecodes.xlsx'}).data, {
                        header: true,
                        complete: function (results) {
                            let count = 0;
                            for (let i = 0; i < results.data.length; i++) {

                                var data = results.data[i];

                                if (data.name) {
                                    self.get('store').createRecord('degree-code', {
                                        name: data.name
                                    }).save().then(function () {
                                        ++degreeCodeCount;
                                        if (++count === results.data.length) {
                                            self.set('uploadPercent', 25);
                                            self.$('.ui.upload.progress .label').html('Creating Term Codes');
                                            setTimeout(function () {
                                                next();
                                            }, 1000);
                                        }
                                    }, function () {
                                        self.$('.ui.upload.progress .label').html('Error Creating Degree Codes');
                                        self.$('.ui.upload.progress').addClass('error');
                                    });
                                } else {
                                    ++count;
                                }
                            }
                        }
                    });
                })
                .then(function (next) {
                  Papa.parse(_.findWhere(csvData, {name: 'termcodes.xlsx'}).data, {
                        header: true,
                        complete: function (results) {
                            let count = 0;
                            for (let i = 0; i < results.data.length; i++) {

                                var data = results.data[i];

                                if (data.name) {
                                    self.get('store').createRecord('term-code', {
                                        name: data.name
                                    }).save().then(function () {
                                        ++termCodeCount;
                                        if (++count === results.data.length) {
                                            self.set('uploadPercent', 50);
                                            self.$('.ui.upload.progress .label').html('Creating Course Codes');
                                            setTimeout(function () {
                                                next();
                                            }, 1000);
                                        }
                                    }, function () {
                                        self.$('.ui.upload.progress .label').html('Error Creating Term Codes');
                                        self.$('.ui.upload.progress').addClass('error');
                                    });
                                } else {
                                    ++count;
                                }
                            }
                        }
                    });
                })
                .then(function (next) {
                  Papa.parse(_.findWhere(csvData, {name: 'coursecodes.xlsx'}).data, {
                        header: true,
                        complete: function (results) {
                            let count = 0;
                            for (let i = 0; i < results.data.length; i++) {

                                var data = results.data[i];

                                if (data.code && data.number) {
                                    self.get('store').createRecord('course-code', {
                                        code: data.code,
                                        number: data.number,
                                        name: data.name,
                                        unit: data.unit
                                    }).save().then(function () {
                                        ++courseCodeCount;
                                        if (++count === results.data.length) {
                                            self.set('uploadPercent', 75);
                                            self.$('.ui.upload.progress .label').html('Creating Program Records + Grades');
                                            setTimeout(function () {
                                                next();
                                            }, 1000);
                                        }
                                    }, function () {
                                        self.$('.ui.upload.progress .label').html('Error Creating Term Codes');
                                        self.$('.ui.upload.progress').addClass('error');
                                    });
                                } else {
                                    ++count;
                                }
                            }
                        }
                    });
                })
                .then(function (next) {
                  Papa.parse(_.findWhere(csvData, {name: 'grades+programrecords.xlsx'}).data, {
                        header: true,
                        complete: function (results) {
                            let count = 0;
                            var uniqueData = [];

                            outer:
                                for (let i = 0; i < results.data.length; i++) {

                                    let data = results.data[i];

                                    if (data.student) {
                                        for (let j = 0; j < uniqueData.length; j++) {
                                            if (data.degreeCode === uniqueData[j].degreeCode &&
                                                data.termCode === uniqueData[j].termCode) {
                                                continue outer;
                                            }
                                        }
                                        uniqueData.push(data);
                                    }
                                }

                            for (let i = 0; i < uniqueData.length; i++) {

                                let data = uniqueData[i];

                                (function (data) {

                                    var degreeCode = null,
                                        termCode = null;

                                    Sequence.Sequence.create()
                                        .then(function (next) {
                                            self.get('store').queryRecord('degree-code', {name: data.degreeCode}).then(function (res) {
                                                degreeCode = res;
                                                next();
                                            });
                                        })
                                        .then(function (next) {
                                            self.get('store').queryRecord('term-code', {name: data.termCode}).then(function (res) {
                                                termCode = res;
                                                next();
                                            });
                                        })
                                        .then(function () {
                                            self.get('store').createRecord('program-record', {
                                                level: data.level,
                                                status: data.status,
                                                comment: data.comment,
                                                degreeCode: degreeCode,
                                                termCode: termCode
                                            }).save().then(function () {
                                                ++programRecordCount;
                                                if (++count === uniqueData.length) {
                                                    next();
                                                }
                                            }, function () {
                                                self.$('.ui.upload.progress .label').html('Error Creating Term Codes');
                                                self.$('.ui.upload.progress').addClass('error');
                                            });
                                        });
                                })(data);
                            }
                        }
                    });
                })
                .then(function () {
                  Papa.parse(_.findWhere(csvData, {name: 'grades+programrecords.xlsx'}).data, {
                        header: true,
                        complete: function (results) {
                            let count = 0,
                                student = null,
                                degreeCode = null,
                                termCode = null;

                            for (let i = 0; i < results.data.length; i++) {

                                let data = results.data[i];

                                if (data.student) {
                                    student = data.student;
                                    degreeCode = data.degreeCode;
                                    termCode = data.termCode;
                                }

                                if (data.mark) {

                                    (function (data, student, degreeCode, termCode) {

                                        var courseCodeObj = null,
                                            degreeCodeObj = null,
                                            termCodeObj = null,
                                            studentObj = null,
                                            programRecordObj = null;

                                        Sequence.Sequence.create()
                                            .then(function (next) {
                                                self.get('store').queryRecord('course-code', {
                                                    code: data.code,
                                                    number: data.number,
                                                    name: data.name,
                                                    unit: data.unit
                                                }).then(function (res) {
                                                    courseCodeObj = res;
                                                    next();
                                                });
                                            })
                                            .then(function (next) {
                                                self.get('store').queryRecord('degree-code', {
                                                    name: degreeCode
                                                }).then(function (res) {
                                                    degreeCodeObj = res;
                                                    next();
                                                });
                                            })
                                            .then(function (next) {
                                                self.get('store').queryRecord('term-code', {
                                                    name: termCode
                                                }).then(function (res) {
                                                    termCodeObj = res;
                                                    next();
                                                });
                                            })
                                            .then(function (next) {
                                                self.get('store').queryRecord('student', {
                                                    studentNumber: student
                                                }).then(function (res) {
                                                    studentObj = res;
                                                    next();
                                                });
                                            })
                                            .then(function (next) {
                                                self.get('store').queryRecord('program-record', {
                                                    degreeCode: degreeCodeObj.get('id'),
                                                    termCode: termCodeObj.get('id')
                                                }).then(function (res) {
                                                    programRecordObj = res;
                                                    next();
                                                });
                                            })
                                            .then(function () {
                                                self.get('store').createRecord('grade', {
                                                    mark: data.mark,
                                                    section: data.section,
                                                    courseCode: courseCodeObj,
                                                    programRecord: programRecordObj,
                                                    student: studentObj
                                                }).save().then(function () {
                                                    ++gradeCount;
                                                    if (++count === results.data.length) {
                                                        self.set('uploadPercent', 100);
                                                        self.$('.ui.upload.progress .label').html('Done');
                                                        self.$('.ui.small.upload.modal .actions').html(
                                                            '<button class="ui green deny button">' +
                                                            '<i class="checkmark icon"></i>Ok' +
                                                            '</button>');
                                                        self.$('.ui.small.upload .content').append(
                                                            '<div class="ui info message">' +
                                                            '<div class="header">Results of Uploading Data</div>' +
                                                            '<ul>' +
                                                            '<li>Number of degree codes uploaded: ' + degreeCodeCount + '</li>' +
                                                            '<li>Number of term codes uploaded: ' + termCodeCount + '</li>' +
                                                            '<li>Number of course codes uploaded: ' + courseCodeCount + '</li>' +
                                                            '<li>Number of program records uploaded: ' + programRecordCount + '</li>' +
                                                            '<li>Number of grades uploaded: ' + gradeCount + '</li>' +
                                                            '</ul>' +
                                                            '</div>'
                                                        );
                                                        self.$('.ui.error.message').remove();
                                                        self.$('.ui.file-upload.grid .column.file').remove();
                                                        self.$('.ui.file-upload.grid .fourteen.wide.column').show();
                                                        self.set('files', []);
                                                    }
                                                }, function () {
                                                    self.$('.ui.upload.progress .label').html('Error Creating Term Codes');
                                                    self.$('.ui.upload.progress').addClass('error');
                                                });
                                            });
                                    })(data, student, degreeCode, termCode);
                                } else {
                                    ++count;
                                }
                            }
                        }
                    });
                });
        },
        clear: function () {
            this.$('.ui.error.message').remove();
            this.$('.ui.file-upload.grid .column.file').remove();
            this.$('.ui.file-upload.grid .fourteen.wide.column').show();
            this.set('files', []);
        },
        closeError: function () {
            this.$('.ui.error.basic.modal').modal('hide');
        }
    }

});
