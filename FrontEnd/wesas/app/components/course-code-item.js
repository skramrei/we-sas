import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),
    tagName: 'tr',

    mouseEnter: function () {
        if (!this.get('isEditing') && !this.get('isRemoving')) {
            this.$('td.right.aligned > button').css('visibility', 'visible');
        }
    },
    mouseLeave: function () {
        this.$('td.right.aligned > button').css('visibility', 'hidden');
    },

    willDestroyElement: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },
    didRender: function () {
        // NOTE: The content of the popup is the button's title element by default
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },

    isEditing: false,
    isRemoving: false,

    actions: {
        edit: function (code, number, name, unit) {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('code', code);
            this.set('number', number);
            this.set('name', name);
            this.set('unit', unit);
            this.set('isEditing', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        warning: function () {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('isRemoving', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        cancel: function () {
            this.set('isEditing', false);
            this.set('isRemoving', false);
        },
        submit: function (id, code, number, name, unit) {

            var self = this;

            this.get('store').find('course-code', id).then(function (courseCode) {
                courseCode.set('code', code);
                courseCode.set('number', number);
                courseCode.set('name', name);
                courseCode.set('unit', unit);
                courseCode.save().then(function () {
                    self.get('notify').success(self.get('notifyMessages').editSuccess('Course Code', name));
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').editFail(name), {closeAfter: null});
                });
            });

            this.set('code', '');
            this.set('number', '');
            this.set('name', '');
            this.set('unit', '');
            this.set('isEditing', false);
        },
        remove: function (id) {

            var self = this;

            this.get('store').find('course-code', id).then(function (courseCode) {

                for (var i = 0; i < courseCode.grades.length; i++) {
                    courseCode.grades[i] = null;
                }

                courseCode.save().then(function () {
                    courseCode.destroyRecord().then(function () {
                        self.get('notify').success(self.get('notifyMessages').deleteSuccess('Course Code'));
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').deleteFail('Course Code'), {closeAfter: null});
                    });
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').deleteFail('Course Code'), {closeAfter: null});
                });
            });

            this.set('isRemoving', false);
        }
    }
});
