import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    provinces: function () {
        return this.get('store').findAll('province');
    }.property(),

    isCreating: false,
    provinceId: null,
    nameError: false,
    provinceError: false,

    actions: {
        setProvinceId: function (comp, id) {
            this.set('provinceId', id);
            this.set('provinceError', false);
            this.$('#city-create-province').removeClass('field-error');
        },
        create: function () {
            this.set('isCreating', true);
        },
        cancel: function () {
            this.set('isCreating', false);
            this.set('nameError', false);
            this.$('#city-create-name').removeClass('field-error');
            this.set('provinceError', false);
            this.$('#city-create-province').removeClass('field-error');
        },
        cityInputEntered: function () {
            this.set('nameError', false);
            this.$('#city-create-name').removeClass('field-error');
        },
        submit: function (name) {

            // Validate inputs
            var foundInvalidFields = false;
            if (name === undefined || name === null || name === "") {
                this.set('nameError', true);
                this.$('#city-create-name').addClass('field-error');
                foundInvalidFields = true;
            }
            if (this.get('provinceId') === null) {
                this.set('provinceError', true);
                this.$('#city-create-province').addClass('field-error');
                foundInvalidFields = true;
            }
            if (foundInvalidFields) {
                return;
            }

            // All inputs are valid so continue with city creation
            var self = this;

            this.get('store').createRecord('city', {
                name: name,
                province: self.get('store').peekRecord('province', self.get('provinceId'))
            }).save().then(function () {
                self.get('notify').success(self.get('notifyMessages').createSuccess('City', name));
            }, function () {
                self.get('notify').alert(self.get('notifyMessages').createFail(name), {closeAfter: null});
            });

            this.set('name', '');
            this.set('provinceId', null);
            this.set('isCreating', false);
        }
    }
});
