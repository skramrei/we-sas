import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),

    users: function () {
        return this.get('store').findAll('user');
    }.property()
});