import Ember from 'ember';

function getProgramFromId (obj, id) {
    if (obj != null && id != null) {
        obj.get('store').findRecord('academic-program-code', id).then((programCode) => {
            obj.set('_program', programCode.get('name'));
            obj.set('programLoading', false);
            obj.notifyPropertyChange('program');
        }, () => {
            obj.get('notify').alert('Error loading academic programs.', { closeAfter: null });
        });
    }
}

function getStudentNameFromId (obj, id) {
    if (obj != null && id != null) {
        obj.get('store').findRecord('student', id).then((student) => {
            obj.set('_firstName', student.get('firstName'));
            obj.set('_lastName', student.get('lastName'));
            obj.set('studentLoading', false);
            obj.notifyPropertyChange('studentName');
        }, () => {
            obj.get('notify').alert('Error loading student names.', { closeAfter: null });
        });
    }
}

export default Ember.Component.extend({

  store: Ember.inject.service(),
  notify: Ember.inject.service('notify'),
  notifyMessages: Ember.inject.service('notify-messages'),

  isEditingItrProgram: false,

  eligibility: false,
  programLoading: false,
  studentLoading: false,
  _program: null,
  _student: null,
  _firstName: null,
  _lastName: null,

  tagName: 'tr',

  willDestroyElement: function() {
      this._super(...arguments);
      // Remove the popup explicitly or it may stay after the row is gone
      this.$('.circular.icon.button').popup('destroy');
      this.set('programLoading', false);
      this.set('studentLoading', false);
      this.set('_program', null);
      this.set('_firstName', null);
      this.set('_lastName', null);
  },
  didRender: function () {
      // NOTE: The content of the popup is the button's title element by default
      this._super(...arguments);
      this.$('.circular.icon.button').popup({position: 'top center', inline: true});
  },

  mouseEnter: function () {
    this.$('td.right.aligned > button').css('visibility', 'visible');
  },
  mouseLeave: function () {
    this.$('td.right.aligned > button').css('visibility', 'hidden');
  },

  program: function () {
      if (this.get('_program') == null) {
          // Load the program
          this.set('programLoading', true);
          getProgramFromId(this, this.get('itr_program.program.id'));
      }
      else {
          // The program has been loaded
          return this.get('_program');
      }
  }.property(),

  studentName: function () {
      if (this.get('_firstName') == null) {
          // Load the program
          this.set('studentLoading', true);
          getStudentNameFromId(this, this.get('itr_program.student.id'));
      }
      else {
          // The name has been loaded
          return this.get('_firstName') + ' ' + this.get('_lastName');
      }
  }.property(),

  actions: {

    editItrProgram: function (id) {
      var store = this.get('store');
      var self = this;

      this.$('td.right.aligned > button').css('visibility', 'hidden');
      this.set('isEditingItrProgram', true);
      this.$('.popup.visible').removeClass('visible').addClass('hidden');

      store.find('itr-program', id).then(function (itr_program) {
        self.set('_student',itr_program.get('student'));
        self.set('_program',itr_program.get('program'));
      });
    },
    cancelItrProgramChanges: function () {
      this.set('isEditingItrProgram', false);
    },
    submitItrProgramChanges: function (id, order) {

      var store = this.get('store');
      var eligibility = this.get('eligibility');
      var self = this;

      store.find('itr-program', id).then(function (itr_program) {
           itr_program.set('student',self.get('_student'));
           itr_program.set('program',self.get('_program'));
           itr_program.set('order', order);
           itr_program.set('eligibility', eligibility);

           itr_program.save().then(() => {
               self.get('notify').success(self.get('notifyMessages').editSuccess('ITR Program', ''));
           }, () => {
               self.get('notify').alert(self.get('notifyMessages').editFail('ITR Program'), {
                   closeAfter: null
               });
           });

       });


      this.set('order', '');
      this.set('eligibility', false);
      this.set('isEditingItrProgram', false);
    },
    deleteItrProgram: function (id) {

        this.$('.popup.visible').removeClass('visible').addClass('hidden');

      var store = this.get('store');
      var self = this;

      var check = confirm("Are you sure you want to delete this ITR Program?");
      if (check) {
        store.find('itr-program', id).then(function (itr_program) {

          itr_program.set('student', null);
          itr_program.set('program', null);

            itr_program.save().then(() => {
              itr_program.destroyRecord();
                self.get('notify').success(self.get('notifyMessages').deleteSuccess('ITR Program'));
            }, () => {
                self.get('notify').alert(self.get('notifyMessages').deleteFail('ITR Program'), {
                    closeAfter: null
                });
            });
        });
      }
    }
  }
});
