import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    isClearingItrProgram: false,

    actions: {
    	clearItrData: function() {
            this.set('isClearingItrProgram', true);
        },
        clear: function() {
        	this.get('store').query('itr-program', {clearAll: true});
            this.set('isClearingItrProgram', false);
        },
        cancel: function() {
            this.set('isClearingItrProgram', false);
        }
    }
});