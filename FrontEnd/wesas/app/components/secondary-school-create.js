import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    students: function () {
        return this.get('store').findAll('student');
    }.property(),

    actions: {

        setStudent: function (comp, id) {
            this.set('student', this.get('store').find('student', id));
        },
        create: function () {
            this.$('.ui.create.modal').modal('show');
        },
        close: function () {
            this.$('.ui.create.modal').modal('hide');
        },
        submit: function () {
            this.get('store').createRecord('secondary-school', {
                name: this.get('name'),
                student: this.get('student')
            }).save().then(() => {
                this.get('notify').success(this.get('notifyMessages').createSuccess('Secondary School', this.get('name')));
            }, () => {
                this.get('notify').alert(this.get('notifyMessages').createFail(this.get('name')), {
                    closeAfter: null
                });
            });
            this.set('name', '');
        }
    }
});
