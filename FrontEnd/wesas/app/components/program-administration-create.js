import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    departments: function () {
        return this.get('store').findAll('department');
    }.property(),
    academicProgramCodes: function () {
        return this.get('store').findAll('academic-program-code');
    }.property(),

    isCreating: false,
    departmentId: null,
    academicProgramCodeId: null,

    actions: {
        setDepartmentId: function (comp, id) {
            this.set('departmentId', id);
        },
        setAcademicProgramCodeId: function (comp, id) {
            this.set('academicProgramCodeId', id);
        },
        create: function () {
            this.set('isCreating', true);
        },
        cancel: function () {
            this.set('isCreating', false);
        },
        submit: function (name, position) {

            var self = this;

            this.get('store').createRecord('program-administration', {
                name: name,
                position: position,
                department: self.get('store').peekRecord('department', self.get('departmentId')),
                academicProgramCode: self.get('store').peekRecord('academic-program-code', self.get('academicProgramCodeId'))
            }).save().then(function () {
                self.get('notify').success(self.get('notifyMessages').createSuccess('Program Administration', name));
            }, function () {
                self.get('notify').alert(self.get('notifyMessages').createFail(name), {closeAfter: null});
            });

            this.set('name', '');
            this.set('position', '');
            this.set('departmentId', null);
            this.set('academicProgramCodeId', null);
            this.set('isCreating', false);
        }
    }
});
