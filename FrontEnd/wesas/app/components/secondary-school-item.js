import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),
    tagName: 'tr',

    willDestroyElement: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },
    didRender: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },

    mouseEnter: function () {
        if (!this.get('isViewing')) {
            this.$('td.right.aligned > button').css('visibility', 'visible');
        }
    },
    mouseLeave: function () {
        this.$('td.right.aligned > button').css('visibility', 'hidden');
    },

    loadStudents: function () {
        this.set('students', this.get('store').findAll('student'));
        this.get('secondarySchool').get('student').then((student) => {
            this.set('student', student);
        });
    }.property(),

    isViewing: false,

    actions: {

        setStudent: function (comp, id) {
            this.set('student', this.get('store').find('student', id));
        },
        edit: function (id) {
            this.$('.ui.view.modal').modal('show');
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
            this.set('isViewing', true);

            var self = this;
            this.get('store').find('secondary-school', id).then(function (secondarySchool) {

                self.set('name', secondarySchool.get('name'));

                secondarySchool.get('student').then(function (student) {
                    self.set('student', student);
                    self.$('.student.selection').dropdown('set selected', student.get('studentNumber'));
                });
            });
        },
        close: function () {
            this.$('.ui.view.modal').modal('hide');
            this.$('.ui.small.basic.delete.modal').modal('hide');
            this.set('isViewing', false);
        },
        submit: function (id) {

            var self = this;

            this.get('store').find('secondary-school', id).then(function (secondarySchool) {
                secondarySchool.set('name', self.get('name'));
                secondarySchool.set('student', self.get('student'));
                secondarySchool.save().then(function () {
                    self.get('notify').success(self.get('notifyMessages').editSuccess('Secondary School', self.get('name')));
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').editFail('Secondary School'), {closeAfter: null});
                });
            });

            this.set('isViewing', false);
            this.$('.ui.view.modal').modal('hide');
        },

        warning: function () {
            this.$('.ui.small.basic.delete.modal').modal('show');
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
            this.set('isViewing', true);
        },
        remove: function (id) {

            var self = this;

            this.get('store').find('secondary-school', id).then(function (secondarySchool) {

                secondarySchool.set('student', null);

                for (var i = 0; i < secondarySchool.courseInfo.length; i++) {
                    secondarySchool.courseInfo[i] = null;
                }

                secondarySchool.save().then(function () {
                    secondarySchool.destroyRecord().then(function () {
                        self.get('notify').success(self.get('notifyMessages').deleteSuccess('Secondary School'));
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').deleteFail('Secondary School'), {closeAfter: null});
                    });
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').deleteFail('Secondary School'), {closeAfter: null});
                });
            });

            this.set('isViewing', false);
            this.$('.ui.small.basic.delete.modal').modal('hide');
        }
    }

});
