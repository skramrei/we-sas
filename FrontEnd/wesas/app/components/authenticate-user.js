import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    routing: Ember.inject.service('-routing'),

    isPasswordChanging: null,
    tempPassword: null,
    error: null,

    errorMessage: Ember.computed('error', function () {
        return this.get('error');
    }),

    actions: {
        login: function () {

            this.$('.ui.error.message').remove();
            this.$('.username.field').removeClass('error');
            this.$('.password.field').removeClass('error');

            var authentication = this.get('oudaAuth');
            var self = this;

            if (this.get('name') === "root") {
                authentication.openRoot(this.get('password')).then(function (name) {
                    authentication.set('getName', name);
                    self.get('routing').transitionTo('home');
                }, function (error) {
                    console.log(error);
                });
            } else {
                authentication.open(this.get('name'), this.get('password')).then(function (userRole) {
                    //console.log(userRole);
                  userRole= userRole;
                    authentication.set('getName', self.get('name'));
                    self.set('error', null);
                    self.get('routing').transitionTo('home');
                }, function (error) {
                    if (error === "passwordReset") {
                        self.set('isPasswordChanging', true);
                    } else {
                        if (error === "wrongUserName") {
                            self.$('.username.field').addClass('error');
                            self.$('.ui.form').after('<div class="ui error message">' +
                                '<ul class="list">' +
                                '<li>Error: Incorrect username</li>' +
                                '</ul>' +
                                '</div>');
                        } else {
                            if (error === "wrongPassword") {
                                self.$('.password.field').addClass('error');
                                self.$('.ui.form').after('<div class="ui error message">' +
                                    '<ul class="list">' +
                                    '<li>Error: Incorrect password</li>' +
                                    '</ul>' +
                                    '</div>');
                            } else {
                                if (error === "loginFailed") {
                                    self.$('.username.field').addClass('error');
                                    self.$('.password.field').addClass('error');
                                    self.$('.ui.form').after('<div class="ui error message">' +
                                        '<ul class="list">' +
                                        '<li>Error: Login failed</li>' +
                                        '</ul>' +
                                        '</div>');
                                }
                            }
                        }
                    }
                });
            }
        },

        save: function () {
            var authentication = this.get('oudaAuth');
            var myStore = this.get('store');
            var userName = this.get('name');
            var hashedPassword = authentication.hash(this.get('firstPassword'));
            var self = this;
            myStore.queryRecord('password', {filter: {userName: userName}}).then(function (userShadow) {
                userShadow.set('encryptedPassword', hashedPassword);
                userShadow.set('passwordMustChanged', true);
                userShadow.set('passwordReset', false);
                userShadow.save().then(function () {
                    self.get('oudaAuth').close();
                    self.set('isPasswordChanging', false);
                    //self.get('routing').transitionTo('login');
                });
            });
        }
    }
});
