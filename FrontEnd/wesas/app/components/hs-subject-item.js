import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),
    tagName: 'tr',

    willDestroyElement: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },
    didRender: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },

    mouseEnter: function () {
        if (!this.get('isViewing')) {
            this.$('td.right.aligned > button').css('visibility', 'visible');
        }
    },
    mouseLeave: function () {
        this.$('td.right.aligned > button').css('visibility', 'hidden');
    },

    isViewing: false,

    actions: {

        edit: function (id) {
            this.$('.ui.view.modal').modal('show');
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
            this.set('isViewing', true);

            var self = this;
            this.get('store').find('hs-subject', id).then(function (hsSubject) {
                self.set('name', hsSubject.get('name'));
                self.set('description', hsSubject.get('description'));
            });
        },
        close: function () {
            this.$('.ui.view.modal').modal('hide');
            this.$('.ui.small.basic.delete.modal').modal('hide');
            this.set('isViewing', false);
        },

        submit: function (id) {

            var self = this;

            this.get('store').find('hs-subject', id).then(function (hsSubject) {
                hsSubject.set('name', self.get('name'));
                hsSubject.set('description', self.get('description'));
                hsSubject.save().then(function () {
                    self.get('notify').success(self.get('notifyMessages').editSuccess('High School Subjects', self.get('name')));
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').editFail('High School Subjects'), {closeAfter: null});
                });
            });

            this.set('isViewing', false);
            this.$('.ui.view.modal').modal('hide');
        },

        warning: function () {
            this.$('.ui.small.basic.delete.modal').modal('show');
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
            this.set('isViewing', true);
        },
        remove: function (id) {

            var self = this;

            this.get('store').find('hs-subject', id).then(function (hsSubject) {

                for (var i = 0; i < hsSubject.marks.length; i++) {
                    hsSubject.marks[i] = null;
                }

                hsSubject.save().then(function () {
                    hsSubject.destroyRecord().then(function () {
                        self.get('notify').success(self.get('notifyMessages').deleteSuccess('High School Subjects'));
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').deleteFail('High School Subjects'), {closeAfter: null});
                    });
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').deleteFail('High School Subjects'), {closeAfter: null});
                });
            });

            this.set('isViewing', false);
            this.$('.ui.small.basic.delete.modal').modal('hide');
        }
    }
});
