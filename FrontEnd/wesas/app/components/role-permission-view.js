import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),

    rolePermissions: function () {
        return this.get('store').findAll('role-permission');
    }.property(),

    MF001IsPermitted: Ember.computed(function () {
        var authentication = this.get('oudaAuth');
        if (authentication.getName === "Root") {
            return true;
        } else {
            return (authentication.get('userCList').indexOf("MF001") >= 0);
        }
    })
});
