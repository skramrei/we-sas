import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    isCreating: false,

    actions: {
        create: function () {
            this.set('isCreating', true);
        },
        cancel: function () {
            this.set('isCreating', false);
        },
        submit: function (code, number, name, unit) {

            var self = this;

            this.get('store').createRecord('course-code', {
                code: code,
                number: number,
                name: name,
                unit: unit
            }).save().then(function () {
                self.get('notify').success(self.get('notifyMessages').createSuccess('Course Code', name));
            }, function () {
                self.get('notify').alert(self.get('notifyMessages').createFail(name), {closeAfter: null});
            });

            this.set('code', '');
            this.set('number', '');
            this.set('name', '');
            this.set('unit', '');
            this.set('isCreating', false);
        }
    }
});
