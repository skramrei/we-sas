import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),
    tagName: 'tr',

    mouseEnter: function () {
        if (!this.get('isEditing') && !this.get('isRemoving')) {
            this.$('td.right.aligned > button').css('visibility', 'visible');
        }
    },
    mouseLeave: function () {
        this.$('td.right.aligned > button').css('visibility', 'hidden');
    },

    willDestroyElement: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },
    didRender: function () {
        // NOTE: The content of the popup is the button's title element by default
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },

    departments: function () {
        return this.get('store').findAll('department');
    }.property(),
    academicProgramCodes: function () {
        return this.get('store').findAll('academic-program-code');
    }.property(),

    isEditing: false,
    isRemoving: false,
    departmentId: null,
    academicProgramCodeId: null,

    actions: {
        setDepartmentId: function (comp, id) {
            this.set('departmentId', id);
        },
        setAcademicProgramCodeId: function (comp, id) {
            this.set('academicProgramCodeId', id);
        },
        edit: function (name, position) {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('name', name);
            this.set('position', position);
            this.set('isEditing', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        warning: function () {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('isRemoving', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        cancel: function () {
            this.set('isEditing', false);
            this.set('isRemoving', false);
        },
        submit: function (id, name, position) {

            var self = this;
            var department = this.get('store').peekRecord('department', this.get('departmentId'));
            var academicProgramCode = this.get('store').peekRecord('academic-program-code', this.get('academicProgramCodeId'));

            this.get('store').find('program-administration', id).then(function (programAdministration) {
                programAdministration.set('name', name);
                programAdministration.set('position', position);
                programAdministration.set('department', department);
                programAdministration.set('academicProgramCode', academicProgramCode);
                programAdministration.save().then(function () {
                    self.get('notify').success(self.get('notifyMessages').editSuccess('Program Administration', name));
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').editFail(name), {closeAfter: null});
                });
            });

            this.set('name', '');
            this.set('position', '');
            this.set('departmentId', null);
            this.set('academicProgramCodeId', null);
            this.set('isEditing', false);
        },
        remove: function (id) {

            var self = this;

            this.get('store').find('program-administration', id).then(function (programAdministration) {

                programAdministration.set('department', null);
                programAdministration.set('academicProgramCode', null);

                programAdministration.save().then(function () {
                    programAdministration.destroyRecord().then(function () {
                        self.get('notify').success(self.get('notifyMessages').deleteSuccess('Program Administration'));
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').deleteFail('Program Administration'), {closeAfter: null});
                    });
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').deleteFail('Program Administration'), {closeAfter: null});
                });
            });

            this.set('isRemoving', false);
        }
    }
});
