import Ember from 'ember';
//import Sequence from 'npm:sequence';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    isCreatingStudent: false,
    selectedResidency: null,
    selectedGender: null,
    selectedStudyLoad: null,
    selectedCountry: null,
    selectedProvince: null,
    selectedCity: null,
    DOB: null,

    residencies: function(){
        return this.get('store').findAll('residency');
    }.property(),

    genders: function(){
        return this.get('store').findAll('gender');
    }.property(),

    studyLoads: function(){
        return this.get('store').findAll('academicLoad');
    }.property(),

    countries: function(){
        return this.get('store').findAll('country');
    }.property(),

    provinces: function(){
        return this.get('store').findAll('province');
    }.property(),

    cities: function(){
        return this.get('store').findAll('city');
    }.property(),

    actions: {
        // The first parameter 'component' of the below functions MUST be there
        setCountry: function (component, country) {
            this.set('selectedCountry', country);
        },
        setGender: function (component, gender) {
            this.set('selectedGender', gender);
        },
        setStudyLoad: function (component, studyLoad) {
            this.set('selectedStudyLoad', studyLoad);
        },
        setResInfo: function (component, resInfo) {
            this.set('selectedResidency', resInfo);
        },
        setProvince: function (component, province) {
            this.set('selectedProvince', province);
        },
        setCity: function (component, city) {
            this.set('selectedCity', city);
        },
        createStudent: function() {
            this.set('isCreatingStudent', true);
        },
        cancelCreateStudent: function() {
            this.set('isCreatingStudent', false);
        },
        submitStudent: function(studentNumber, firstName, lastName) {

            var store = this.get('store'),
            _DOB = this.get('DOB'),
            chosenResidency = store.peekRecord('residency', this.get ('selectedResidency')),
            chosenStudyLoad = store.peekRecord('academicLoad', this.get ('selectedStudyLoad')),
            chosenGender = store.peekRecord('gender', this.get ('selectedGender')),
            chosenCountry = store.peekRecord('country', this.get ('selectedCountry')),
            chosenProvince = store.peekRecord('province', this.get ('selectedProvince')),
            chosenCity = store.peekRecord('city', this.get ('selectedCity'));

            store.createRecord('student', {
                studentNumber: studentNumber,
                firstName: firstName,
                lastName: lastName,
                DOB: new Date(_DOB),
                studyLoad: chosenStudyLoad,
                resInfo: chosenResidency,
                gender: chosenGender,
                country: chosenCountry,
                province: chosenProvince,
                city: chosenCity

            }).save().then(() => {
                this.get('notify').success(this.get('notifyMessages').createSuccess('Student', firstName + ' ' + lastName));
            }, () => {
                this.get('notify').alert(this.get('notifyMessages').createFail(firstName + ' ' + lastName), {
                    closeAfter: null
                });
            });

            this.set('studentNumber', '');
            this.set('firstName', '');
            this.set('lastName', '');
            this.set('DOB', '');
            this.set('studyLoad', '');
            this.set('resInfo', '');
            this.set('gender', '');
            this.set('country', '');
            this.set('province', '');
            this.set('city', '');
            this.set('isCreatingStudent', false);
        }
    }
});
