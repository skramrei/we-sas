import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    hsSubjects: function () {
        return this.get('store').findAll('hs-subject');
    }.property(),
    secondarySchools: function () {
        return this.get('store').findAll('secondary-school');
    }.property(),

    actions: {

        setSubject: function (comp, id) {
            this.set('subject', this.get('store').find('hs-subject', id));
        },
        setSchool: function (comp, id) {
            this.set('school', this.get('store').find('secondary-school', id));
        },

        create: function () {
            this.$('.ui.create.modal').modal('show');
        },
        close: function () {
            this.$('.ui.create.modal').modal('hide');
        },
        submit: function () {
            this.get('store').createRecord('hs-course-mark', {
                level: this.get('level'),
                source: this.get('source'),
                unit: this.get('unit'),
                grade: this.get('grade'),
                subject: this.get('subject'),
                school: this.get('school')
            }).save().then(() => {
                this.get('notify').success(this.get('notifyMessages').createSuccess('High School Course Marks', this.get('grade')));
            }, () => {
                this.get('notify').alert(this.get('notifyMessages').createFail(this.get('grade')), {
                    closeAfter: null
                });
            });

            this.set('level', '');
            this.set('source', '');
            this.set('unit', '');
            this.set('grade', '');
        }
    }
});
