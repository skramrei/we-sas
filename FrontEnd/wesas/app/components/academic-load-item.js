import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),
    tagName: 'tr',

    mouseEnter: function () {
        if (!this.get('isEditing') && !this.get('isRemoving')) {
            this.$('td.right.aligned > button').css('visibility', 'visible');
        }
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },
    mouseLeave: function () {
        this.$('td.right.aligned > button').css('visibility', 'hidden');
    },

    willDestroyElement: function () {
        // Remove the popup explicitly or it may stay after the row is gone
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },

    isEditing: false,
    isRemoving: false,

    actions: {
        edit: function () {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('isEditing', true);
        },
        warning: function () {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('isRemoving', true);
        },
        cancel: function () {
            this.set('isEditing', false);
            this.set('isRemoving', false);
            this.set('name', null);
        },
        submit: function (id, name) {

            if (name === undefined || name === null || name === "") {
                // There is nothing to update so return
                this.get('notify').warning(this.get('notifyMessages').noChanges('Academic Load'));
                this.set('isEditing', false);
                return;
            }

            var self = this;

            this.get('store').find('academic-load', id).then(function (academicLoad) {
                academicLoad.set('name', name);
                academicLoad.save().then(function () {
                    self.get('notify').success(self.get('notifyMessages').editSuccess('Academic Load', name));
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').editFail(name), {closeAfter: null});
                });
            });

            this.set('name', null);
            this.set('isEditing', false);
        },
        remove: function (id) {

            var self = this;

            this.get('store').find('academic-load', id).then(function (academicLoad) {

                for (var i = 0; i < academicLoad.students.length; i++) {
                    academicLoad.students[i] = null;
                }

                academicLoad.save().then(function () {
                    academicLoad.destroyRecord().then(function () {
                        self.get('notify').success(self.get('notifyMessages').deleteSuccess('Academic Load'));
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').deleteFail('Academic Load'), {closeAfter: null});
                    });
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').deleteFail('Academic Load'), {closeAfter: null});
                });
            });

            this.set('isRemoving', false);
        }
    }
});
