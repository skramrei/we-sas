import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),
    tagName: 'tr',

    willDestroyElement: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },
    didRender: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },

    mouseEnter: function () {
        if (!this.get('isViewing')) {
            this.$('td.right.aligned > button').css('visibility', 'visible');
        }
    },
    mouseLeave: function () {
        this.$('td.right.aligned > button').css('visibility', 'hidden');
    },

    load: function () {
        this.set('basisOfAdmissions', this.get('store').findAll('basis-of-admission'));
        this.get('code').get('basis').then((basis) => {
            this.set('basis', basis);
        });
    }.property(),

    isViewing: false,

    actions: {

        setAdmission: function (comp, id) {
            this.set('basis', this.get('store').find('basis-of-admission', id));
        },
        edit: function (id) {
            this.$('.ui.view.modal').modal('show');
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
            this.set('isViewing', true);

            var self = this;
            this.get('store').find('basis-of-admission-code', id).then(function (code) {

                self.set('name', code.get('name'));

                code.get('basis').then(function (basis) {
                    self.set('basis', basis);
                    if (basis) {
                        self.$('.admission.selection').dropdown('set selected', basis.get('comment'));
                    }
                });
            });
        },
        close: function () {
            this.$('.ui.view.modal').modal('hide');
            this.$('.ui.small.basic.delete.modal').modal('hide');
            this.set('isViewing', false);
        },
        submit: function (id) {

            var self = this;

            this.get('store').find('basis-of-admission-code', id).then(function (code) {
                code.set('name', self.get('name'));
                code.set('basis', self.get('basis'));
                code.save().then(function () {
                    self.get('notify').success(self.get('notifyMessages').editSuccess('Basis of Admission Code', self.get('name')));
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').editFail('Basis of Admission Code'), {closeAfter: null});
                });
            });

            this.set('isViewing', false);
            this.$('.ui.view.modal').modal('hide');
        },

        warning: function () {
            this.$('.ui.small.basic.delete.modal').modal('show');
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
            this.set('isViewing', true);
        },
        remove: function (id) {

            var self = this;

            this.get('store').find('basis-of-admission-code', id).then(function (code) {

                code.set('basis', null);
                code.save().then(function () {
                    code.destroyRecord().then(function () {
                        self.get('notify').success(self.get('notifyMessages').deleteSuccess('Basis of Admission Code'));
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').deleteFail('Basis of Admission Code'), {closeAfter: null});
                    });
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').deleteFail('Basis of Admission Code'), {closeAfter: null});
                });
            });

            this.set('isViewing', false);
            this.$('.ui.small.basic.delete.modal').modal('hide');
        }
    }


   /* store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    isEditingBasisOfAdmissionCode: false,
    isRemoving: false,
    nameError: false,

    tagName: 'tr',

    mouseEnter: function () {
        if (!this.get('isEditing') && !this.get('isRemoving')) {
            this.$('td.right.aligned > button').css('visibility', 'visible');
        }
    },
    mouseLeave: function () {
        this.$('td.right.aligned > button').css('visibility', 'hidden');
    },

    willDestroyElement: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },
    didRender: function () {
        // NOTE: The content of the popup is the button's title element by default
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },

    actions: {
        resetFormFields: function () {

            this.set('name', null);

        },

        editBasisOfAdmissionCode: function () {
            this.set('isEditingBasisOfAdmissionCode', true);
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        cancelBasisOfAdmissionCodeChanges: function () {
            this.set('isEditingBasisOfAdmissionCode', false);
            this.set('isRemoving', false);
            this.send('resetFormFields');
        },
        submitBasisOfAdmissionCodeChanges: function (name) {

            // Set flags for the fields that need to be updated.
            var updateName = false;

            if (name!== undefined && name !== null && name !== "") {
                updateName = true;
                updateAnyField = true;
            }

            if (!updateAnyField) {
                // There is actually nothing to update so do nothing
                this.get('notify').warning(this.get('notifyMessages').noChanges('Basis of Admission Code'));
                this.set('isEditingBasisOfAdmissionCode', false);
                return;
            }

            // There must be something to update so continue
            var store = this.get('store');
            var eligibility = this.get('eligibility'); // TODO: What is this???
            var self = this;

            store.find('basis-of-admission-code', id).then(function (basis_of_admission_code) {

                if (updateName) {
                    basis_of_admission_code.set('name', name);
                }

                basis_of_admission_code.save().then(() => {
                    self.get('notify').success(self.get('notifyMessages').editSuccess('Basis of Admission Code', ''));
                }, () => {
                    self.get('notify').alert(self.get('notifyMessages').editFail('Basis of Admission Code'), {
                        closeAfter: null
                });
            });
        });

            this.send('resetFormFields');
            this.set('isEditingBasisOfAdmissionCode', false);
        },
        warning: function () {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('isRemoving', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        // remember to change this
        remove: function (id) {

            var self = this;

            this.get('store').find('basis-of-admission-code', id).then(function (basisOfAdmissionCode) {

                basisOfAdmissionCode.name = null;

                basisOfAdmissionCode.save().then(function () {
                    basisOfAdmissionCode.destroyRecord().then(function () {
                        self.get('notify').success(self.get('notifyMessages').deleteSuccess('Basis of Admission Code'));
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').deleteFail('Basis of Admission Code'), {closeAfter: null});
                    });
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').deleteFail('Basis of Admission Code'), {closeAfter: null});
                });
            });

            this.set('isRemoving', false);
        }
  }*/
});
