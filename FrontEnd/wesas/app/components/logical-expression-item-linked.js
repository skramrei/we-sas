import Ember from 'ember';

// Sets the private display expression field for this component as the name for a certain course code
function getCourseNameById (obj, id) {
    obj.get('store').findRecord('course-code', id).then((course) => {
        obj.set('_displayExpression', (course.get('code') + course.get('number')));
        obj.notifyPropertyChange('displayExpression');
    }, () => {
        obj.get('notify').alert('Error loading logical expressions.', { closeAfter: null });
    });
}

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    loadingExpression: false,
    _displayExpression: null,

    linkIsNotNone: function () {
        return this.get('exp.logicalLink') !== 'None';
    }.property(),

    displayExpression: function () {
        var expression = this.get('store').peekRecord('logical-expression', this.get('exp.id'));
        var expressionWords = expression.get('booleanExp').split(' ');

        if (expressionWords[0] === 'size') {
            return 'Total Number of Students ' + '<=' + ' ' + expressionWords[2];
        }
        else if (expressionWords[0] === 'average') {
            return 'Weighted Average ' + expressionWords[1] + ' ' + expressionWords[2];
        }
        else {
            // First word must be the id of a Course Code
            if (this.get('_displayExpression') == null) {
                // Update the Course Code Name
                this.set('loadingExpression', true);
                getCourseNameById(this, expressionWords[0]);
            }
            else {
                // Name must have been updated so return a value
                this.set('loadingExpression', false);
                return this.get('_displayExpression') + ' ' + expressionWords[1] + ' ' + expressionWords[2];
            }
        }
    }.property(),

    actions: {

        editExpression: function () {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('isEditing', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },

        submitEditedExpression: function (booleanExpression, link, isForFull, canceled) {
            if (canceled) {
                this.set('isEditing', false);
            } else {

                // Edit the expression
                var expressionToModify = this.get('store').peekRecord('logical-expression', this.get('exp.id'));

                expressionToModify.set('booleanExp', booleanExpression);
                expressionToModify.set('logicalLink', link);
                expressionToModify.set('ifFull', isForFull);

                expressionToModify.save()
                    .then((newExpression) => {
                        this.set('logical_expression', newExpression);
                        this.notifyPropertyChange('displayExpression');
                        this.get('notify').success(this.get('notifyMessages').editSuccess('Logical Expression', ''));
                    }, () => {
                        this.get('notify').alert(this.get('notifyMessages').editFail('Logical Expression'), {closeAfter: null});
                    });
            }
        },

        deleteExpression: function () {

            var expressionToDelete = this.get('store').peekRecord('logical-expression', this.get('exp.id'));

            expressionToDelete.destroyRecord()
                .then(() => {
                    this.get('notify').success(this.get('notifyMessages').deleteSuccess('Logical Expression'));
                }, () => {
                    this.get('notify').alert(this.get('notifyMessages').deleteFail('Logical Expression'), {closeAfter: null});
                });
        }
    }
});
