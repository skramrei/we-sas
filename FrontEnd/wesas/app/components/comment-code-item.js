import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    isEditingCommentCode: false,
    isRemoving: false,
    codeError: false,
    actionError: false,
    descriptionError: false,
    notesError: false,

    tagName: 'tr',

    mouseEnter: function () {
        if (!this.get('isEditing') && !this.get('isRemoving')) {
            this.$('td.right.aligned > button').css('visibility', 'visible');
        }
    },
    mouseLeave: function () {
        this.$('td.right.aligned > button').css('visibility', 'hidden');
    },

    willDestroyElement: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },
    didRender: function () {
        // NOTE: The content of the popup is the button's title element by default
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },

    actions: {
        resetFormFields: function () {
            this.set('code', null);
            this.set('progAction', null);
            this.set('description', null);
            this.set('notes', null);
        },

        editCommentCode: function () {
            this.set('isEditingCommentCode', true);
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        cancelCommentCodeChanges: function () {
            this.set('isEditingCommentCode', false);
            this.set('isRemoving', false);
            this.send('resetFormFields');
        },
        submitCommentCodeChanges: function (id, code, progAction, description, notes) {

            // Set flags for the fields that need to be updated.
            var updateCode = false;
            var updateAction = false;
            var updateDescription = false;
            var updateNotes = false;
            var updateAnyField = false;
            if (code !== undefined && code !== null && code !== "") {
                updateCode = true;
                updateAnyField = true;
            }
            if (progAction !== undefined && progAction !== null && progAction !== "") {
                updateAction = true;
                updateAnyField = true;
            }
            if (description !== undefined && description !== null && description !== "") {
                updateDescription = true;
                updateAnyField = true;
            }
            if (notes !== undefined && notes !== null && notes !== "") {
                updateNotes = true;
                updateAnyField = true;
            }
            if (!updateAnyField) {
                // There is actually nothing to update so do nothing
                this.get('notify').warning(this.get('notifyMessages').noChanges('Comment Code'));
                this.set('isEditingCommentCode', false);
                return;
            }

            // There must be something to update so continue
            var store = this.get('store');
           // var eligibility = this.get('eligibility'); // TODO: What is this???
            var self = this;

            store.find('comment-code', id).then(function (comment_code) {
                if (updateCode) {
                    comment_code.set('code', code);
                }
                if (updateAction) {
                    comment_code.set('progAction', progAction);
                }
                if (updateDescription) {
                    comment_code.set('description', description);
                }
                if (updateNotes) {
                    comment_code.set('notes', notes);
                }

                comment_code.save().then(() => {
                    self.get('notify').success(self.get('notifyMessages').editSuccess('Comment Code', ''));
                }, () => {
                    self.get('notify').alert(self.get('notifyMessages').editFail('Comment Code'), {
                        closeAfter: null
                });
            });
        });

            this.send('resetFormFields');
            this.set('isEditingCommentCode', false);
        },
        warning: function () {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('isRemoving', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        remove: function (id) {

            var self = this;

            this.get('store').find('comment-code', id).then(function (commentCode) {

                commentCode.comment = null;

                commentCode.save().then(function () {
                    commentCode.destroyRecord().then(function () {
                        self.get('notify').success(self.get('notifyMessages').deleteSuccess('Comment Code'));
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').deleteFail('Comment Code'), {closeAfter: null});
                    });
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').deleteFail('Comment Code'), {closeAfter: null});
                });
            });

            this.set('isRemoving', false);
        }
  }
});
