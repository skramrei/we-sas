import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),
    tagName: 'tr',

    willDestroyElement: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },
    didRender: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },

    mouseEnter: function () {
        if (!this.get('isViewing')) {
            this.$('td.right.aligned > button').css('visibility', 'visible');
        }
    },
    mouseLeave: function () {
        this.$('td.right.aligned > button').css('visibility', 'hidden');
    },

    rolePermissions: function () {
        return this.get('store').findAll('role-permission');
    }.property(),
    roleCodes: function () {
        return this.get('store').findAll('role-code');
    }.property(),

    rolePermissionId: null,
    isViewing: false,

    actions: {
        setRolePermissionId: function (comp, id) {
            this.set('rolePermissionId', id);
        },
        warning: function () {
            this.$('.ui.small.basic.modal').modal('show');
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
            this.set('isViewing', true);
        },
        remove: function (id, obj) {

            var self = this;

            this.get('store').find('role-code', id).then(function (roleCode) {
                roleCode.set('userRoles', []);
                roleCode.set('features', []);
                roleCode.save().then(function () {
                    roleCode.destroyRecord().then(function () {

                        self.get('notify').success(self.get('notifyMessages').deleteSuccess('Role Code'));

                        self.get('store').query('userRole', {filter: {role: id}}).then(function (userRole) {
                            userRole.forEach(function (oneRole) {
                                oneRole.user = null;
                                oneRole.role = null;
                                oneRole.save().then(function (toDelete) {
                                    toDelete.destroyRecord();
                                });
                            });
                        });

                        self.get('store').query('rolePermission', {filter: {roleCodes: id}}).then(function (features) {
                            features.forEach(function (oneFeature) {
                                var rolePermission = self.get('store').peekRecord('rolePermission', oneFeature.get('id'));
                                rolePermission.get('roleCodes').popObject(obj);
                                rolePermission.save();
                            });
                        });

                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').deleteFail('Role Permission'), {closeAfter: null});
                    });
                });
            });
            this.set('isViewing', false);
        },
        edit: function (roleCode) {
            this.$('.ui.edit.small.modal').modal('show');
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
            this.set('isViewing', true);
            this.set('name', roleCode.get('name'));

            var self = this;

            this.get('store').query('rolePermission', {filter: {roleCodes: roleCode.get('id')}}).then(function (features) {

                var IDs = [];
                features.forEach(function (feature) {
                    IDs.push(feature.get('code'));
                });

                self.$('.multiple.search.selection').dropdown('set selected', IDs);
            });
        },
        submit: function (id) {

            var name = this.get('name');
            var rolePermissionId = this.get('rolePermissionId');
            var self = this;

            this.get('store').find('role-code', id).then(function (roleCode) {
                roleCode.set('name', name);
                roleCode.save().then(function () {

                    self.get('notify').success(self.get('notifyMessages').editSuccess('Role Code', name));

                    self.get('store').query('rolePermission', {filter: {roleCodes: id}}).then(function (features) {
                        features.forEach(function (oneFeature) {
                            var rolePermission = self.get('store').peekRecord('rolePermission', oneFeature.get('id'));
                            rolePermission.get('roleCodes').popObject(roleCode);
                            rolePermission.save();
                        });
                    });

                    setTimeout(function () {
                        var rolePermissions = rolePermissionId.split(',');
                        for (var i = 0; i < rolePermissions.length; i++) {
                            var rolePermission2 = self.get('store').peekRecord('role-permission', rolePermissions[i]);
                            rolePermission2.get('roleCodes').pushObject(roleCode);
                            rolePermission2.save();
                        }
                    }, 1000);
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').editFail('Role Code'), {closeAfter: null});
                });
            });
            this.set('isViewing', false);
        },
        cancel: function () {
            this.set('isViewing', false);
        }
    }
});
