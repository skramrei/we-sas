import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),
    tagName: 'tr',

    willDestroyElement: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },
    didRender: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },

    mouseEnter: function () {
        if (!this.get('isViewing')) {
            this.$('td.right.aligned > button').css('visibility', 'visible');
        }
    },
    mouseLeave: function () {
        this.$('td.right.aligned > button').css('visibility', 'hidden');
    },

    loadHSSubjects: function () {
        this.set('hsSubjects', this.get('store').findAll('hs-subject'));
        this.get('hsCourseMark').get('subject').then((subject) => {
            this.set('subject', subject);
        });
    }.property(),
    loadSecondarySchools: function () {
        this.set('secondarySchools', this.get('store').findAll('secondary-school'));
        this.get('hsCourseMark').get('school').then((school) => {
            this.set('school', school);
        });
    }.property(),

    isViewing: false,

    actions: {

        setSubject: function (comp, id) {
            this.set('subject', this.get('store').find('hs-subject', id));
        },
        setSchool: function (comp, id) {
            this.set('school', this.get('store').find('secondary-school', id));
        },

        edit: function (id) {
            this.$('.ui.view.modal').modal('show');
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
            this.set('isViewing', true);

            var self = this;
            this.get('store').find('hs-course-mark', id).then(function (hsCourseGrade) {

                self.set('level', hsCourseGrade.get('level'));
                self.set('source', hsCourseGrade.get('source'));
                self.set('unit', hsCourseGrade.get('unit'));
                self.set('grade', hsCourseGrade.get('grade'));

                self.$('.subject.selection').dropdown('set selected', self.get('subject').get('name'));
                self.$('.school.selection').dropdown('set selected', self.get('school').get('name'));
            });
        },
        close: function () {
            this.$('.ui.view.modal').modal('hide');
            this.$('.ui.small.basic.delete.modal').modal('hide');
            this.set('isViewing', false);
        },
        submit: function (id) {

            var self = this;

            this.get('store').find('hs-course-mark', id).then(function (hsCourseGrade) {
                hsCourseGrade.set('level', self.get('level'));
                hsCourseGrade.set('source', self.get('source'));
                hsCourseGrade.set('unit', self.get('unit'));
                hsCourseGrade.set('grade', self.get('grade'));
                hsCourseGrade.set('subject', self.get('subject'));
                hsCourseGrade.set('school', self.get('school'));
                hsCourseGrade.save().then(function () {
                    self.get('notify').success(self.get('notifyMessages').editSuccess('High School Course Mark', self.get('grade')));
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').editFail('High School Course Mark'), {closeAfter: null});
                });
            });

            this.set('isViewing', false);
            this.$('.ui.view.modal').modal('hide');
        },

        warning: function () {
            this.$('.ui.small.basic.delete.modal').modal('show');
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
            this.set('isViewing', true);
        },
        remove: function (id) {

            var self = this;

            this.get('store').find('hs-course-mark', id).then(function (hsCourseGrade) {

                hsCourseGrade.set('subject', null);
                hsCourseGrade.set('school', null);

                hsCourseGrade.save().then(function () {
                    hsCourseGrade.destroyRecord().then(function () {
                        self.get('notify').success(self.get('notifyMessages').deleteSuccess('High School Course Mark'));
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').deleteFail('High School Course Mark'), {closeAfter: null});
                    });
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').deleteFail('High School Course Mark'), {closeAfter: null});
                });
            });

            this.set('isViewing', false);
            this.$('.ui.small.basic.delete.modal').modal('hide');
        }
    }

});
