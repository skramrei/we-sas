import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),
    tagName: 'tr',

    mouseEnter: function () {
        if (!this.get('isEditing') && !this.get('isRemoving')) {
            this.$('td.right.aligned > button').css('visibility', 'visible');
        }
    },
    mouseLeave: function () {
        this.$('td.right.aligned > button').css('visibility', 'hidden');
    },

    willDestroyElement: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },
    didRender: function () {
        // NOTE: The content of the popup is the button's title element by default
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },


    provinces: function () {
        return this.get('store').findAll('province');
    }.property(),

    isEditing: false,
    isRemoving: false,
    provinceId: null,

    actions: {
        resetFormFields: function () {
            this.set('name', '');
            this.set('provinceId', null);
        },

        setProvinceId: function (comp, id) {
            this.set('provinceId', id);
        },
        edit: function (name) {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('name', name);
            this.set('isEditing', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        warning: function () {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('isRemoving', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        cancel: function () {
            this.set('isEditing', false);
            this.set('isRemoving', false);
            this.send('resetFormFields');
        },
        submit: function (id, name) {

            // Set flags for the fields that need to be updated.
            var updateName = false;
            var updateProvince = false;
            var updateAnyField = false;
            if (name !== undefined && name !== null && name !== "") {
                updateName = true;
                updateAnyField = true;
            }
            if (this.get('provinceId') !== null) {
                updateProvince = true;
                updateAnyField = true;
            }
            if (!updateAnyField) {
                // There is actually nothing to update so do nothing
                this.get('notify').warning(this.get('notifyMessages').noChanges('City'));
                return;
            }

            // There must be something to update so continue
            var self = this;

            this.get('store').find('city', id).then(function (city) {
                if (updateName) {
                    city.set('name', name);
                }
                if (updateProvince) {
                    city.set('province', this.get('store').peekRecord('province', this.get('provinceId')));
                }

                city.save().then(function () {
                    self.get('notify').success(self.get('notifyMessages').editSuccess('City', name));
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').editFail(name), {closeAfter: null});
                });
            });

            this.send('resetFormFields');
            this.set('isEditing', false);
        },
        remove: function (id) {

            var self = this;

            this.get('store').find('city', id).then(function (city) {

                for (var i = 0; i < city.students.length; i++) {
                    city.students[i] = null;
                }
                city.set('province', null);

                city.save().then(function () {
                    city.destroyRecord().then(function () {
                        self.get('notify').success(self.get('notifyMessages').deleteSuccess('City'));
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').deleteFail('City'), {closeAfter: null});
                    });
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').deleteFail('City'), {closeAfter: null});
                });
            });
        }
    }
});
