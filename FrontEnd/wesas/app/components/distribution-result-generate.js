import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    isGenerating: false,

    actions: {
    	generateResult: function() {
            this.set('isGenerating', true);
        },
        distribute: function() {
            this.get('store').query('distribution-result', {generate: true}).then(() => {
                this.get('onGenerate')();
            });
            this.set('isGenerating', false);
        },
        cancel: function() {
            this.set('isGenerating', false);
        }
    }
});
