import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    isCreatingAdmissionRule: false,
    descriptionError: false,
    booleanExpression: null,
    logicalLink: null,
    isCreatingExpression: true, // Start with true because we want to add at least one expression on creation
    logicalExpressionsToSave: [],
    currentRule: null,

    expressions: function () {
        return this.get('logicalExpressionsToSave');
    }.property(),

    actions: {
        createAdmissionRule: function () {
            this.set('isCreatingAdmissionRule', true);
            this.get('store').createRecord('admission-rule').save()
                .then((rule) => {
                    this.set('currentRule', rule);
                });
        },
        cancelCreateAdmissionRule: function () {

            // Need to delete all the logical expressions that were created and saved during creation
            for (var i=0; i<this.get('logicalExpressionsToSave').length; i++) {
                this.get('logicalExpressionsToSave')[i].destroyRecord();
            }

            this.get('currentRule').destroyRecord().then(() => {
                this.set('currentRule', null);
                this.set('isCreatingAdmissionRule', false);
                this.set('logicalExpressionsToSave', []);
                this.notifyPropertyChange('expressions');
                this.set('descriptionError', false);
                this.$('#admission-rule-create-description').removeClass('field-error');
            });


        },
        descriptionEntered: function () {
            this.set('descriptionError', false);
            this.$('#admission-rule-create-description').removeClass('field-error');
        },
        addLogicalExpression: function () {
            this.set('isCreatingExpression', true);
        },
        onExpressionCreated: function (booleanExpression, logicalLink, isForFull, wasCanceled) {
            if (wasCanceled) {
                this.set('isCreatingExpression', false);
            }
            else {
                // Add this expression to the list of expressions to save, these expressions are always the first ones in their lists
                var newExpression = this.get('store').createRecord('logical-expression', {
                    booleanExp: booleanExpression,
                    logicalLink: logicalLink,
                    isFirst: true,
                    ifFull: isForFull,
                    rule: this.get('currentRule')
                });
              newExpression.save().then((savedExpression) => {
                    this.get('logicalExpressionsToSave').push(savedExpression);
                    this.notifyPropertyChange('expressions');
                    this.set('isCreatingExpression', false);
                });
            }
        },

        submitAdmissionRule: function (description) {

            // Validate inputs
            if (description === null || description === undefined || description === "") {
                this.set('descriptionError', true);
                this.$('#admission-rule-create-description').addClass('field-error');
                return;
            }

            // Input valid so continue with admission rule creation

            // All the logical expressions have already been saved so just update the description and save
            this.get('currentRule').set('description', description);
            this.get('currentRule').save().then(() => {
                this.set('currentRule', null);
                this.get('notify').success(this.get('notifyMessages').genCreateSuccess('Admission Rule'));
                this.set('logicalExpressionsToSave', []);
                this.notifyPropertyChange('expressions');
                this.set('description', '');
                this.set('firstExpression', true);
                this.set('isCreatingAdmissionRule', false);
            }, () => {
                this.get('notify').alert(this.get('notifyMessages').createFail("rule"), { closeAfter: null });
                this.set('currentRule', null);
                this.set('logicalExpressionsToSave', []);
                this.notifyPropertyChange('expressions');
                this.set('description', '');
                this.set('firstExpression', true);
                this.set('isCreatingAdmissionRule', false);

            });

        }
    }
});
