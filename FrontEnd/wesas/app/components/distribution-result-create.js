import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    isCreatingDistributionResult: false,
    studentId: null,

    students: function () {
        return this.get('store').findAll('student');
    }.property(),

    actions: {
        // The first parameter 'component' of the below function MUST be there
        setStudentId: function (component, studentId) {
            this.set('studentId', studentId);
        },
        createDistributionResult: function () {
            this.set('isCreatingDistributionResult', true);
        },
        cancelCreateDistributionResult: function () {
            this.set('isCreatingDistributionResult', false);
        },
        submitDistributionResult: function (date) {

            var store = this.get('store');

            var student = store.peekRecord('student', this.get('studentId'));

            store.createRecord('distribution-result', {
                student: student,
                date: new Date(date)
            }).save()
                .then(() => {
                    this.get('notify').success(this.get('notifyMessages').createSuccess('Distribution Result', name));
                }, () => {
                    this.get('notify').alert(this.get('notifyMessages').createFail(name), {
                        closeAfter: null
                    });
                }
            );

            this.set('studentId', '');
            this.set('isCreatingDistributionResult', false);
        }
    }
});
