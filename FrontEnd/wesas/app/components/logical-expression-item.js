import Ember from 'ember';

// Sets the private display expression field for this component as the name for a certain course code
function getCourseNameById (obj, id) {
    obj.get('store').findRecord('course-code', id).then((course) => {
        obj.set('_displayExpression', (course.get('code') + course.get('number')));
        obj.notifyPropertyChange('displayExpression');
    }, () => {
        obj.get('notify').alert('Error loading logical expressions.', { closeAfter: null });
    });
}

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),
    //linkExpression: Ember.inject.service('link-expression'),

    isAddingExpression: false,
    loadingExpression: false,
    _displayExpression: null,
    isEditing: false,


    linkIsNotNone: function () {
        return this.get('logical_expression.logicalLink') !== 'None';
    }.property(),

    displayExpression: function () {
        var expressionWords = this.get('logical_expression.booleanExp').split(' ');
        var usedWhen = '';

        if (this.get('logical_expression.ifFull')) {
            usedWhen = ' (applied only when program is full)';
        }

        if (expressionWords[0] === 'size') {
            return 'Total Number of Students ' + '<=' + ' ' + expressionWords[2] + usedWhen;
        }
        else if (expressionWords[0] === 'average') {
            return 'Weighted Average ' + expressionWords[1] + ' ' + expressionWords[2] + usedWhen;
        }
        else {
            // First word must be the id of a Course Code
            if (this.get('_displayExpression') == null) {
                // Update the Course Code Name
                this.set('loadingExpression', true);
                getCourseNameById(this, expressionWords[0]);
            }
            else {
                // Name must have been updated so return a value
                this.set('loadingExpression', false);
                const exp = this.get('_displayExpression');
                this.set('_displayExpression', null);
                return exp + ' ' + expressionWords[1] + ' ' + expressionWords[2] + usedWhen;
            }
        }
    }.property(),

    didRender: function () {
        // NOTE: The content of the popup is the button's title element by default
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },

    actions: {
        addLinkedExpression: function () {
            if (this.get('logical_expression.logicalLink') !== 'None') {
                // Can't have more than one logiclal expression
                this.$('.popup.visible').removeClass('visible').addClass('hidden');
                this.get('notify').warning('You can not add more than one linked expression.', {closeAfter: 7000});
            } else {
                this.set('isAddingExpression', true);
                this.$('.popup.visible').removeClass('visible').addClass('hidden');
            }
        },

        onLinkedExpressionCreated: function (booleanExpression, link, isForFull, canceled) {
            if (canceled) {
                this.set('isAddingExpression', false);
            } else {
                var expressionToLinkWith = this.get('logical_expression');
                const rule = expressionToLinkWith.get('rule');

                // Create the new expression, these expressions are always not the first ones on their linked list of expressions
                var newExpression = this.get('store').createRecord('logical-expression', {
                    booleanExp: booleanExpression,
                    logicalLink: 'None',
                    rule: rule,
                    isFirst: false,
                    ifFull: isForFull,
                });

                // Find the last link in this expressions linked list of rules
                var baseExpressionId = this.get('logical_expression.id');
                expressionToLinkWith = this.get('store').peekRecord('logical-expression', baseExpressionId);
                var newLinkWithId = null;

                // For some reason ember want to create a two-way link between the base expression and it link even though the server
                // explicitly sets the link to null. So we need to do this complicated check.
                var previousId = null;
                while (expressionToLinkWith.get('link.id') != null && expressionToLinkWith.get('link.id') !== previousId) {
                    newLinkWithId = expressionToLinkWith.get('link.id');
                    previousId = expressionToLinkWith.get('id');
                    expressionToLinkWith = this.get('store').peekRecord('logical-expression', newLinkWithId);
                }

                expressionToLinkWith.set('logicalLink', link);
                expressionToLinkWith.set('link', newExpression);

                newExpression.save().then(() => {
                    expressionToLinkWith.save().then(() => {
                        this.notifyPropertyChange('linkIsNotNone');
                    });
                });

                this.set('isAddingExpression', false);
            }
        },

        editExpression: function () {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('isEditing', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },

        submitEditedExpression: function (booleanExpression, link, isForFull, canceled) {
            if (canceled) {
                this.set('isEditing', false);
            } else {

                // This logical expression must be the first in its chain
                var expressionToModify = this.get('store').peekRecord('logical-expression', this.get('logical_expression.id'));

                expressionToModify.set('booleanExp', booleanExpression);
                expressionToModify.set('logicalLink', link);
                expressionToModify.set('ifFull', isForFull);

                expressionToModify.save()
                    .then((newExpression) => {
                        this.set('logical_expression', newExpression);
                        this.notifyPropertyChange('displayExpression');
                        this.get('notify').success(this.get('notifyMessages').editSuccess('Logical Expression', ''));
                    }, () => {
                        this.get('notify').alert(this.get('notifyMessages').editFail('Logical Expression'), {closeAfter: null});
                    });
            }
        },

        deleteExpression: function () {

            var expressionToDelete = this.get('store').peekRecord('logical-expression', this.get('logical_expression.id'));

            if (this.get('logical_expression.link.content') != null) {
                // Need to delete the link too
                this.get('store').find('logical-expression', this.get('logical_expression.link.content.id'))
                    .then((linkedExpressionToDelete) => {
                        linkedExpressionToDelete.destroyRecord()
                            .then(() => {
                                expressionToDelete.destroyRecord()
                                    .then(() => {
                                        this.get('notify').success(this.get('notifyMessages').deleteSuccess('Logical Expression'));
                                    }, () => {
                                        this.get('notify').alert(this.get('notifyMessages').deleteFail('Logical Expression'), {closeAfter: null});
                                    });
                            }, () => {
                                this.get('notify').alert(this.get('notifyMessages').deleteFail('Logical Expression'), {closeAfter: null});
                            });
                    }, () => {
                        this.get('notify').alert(this.get('notifyMessages').deleteFail('Logical Expression'), {closeAfter: null});
                    });
            } else {
                // Just destroy this expression
                expressionToDelete.destroyRecord()
                    .then(() => {
                        this.get('notify').success(this.get('notifyMessages').deleteSuccess('Logical Expression'));
                    }, () => {
                        this.get('notify').alert(this.get('notifyMessages').deleteFail('Logical Expression'), {closeAfter: null});
                    });
            }

        }
    }

});
