import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    isCreating: false,

    actions: {
        create: function () {
            this.set('isCreating', true);
        },
        cancel: function () {
            this.set('isCreating', false);
        },
        submit: function (name) {

            var self = this;

            this.get('store').createRecord('faculty', {
                name: name
            }).save().then(function () {
                self.get('notify').success(self.get('notifyMessages').createSuccess('Faculty', name));
            }, function () {
                self.get('notify').alert(self.get('notifyMessages').createFail(name), {closeAfter: null});
            });

            this.set('name', '');
            this.set('isCreating', false);
        }
    }
});
