import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    ANU01IsPermitted: Ember.computed(function () {
        var authentication = this.get('oudaAuth');
        if (authentication.getName === "Root") {
            return true;
        } else {
            return (authentication.get('userCList').indexOf("ANU01") >= 0);
        }
    }),

    actions: {
        create: function () {
            this.$('.ui.small.modal').modal('show');
        },
        close: function () {
            this.$('.ui.form').form('clear');
        },
        submit: function () {

            var store = this.get('store');
            var authentication = this.get('oudaAuth');
            var self = this;

            store.createRecord('user', {
                firstName: this.get('firstName'),
                lastName: this.get('lastName'),
                email: this.get('email')
            }).save().then(function (user) {
                store.createRecord('password', {
                    userName: self.get('username'),
                    encryptedPassword: authentication.hash(self.get('password')),
                    userAccountExpiryDate: new Date(self.get('expiry')),
                    passwordMustChanged: false,
                    passwordReset: false,
                    user: user
                }).save().then(function (password) {
                    var addPassword = store.peekRecord('user', user.get('id'));
                    addPassword.set('userShadow', password);
                    addPassword.save().then(function () {
                        self.get('notify').success(self.get('notifyMessages').createSuccess('User', self.get('firstName')));
                        self.$('.ui.form').form('clear');
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').createFail(self.get('firstName')), {closeAfter: null});
                    });
                });
            });
        }
    }
});
