import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    routing: Ember.inject.service('-routing'),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    user: null,
    firstName: null,
    lastName: null,
    email: null,

    loadUser: Ember.computed(function () {
        var userName = this.get('userName');
        var store = this.get('store');
        var self = this;
        store.queryRecord('password', {filter: {userName: userName}}).then(function (userShadow) {
            store.find('user', userShadow.get('user').get('id')).then(function (user) {
                self.set('user', user);
            });
        });
    }),

    EUP01IsPermitted: Ember.computed(function () {
        var authentication = this.get('oudaAuth');
        if (authentication.getName === "Root") {
            return true;
        } else {
            return (authentication.get('userCList').indexOf("EUP01") >= 0);
        }
    }),

    actions: {

        edit: function (firstName, lastName, email) {
            this.set('firstName', firstName);
            this.set('lastName', lastName);
            this.set('email', email);
            this.$('.ui.modal.edit.small').modal('show');
        },
        submit: function (id) {

            var self = this;

            this.get('store').find('user', id).then(function (user) {
                user.set('firstName', self.get('firstName'));
                user.set('lastName', self.get('lastName'));
                user.set('email', self.get('email'));
                user.save().then(function () {
                    self.get('notify').success(self.get('notifyMessages').editSuccess('User', self.get('firstName')));
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').editFail(self.get('firstName')), {closeAfter: null});
                });
            });

            this.$('.ui.modal.edit.small').modal('hide');
        },

        resetPassword: function () {
            this.$('.ui.modal.resetPassword.small').modal('show');
        },
        submitPassword: function (id) {
id = id;
            var oldPass = this.get('oldPass'),
                newPass1 = this.get('newPass1'),
                newPass2 = this.get('newPass2'),
                userName = this.get('userName'),
                authentication = this.get('oudaAuth'),
                store = this.get('store'),
                self = this;

            if (newPass1 !== newPass2) {
                self.get('notify').alert(self.get('notifyMessages').editFail('your password'), {closeAfter: null});
                return;
            }

            authentication.open(userName, oldPass).then(function () {

                var hash = authentication.hash(newPass1);

                store.queryRecord('password', {filter: {userName: userName}}).then(function (userShadow) {
                    userShadow.set('encryptedPassword', hash);
                    userShadow.set('passwordMustChanged', true);
                    userShadow.set('passwordReset', false);
                    userShadow.save().then(function () {
                        self.get('oudaAuth').close();
                        self.get('routing').transitionTo('login');
                    });
                });
            }, function () {
                self.get('notify').alert(self.get('notifyMessages').editFail('your password'), {closeAfter: null});
            });
        },

        close: function () {
            this.$('.ui.modal').modal('hide');
        }
    }
});
