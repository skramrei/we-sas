import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    countries: function () {
        return this.get('store').findAll('country');
    }.property(),

    isCreating: false,
    countryId: null,

    actions: {
        setCountryId: function (comp, id) {
            this.set('countryId', id);
        },
        create: function () {
            this.set('isCreating', true);
        },
        cancel: function () {
            this.set('isCreating', false);
        },
        submit: function (name) {

            var self = this;

            this.get('store').createRecord('province', {
                name: name,
                country: self.get('store').peekRecord('country', self.get('countryId'))
            }).save().then(function () {
                self.get('notify').success(self.get('notifyMessages').createSuccess('Province', name));
            }, function () {
                self.get('notify').alert(self.get('notifyMessages').createFail(name), {closeAfter: null});
            });

            this.set('name', '');
            this.set('countryId', null);
            this.set('isCreating', false);
        }
    }
});
