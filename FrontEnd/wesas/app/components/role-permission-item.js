import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),
    tagName: 'tr',

    willDestroyElement: function() {
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },
    didRender: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },

    mouseEnter: function () {
        if (!this.get('isViewing')) {
            this.$('td.right.aligned > button').css('visibility', 'visible');
        }
    },
    mouseLeave: function () {
        this.$('td.right.aligned > button').css('visibility', 'hidden');
    },

    isViewing: false,

    actions: {

        warning: function () {
            this.$('.ui.small.basic.modal').modal('show');
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
            this.set('isViewing', true);
        },
        remove: function (id) {

            var self = this;

            this.get('store').find('role-permission', id).then(function (rolePermission) {

                for (var i = 0; i < rolePermission.roleCodes.length; i++) {
                    rolePermission.roleCodes[i] = null;
                }
                rolePermission.set('roleCodes', []);

                rolePermission.save().then(function () {
                    rolePermission.destroyRecord().then(function () {
                        self.get('notify').success(self.get('notifyMessages').deleteSuccess('Role Permission'));
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').deleteFail('Role Permission'), {closeAfter: null});
                    });
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').deleteFail('Role Permission'), {closeAfter: null});
                });
            });

            this.set('isViewing', false);
        }
    }
});
