import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    students: function () {
        return this.get('store').findAll('student');
    }.property(),

    isCreating: false,
    studentId: null,

    studentError: false,
    dateError: false,
    commentError: false,

    actions: {
        setStudentId: function (comp, id) {
            this.set('studentId', id);
            this.set('studentError', false);
            this.$('#student-field').removeClass('field-error');
        },
        create: function () {
            this.set('isCreating', true);
        },
        cancel: function () {
            this.set('isCreating', false);
            this.set('dateError', false);
            this.$('#basis-of-admission-create-date').removeClass('field-error');
            this.set('commentError', false);
            this.$('#basis-of-admission-create-comment').removeClass('field-error');
            this.set('studentError', false);
            this.$('#student-field').removeClass('field-error');
        },
        onDateInput: function () {
            this.set('dateError', false);
            this.$('#basis-of-admission-create-date').removeClass('field-error');
        },
        onCommentInput: function () {
            this.set('commentError', false);
            this.$('#basis-of-admission-create-comment').removeClass('field-error');
        },

        submit: function (date,comment) {

            // Display any errors for invalid inputs
            var foundInvalidFields = false;

            // student must be set
            if (this.get('studentId')!==null) {
                this.set('studentError', false);
                this.$('#student-field').removeClass('field-error');
            } else {
                this.set('studentError', true);
                this.$('#student-field').addClass('field-error');
                foundInvalidFields = true;
            }

            if (date === undefined || date === null || date === "") {
                this.set('dateError', true);
                this.$('#basis-of-admission-create-date').addClass('field-error');
                foundInvalidFields = true;
            }
            if (comment === undefined || comment=== null || comment === "") {
                this.set('commentError', true);
                this.$('#basis-of-admission-create-comment').addClass('field-error');
                foundInvalidFields = true;
            }

            if (foundInvalidFields) {
                return;
            }

            // All inputs are valid so conintue creating comment code
            var store = this.get('store');

            store.createRecord('basis-of-admission', {
                date:date,
                comment:comment,
                student: this.get('store').peekRecord('student', this.get('studentId'))
            }).save()
                .then(() => {
                    this.get('notify').success(this.get('notifyMessages').createSuccess('basis of admission', comment));
                }, () => {
                    this.get('notify').alert(this.get('notifyMessages').createFail(comment), {
                        closeAfter: null
                    });
                }
            );

            this.set('date', null);
            this.set('comment', null);
            this.set('isCreating', false);
            this.set('dateError', false);
            this.$('#basis-of-admission-create-date').removeClass('field-error');
            this.set('commentError', false);
            this.$('#basis-of-admission-create-comment').removeClass('field-error');
            this.set('studentError', false);
            this.$('#student-field').removeClass('field-error');
        }
    }
});
