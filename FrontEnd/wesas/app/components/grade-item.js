import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),
    tagName: 'tr',

    mouseEnter: function () {
        if (!this.get('isEditing') && !this.get('isRemoving')) {
            this.$('td.right.aligned > button').css('visibility', 'visible');
        }
    },
    mouseLeave: function () {
        this.$('td.right.aligned > button').css('visibility', 'hidden');
    },

    willDestroyElement: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },
    didRender: function () {
        // NOTE: The content of the popup is the button's title element by default
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },

    courseCodes: function () {
        return this.get('store').findAll('course-code');
    }.property(),
    programRecords: function () {
        return this.get('store').findAll('program-record');
    }.property(),
    students: function () {
        return this.get('store').findAll('student');
    }.property(),

    isEditing: false,
    isRemoving: false,
    courseCodeId: null,
    programRecordId: null,
    studentId: null,

    actions: {
        setCourseCodeId: function (comp, id) {
            this.set('courseCodeId', id);
        },
        setProgramRecordId: function (comp, id) {
            this.set('programRecordId', id);
        },
        setStudentId: function (comp, id) {
            this.set('studentId', id);
        },
        edit: function (mark, section) {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('mark', mark);
            this.set('section', section);
            this.set('isEditing', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        warning: function () {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('isRemoving', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        cancel: function () {
            this.set('isEditing', false);
            this.set('isRemoving', false);
        },
        submit: function (id, mark, section) {

            var self = this;
            var courseCode = this.get('store').peekRecord('course-code', this.get('courseCodeId'));
            var programRecord = this.get('store').peekRecord('program-record', this.get('programRecordId'));
            var student = this.get('store').peekRecord('student', this.get('studentId'));

            this.get('store').find('grade', id).then(function (grade) {
                grade.set('mark', mark);
                grade.set('section', section);
                grade.set('courseCode', courseCode);
                grade.set('programRecord', programRecord);
                grade.set('student', student);
                grade.save().then(function () {
                    self.get('notify').success(self.get('notifyMessages').createSuccess('Grade', mark));
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').createFail(mark), {closeAfter: null});
                });
            });

            this.set('mark', '');
            this.set('section', '');
            this.set('courseCodeId', null);
            this.set('programRecordId', null);
            this.set('studentId', null);
            this.set('isEditing', false);
        },
        remove: function (id) {

            var self = this;

            this.get('store').find('grade', id).then(function (grade) {

                grade.set('courseCode', null);
                grade.set('programRecord', null);
                grade.set('student', null);

                grade.save().then(function () {
                    grade.destroyRecord().then(function () {
                        self.get('notify').success(self.get('notifyMessages').deleteSuccess('Grade'));
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').deleteFail('Grade'), {closeAfter: null});
                    });
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').deleteFail('Grade'), {closeAfter: null});
                });
            });

            this.set('isRemoving', false);
        }
    }
});
