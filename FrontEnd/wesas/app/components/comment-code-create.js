import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    isCreatingCommentCode: false,
    codeError: false,
    actionError: false,
    descriptionError: false,
    notesError: false,

    actions: {
        createCommentCode: function () {
            this.set('isCreatingCommentCode', true);
        },
        cancelCreateCommentCode: function () {
            this.set('isCreatingCommentCode', false);
            this.set('codeError', false);
            this.$('#comment-code-create-code').removeClass('field-error');
            this.set('actionError', false);
            this.$('#comment-code-create-action').removeClass('field-error');
            this.set('descriptionError', false);
            this.$('#comment-code-create-description').removeClass('field-error');
            this.set('notesError', false);
            this.$('#comment-code-create-notes').removeClass('field-error');
        },
        onCodeInput: function () {
            this.set('codeError', false);
            this.$('#comment-code-create-code').removeClass('field-error');
        },
        onActionInput: function () {
            this.set('actionError', false);
            this.$('#comment-code-create-action').removeClass('field-error');
        },
        onDescriptionInput: function () {
            this.set('descriptionError', false);
            this.$('#comment-code-create-description').removeClass('field-error');
        },
        onNotesInput: function () {
            this.set('notesError', false);
            this.$('#comment-code-create-notes').removeClass('field-error');
        },
        submitCommentCode: function (code, progAction, description, notes) {

            // Display any errors for invalid inputs
            var foundInvalidFields = false;
            if (code === undefined || code === null || code === "") {
                this.set('codeError', true);
                this.$('#comment-code-create-code').addClass('field-error');
                foundInvalidFields = true;
            }
            if (progAction === undefined || progAction === null || progAction === "") {
                this.set('actionError', true);
                this.$('#comment-code-create-action').addClass('field-error');
                foundInvalidFields = true;
            }
            if (description === undefined || description === null || description === "") {
                this.set('descriptionError', true);
                this.$('#comment-code-create-description').addClass('field-error');
                foundInvalidFields = true;
            }
            if (notes === undefined || notes === null || notes === "") {
                this.set('notesError', true);
                this.$('#comment-code-create-notes').addClass('field-error');
                foundInvalidFields = true;
            }
            if (foundInvalidFields) {
                return;
            }

            // All inputs are valid so conintue creating comment code
            var store = this.get('store');

            store.createRecord('comment-code', {
                code: code,
                progAction: progAction,
                description: description,
                notes: notes
            }).save()
                .then(() => {
                    this.get('notify').success(this.get('notifyMessages').createSuccess('Comment Code', name));
                }, () => {
                    this.get('notify').alert(this.get('notifyMessages').createFail(name), {
                        closeAfter: null
                    });
                }
            );

            this.set('code', null);
            this.set('progAction', null);
            this.set('description', null);
            this.set('notes', null);
            this.set('isCreatingCommentCode', false);
        }
    }
});
