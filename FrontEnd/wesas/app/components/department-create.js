import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    faculties: function () {
        return this.get('store').findAll('faculty');
    }.property(),

    isCreating: false,
    facultyId: null,

    actions: {
        setFacultyId: function (comp, id) {
            this.set('facultyId', id);
        },
        create: function () {
            this.set('isCreating', true);
        },
        cancel: function () {
            this.set('isCreating', false);
        },
        submit: function (name) {

            var self = this;

            this.get('store').createRecord('department', {
                name: name,
                faculty: self.get('store').peekRecord('faculty', self.get('facultyId'))
            }).save().then(function () {
                self.get('notify').success(self.get('notifyMessages').createSuccess('Department', name));
            }, function () {
                self.get('notify').alert(self.get('notifyMessages').createFail(name), {closeAfter: null});
            });

            this.set('name', '');
            this.set('facultyId', null);
            this.set('isCreating', false);
        }
    }
});
