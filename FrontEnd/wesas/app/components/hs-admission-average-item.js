import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),
    tagName: 'tr',

    mouseEnter: function () {
        if (!this.get('isEditing') && !this.get('isRemoving')) {
            this.$('td.right.aligned > button').css('visibility', 'visible');
        }
    },
    mouseLeave: function () {
        this.$('td.right.aligned > button').css('visibility', 'hidden');
    },

    willDestroyElement: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },
    didRender: function () {
        // NOTE: The content of the popup is the button's title element by default
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});
    },

    students: function () {
        return this.get('store').findAll('student');
    }.property(),

    isEditing: false,
    isRemoving: false,
    studentId: null,

    g11Error: false,
    g11ErrorMessage: '',
    firstError: false,
    firstErrorMessage: '',
    MYError: false,
    MYErrorMessage: '',
    finalError: false,
    finalErrorMessage: '',

    actions: {
        setStudentId: function (comp, id) {
            this.set('studentId', id);
        },
        onG11Input: function(value) {
            if (!isNaN(value) && value >= 0 && value <= 100) {
                this.set('g11Error', false);
                this.$('#g11-field').removeClass('field-error');
                //this.set('showExpression', true);
                //this.set('valueText', value);
            } else {
                this.set('g11Error', true);
                this.$('#g11-field').addClass('field-error');
                this.set('g11ErrorMessage', 'Please enter a number between 0 and 100');
            }
        },
        onFirstInput: function(value) {
            if (!isNaN(value) && value >= 0 && value <= 100) {
                this.set('firstError', false);
                this.$('#first-field').removeClass('field-error');
                //this.set('showExpression', true);
                //this.set('valueText', value);
            } else {
                this.set('firstError', true);
                this.$('#first-field').addClass('field-error');
                this.set('firstErrorMessage', 'Please enter a number between 0 and 100');
            }
        },
        onMYInput: function(value) {
            if (!isNaN(value) && value >= 0 && value <= 100) {
                this.set('MYError', false);
                this.$('#m-y-field').removeClass('field-error');
                //this.set('showExpression', true);
                //this.set('valueText', value);
            } else {
                this.set('MYError', true);
                this.$('#m-y-field').addClass('field-error');
                this.set('MYErrorMessage', 'Please enter a number between 0 and 100');
            }
        },
        onFinalInput: function(value) {
            if (!isNaN(value) && value >= 0 && value <= 100) {
                this.set('finalError', false);
                this.$('#final-field').removeClass('field-error');
                //this.set('showExpression', true);
                //this.set('valueText', value);
            } else {
                this.set('finalError', true);
                this.$('#final-field').addClass('field-error');
                this.set('finalErrorMessage', 'Please enter a number between 0 and 100');
            }
        },
        edit: function (grade11,first,midYear,finalVal) {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('grade11', grade11);
            this.set('first', first);
            this.set('midYear', midYear);
            this.set('finalVal', finalVal);
            this.set('isEditing', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        warning: function () {
            this.$('td.right.aligned > button').css('visibility', 'hidden');
            this.set('isRemoving', true);
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
        },
        cancel: function () {
            this.set('isEditing', false);
            this.set('isRemoving', false);

            this.set('g11Error', false);
            this.set('MYError', false);
            this.set('finalError', false);
            this.set('firstError', false);
        },
        submit: function (id, grade11, first, midYear, finalVal) {

            var self = this;
            var student = this.get('store').peekRecord('student', this.get('studentId'));

            // Set flags for the fields that need to be updated and/or are invalid
            var updateG11 = false;
            var updateFirst = false;
            var updateMY = false;
            var updateFinal = false;
            var updateStudent = false;

            if (self.get('g11Error') === true || self.get('MYError') === true || self.get('finalError') === true || self.get('firstError') === true) {
            	return;
            }

            var updateAnyField = false;

            if (grade11 !== undefined && grade11 !== null && grade11 !== "") {
                updateG11= true;
                updateAnyField = true;
            }
            if (first !== undefined && first !== null && first !== "") {
                updateFirst= true;
                updateAnyField = true;
            }
            if (midYear !== undefined && midYear !== null && midYear !== "") {
                updateMY= true;
                updateAnyField = true;
            }
            if (finalVal !== undefined && finalVal !== null && finalVal !== "") {
                updateFinal= true;
                updateAnyField = true;
            }
            if (this.get('studentId') !== null) {
                updateStudent = true;
                updateAnyField = true;
            }
            if (!updateAnyField) {
                // Actually don't need to update anything so do nothing
                this.get('notify').warning(this.get('notifyMessages').noChanges('High School Admission Average'));
                this.set('isEditingProgram', false);
                return;
            }

            this.get('store').find('hsAdmissionAverage', id).then(function (hsaa) {
                if (updateG11) {
                	hsaa.set('grade11', grade11);
                }
                if (updateFirst) {
                	hsaa.set('first', first);
                }
                if (updateMY) {
                	hsaa.set('midYear', midYear);
                }
                if (updateFinal) {
                	hsaa.set('finalVal', finalVal);
                }
                if (updateStudent) {
                	hsaa.set('student', student);
                }
                hsaa.save().then(function () {
                    self.get('notify').success(self.get('notifyMessages').editSuccess('High School Admission Average', " "));
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').editFail(' '), {closeAfter: null});
                });
            });

            this.set('first', '');
            this.set('midYear', '');
            this.set('grade11', '');
            this.set('finalVal', '');
            this.set('studentId', null);
            this.set('isEditing', false);
            this.set('g11Error', false);
            this.set('MYError', false);
            this.set('finalError', false);
            this.set('firstError', false);
        },
        remove: function (id) {

            var self = this;

            this.get('store').find('hsAdmissionAverage', id).then(function (hsaa) {

                hsaa.set('student', null);

                hsaa.save().then(function () {
                    hsaa.destroyRecord().then(function () {
                        self.get('notify').success(self.get('notifyMessages').deleteSuccess('High School Admission Average'));
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').deleteFail('High School Admission Average'), {closeAfter: null});
                    });
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').deleteFail('High School Admission Average'), {closeAfter: null});
                });
            });

            this.set('isRemoving', false);
            this.set('g11Error', false);
            this.set('MYError', false);
            this.set('finalError', false);
            this.set('firstError', false);
        }
    }
});