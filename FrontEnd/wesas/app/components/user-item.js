import Ember from 'ember';

export default Ember.Component.extend({

    store: Ember.inject.service(),
    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),
    tagName: 'tr',

    willDestroyElement: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup('destroy');
    },
    didRender: function () {
        this._super(...arguments);
        this.$('.circular.icon.button').popup({position: 'top center', inline: true});

      var self = this;
      this.$(document).ready(function () {
        self.$('.vertical.menu .item').tab({history: false});
      });

    },


    mouseEnter: function () {
        if (!this.get('isViewing')) {
            this.$('td.right.aligned > button').css('visibility', 'visible');
        }
    },
    mouseLeave: function () {
        this.$('td.right.aligned > button').css('visibility', 'hidden');
    },

    isViewing: false,

    MR001IsPermitted: Ember.computed(function () {
        var authentication = this.get('oudaAuth');
        if (authentication.getName === "Root") {
            return true;
        } else {
            return (authentication.get('userCList').indexOf("MR001") >= 0);
        }
    }),
    EU001IsPermitted: Ember.computed(function () {
        var authentication = this.get('oudaAuth');
        if (authentication.getName === "Root") {
            return true;
        } else {
            return (authentication.get('userCList').indexOf("EU001") >= 0);
        }
    }),
    EU002IsPermitted: Ember.computed(function () {
        var authentication = this.get('oudaAuth');
        if (authentication.getName === "Root") {
            return true;
        } else {
            return (authentication.get('userCList').indexOf("EU002") >= 0);
        }
    }),
    EU003IsPermitted: Ember.computed(function () {
        var authentication = this.get('oudaAuth');
        if (authentication.getName === "Root") {
            return true;
        } else {
            return (authentication.get('userCList').indexOf("EU003") >= 0);
        }
    }),

    roleCodes: function () {
        return this.get('store').findAll('role-code');
    }.property(),

    userRoles: [],
    userRoleCodes: [],

    isEnabled: function () {
        return this.get('user').get('enabled');
    }.property(),

    actions: {
        enable: function () {
            var self = this;

            this.get('store').findRecord('user', this.get('user').get('id')).then(function (updatedUser) {
                if (updatedUser.get('enabled')) {
                    updatedUser.set('enabled', false);
                } else {
                    updatedUser.set('enabled', true);
                }
                updatedUser.save().then(function () {
                    self.get('notify').success(self.get('notifyMessages').editSuccess('User', ''));
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').editFail('User'), {closeAfter: null});
                });
            });
        },
        view: function (id) {
            this.$('.ui.view.modal').modal('show');
            this.set('firstName', this.get('user').get('firstName'));
            this.set('lastName', this.get('user').get('lastName'));
            this.set('email', this.get('user').get('email'));
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
            this.set('isViewing', true);

            var self = this;
            var user = this.get('store').peekRecord('user', id);
            user.get('userShadow').then(function (userShadow) {
                self.set('username', userShadow.get('userName'));
                var now = userShadow.get('userAccountExpiryDate');
                var day = ("0" + now.getDate()).slice(-2);
                var month = ("0" + (now.getMonth() + 1)).slice(-2);
                var date = now.getFullYear() + "-" + (month) + "-" + (day);
                self.set('expiry', date);
            });
            this.$('.popup.visible').removeClass('visible').addClass('hidden');

            self.set('userRoleCodes', []);
            self.set('userRoles', []);
            this.get('store').query('userRole', {filter: {user: id}}).then(function (roles) {
                roles.forEach(function (oneRole) {
                    var roleID = oneRole.get('role').get('id');
                    self.get('store').findRecord('roleCode', roleID).then(function (role) {
                        self.get('userRoleCodes').pushObject(role);
                        self.get('userRoles').pushObject(oneRole);
                    });
                });
            });
        },
        submitEdit: function (id) {
            var store = this.get('store');
            var self = this;

            var user = store.peekRecord('user', id);
            user.set('firstName', this.get('firstName'));
            user.set('lastName', this.get('lastName'));
            user.set('email', this.get('email'));

            user.save().then(function (newUser) {
                newUser.get('userShadow').then(function (userShadow) {
                    var password = store.peekRecord('password', userShadow.get('id'));
                    password.set('userName', self.get('username'));
                    password.set('userAccountExpiryDate', new Date(self.get('expiry')));
                    password.save().then(function () {
                        self.get('notify').success(self.get('notifyMessages').editSuccess('User', 'Profile'));
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').editFail('User'), {closeAfter: null});
                    });
                });
            });

            this.$('.ui.view.modal').modal('hide');
            this.set('isViewing', false);
        },
        submitPassword: function (id) {
            var store = this.get('store');
            var self = this;

            store.queryRecord('password', {filter: {user: id}}).then(function (userShadow) {
                store.find('user', userShadow.get('user').get('id')).then(function (user) {
                    var authentication = self.get('oudaAuth');
                    userShadow.set('encryptedPassword', authentication.hash(self.get('tempPass')));
                    userShadow.set('passwordMustChanged', true);
                    userShadow.set('passwordReset', true);
                    userShadow.save().then(function () {
                        user.save().then(function () {
                            self.get('notify').success(self.get('notifyMessages').editSuccess('User', 'Profile'));
                            self.$('.ui.reset.form').form('clear');
                        }, function () {
                            self.get('notify').alert(self.get('notifyMessages').editFail('your password'), {closeAfter: null});
                            self.$('.ui.reset.form').form('clear');
                        });
                    });
                });
            });

            this.$('.ui.view.modal').modal('hide');
        },
        remove: function () {
            this.$('.ui.remove.modal').modal('show');
            this.$('.popup.visible').removeClass('visible').addClass('hidden');
            this.set('isViewing', true);
        },
        cancelRemove: function () {
            this.$('.ui.view.modal').modal('show');
        },
        submitRemove: function (id) {
            var store = this.get('store');
            var self = this;

            store.find('user', id).then(function (user) {
                user.set('userRoles', []);
                user.set('userShadow', null);
                user.save().then(function () {
                    user.destroyRecord().then(function () {
                        self.get('notify').success(self.get('notifyMessages').deleteSuccess('User'));
                    }, function () {
                        self.get('notify').alert(self.get('notifyMessages').deleteFail('Role Permission'), {closeAfter: null});
                    });
                });
            });

            store.queryRecord('password', {filter: {user: id}}).then(function (userShadow) {
                userShadow.set('user', null);
                userShadow.save().then(function () {
                    userShadow.destroyRecord();
                });
            });

            store.query('userRole', {filter: {user: id}}).then(function (userRoles) {
                userRoles.forEach(function (userRole) {
                    userRole.user = null;
                    userRole.role = null;
                    userRole.save().then(function () {
                        userRole.destroyRecord();
                    });
                });
            });
            this.set('isViewing', false);
        },
        addUserRole: function (comp, id) {

            this.$('.ui.floating.dropdown .text').val('Add User Role');

            var store = this.get('store');
            var roleCode = store.peekRecord('roleCode', id);
            var self = this;

            var roleNotAssigned = this.get('userRoleCodes').every(function (oneRole) {
                return (roleCode.get('id') !== oneRole.id);
            });

            if (roleNotAssigned) {
                this.get('userRoleCodes').pushObject(roleCode);
                store.createRecord('userRole', {
                    dateAssigned: new Date(),
                    user: store.peekRecord('user', this.get('user').get('id')),
                    role: roleCode
                }).save().then(function (userRole) {
                    self.get('notify').success(self.get('notifyMessages').createSuccess('User Role', roleCode.get('name')));
                    self.get('userRoles').pushObject(userRole);
                }, function () {
                    self.get('notify').alert(self.get('notifyMessages').createFail(roleCode.get('name')), {closeAfter: null});
                });
            }
        }
    }
});
