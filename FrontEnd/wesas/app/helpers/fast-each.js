import Ember from 'ember';

export function fastEach(parameters) {

    var values = parameters[0].get('content');
    var componentName = parameters[1];
    var componentVarName = parameters[2];

    return new Ember.Handlebars.SafeString(
        values.map((item) => {
            return '{{' + componentName + ' ' + componentVarName + '=' + item.record + '}}';
        }).join('')
    );
}

export default Ember.Helper.helper(fastEach);
