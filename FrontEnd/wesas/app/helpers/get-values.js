import Ember from 'ember';

export function getValues(params) {

    var roleCode = params[0];
    var rolePermissions = params[1];

    rolePermissions.forEach(function (rolePermission) {
        rolePermission.get('roleCodes').then(function (roleCodes) {
            roleCodes.forEach(function (code) {
                if (roleCode.get('id') === code.get('id')) {
                    this.$('#' + roleCode.get('id') + 'codes').append(rolePermission.get('code'));
                }
            });
        });
    });
}

export default Ember.Helper.helper(getValues);
