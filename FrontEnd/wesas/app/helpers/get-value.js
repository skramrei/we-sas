import Ember from 'ember';

export function getValue(params) {

    var item = params[0];
    var relatedItem = params[1];
    var value = params[2];
    var defaultValue = params[3];
    var htmlId = params[4];

var self = this;
    item.get(relatedItem).then(function (result) {
        if (result !== null) {
          $('#' + htmlId).html(result.get(value));
        } else {
          $('#' + htmlId).html(defaultValue);
        }
    });
}

export default Ember.Helper.helper(getValue);
