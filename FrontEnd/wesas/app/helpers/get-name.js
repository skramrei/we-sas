import Ember from 'ember';

export function getName(params) {

    var item = params[0];
    var relatedItem = params[1];
    var name = params[2];
    var htmlId = params[3];
  var self = this;
    item.get(relatedItem).then(function (result) {
        if (result !== null) {
          self.$('#' + htmlId).html(result.get('name'));
        } else {
          self.$('#' + htmlId).html(name);
        }
    });
}

export default Ember.Helper.helper(getName);
