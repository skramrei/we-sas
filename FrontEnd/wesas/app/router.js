import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function () {
  this.route('academic-loads');
  this.route('academic-program-codes');
  this.route('cities');
  this.route('countries');
  this.route('genders');
  this.route('itr-programs');
  this.route('provinces');
  this.route('residencies');
  this.route('students');
  this.route('home', {path: '/'});
  this.route('course-codes');
  this.route('grades');
  this.route('faculties');
  this.route('departments');
  this.route('program-administrations');
  this.route('term-codes');
  this.route('admission-rules');
  // NOTE: This a catchall to show a 404 error page.
  this.route('pageNotFound', {path: '/*wildcard'});
  this.route('program-records');
  this.route('degree-codes');
  this.route('comment-codes');
  this.route('distribution-results');
  this.route('enter-data');
  this.route('populate-tables');
  this.route('records');
  this.route('admin-portal');
  this.route('login');
  this.route('user');
  this.route('users');
  this.route('user-roles');
  this.route('role-permissions');
  this.route('basis-of-admissions');
  this.route('basis-of-admission-codes');
  this.route('hs-admission-averages');
  this.route('hs-course-marks');
  this.route('hs-subjects');
  this.route('secondary-schools');
  this.route('scholar-award-codes');
  this.route('about-us');
});

export default Router;
