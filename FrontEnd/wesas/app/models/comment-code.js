import DS from 'ember-data';

export default DS.Model.extend({
  code:DS.attr(),
  progAction:DS.attr(),
  description:DS.attr(),
  notes:DS.attr(),
  results: DS.hasMany('distributionResult', {async: true}),
  programs: DS.hasMany('academicProgramCode', {async: true})
});
