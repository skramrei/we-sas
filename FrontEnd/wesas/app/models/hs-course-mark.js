import DS from 'ember-data';

export default DS.Model.extend({
    level: DS.attr(),
    source: DS.attr(),
    unit: DS.attr(),
    grade: DS.attr(),
    subject: DS.belongsTo('hs-subject', {async: true}),
    school: DS.belongsTo('secondary-school', {async: true})
});
