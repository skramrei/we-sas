import DS from 'ember-data';

export default DS.Model.extend({
    code: DS.attr(),
    sysFeature: DS.attr(),
    roleCodes: DS.hasMany('roleCode', {async: true})
});
