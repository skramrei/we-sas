import DS from 'ember-data';

export default DS.Model.extend({
  date: DS.attr(),
  commentCode: DS.belongsTo('commentCode', {async: true}),
  student: DS.belongsTo('student', {async: true}),
  rejected: DS.attr('boolean')
});
