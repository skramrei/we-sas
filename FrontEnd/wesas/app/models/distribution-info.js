import DS from 'ember-data';

// Note that the array uses the custom transform of the same name

export default DS.Model.extend({
    "date": DS.attr(),
    "dates": DS.attr('array')
});
