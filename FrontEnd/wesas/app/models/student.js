import DS from 'ember-data';

export default DS.Model.extend({
    studentNumber: DS.attr(),
    firstName: DS.attr(),
    lastName: DS.attr(),
    DOB: DS.attr('date'),
    cumAVG: DS.attr(),
    residency: DS.belongsTo('residency', {async: true}),
    gender: DS.belongsTo('gender', {async: true}),
    academicLoad: DS.belongsTo('academic-load', {async: true}),
    country: DS.belongsTo('country', {async: true}),
    province: DS.belongsTo('province', {async: true}),
    city: DS.belongsTo('city', {async: true}),
    grades: DS.hasMany('grade', {async: true}),
    itrPrograms: DS.hasMany('itr-program', {async: true}),
    distributionResults: DS.hasMany('distribution-result', {async: true}),
    admBase: DS.belongsTo('basisOfAdmission', {async: true}),
    hsGrade: DS.belongsTo('hsAdmissionAverage', {async: true}),
    hSchool: DS.hasMany('secondarySchool', {async: true}),
    awardInfo: DS.belongsTo('scholarAwardCode', {async: true})
});
