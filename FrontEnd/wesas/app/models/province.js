import DS from 'ember-data';

export default DS.Model.extend({
    name: DS.attr(),
    cities: DS.hasMany('city', {async: true}),
    country: DS.belongsTo('country', {async: true}),
    students: DS.hasMany('student', {async: true})
});
