import DS from 'ember-data';

export default DS.Model.extend({
    mark: DS.attr(),
    section: DS.attr(),
    courseCode: DS.belongsTo('course-code', {async: true}),
    programRecord: DS.belongsTo('program-record', {async: true}),
    student: DS.belongsTo('student', {async: true})
});
