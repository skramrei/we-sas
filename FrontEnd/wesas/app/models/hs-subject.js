import DS from 'ember-data';

export default DS.Model.extend({
    name: DS.attr(),
    description: DS.attr(),
    marks: DS.hasMany('hs-course-mark', {async: true})
});
