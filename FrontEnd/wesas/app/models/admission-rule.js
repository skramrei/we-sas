import DS from 'ember-data';

export default DS.Model.extend({
    description: DS.attr(),
    code: DS.hasMany('academicProgramCode', { async: true, inverse: 'rule' }),
    testExpressions: DS.hasMany('logicalExpression', { async: true })
});
