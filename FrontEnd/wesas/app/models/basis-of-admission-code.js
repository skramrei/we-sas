import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr(),
  basis: DS.belongsTo('basisOfAdmission', { async: true })
});
