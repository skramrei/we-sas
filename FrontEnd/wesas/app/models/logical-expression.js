import DS from 'ember-data';

export default DS.Model.extend({
    booleanExp: DS.attr(),
    logicalLink: DS.attr(), // This is the link between this expression and the
    link: DS.belongsTo('logicalExpression', { async: true }),
    rule: DS.belongsTo('admissionRule', { async:true }),
    isFirst: DS.attr('boolean'), // Indicates whether this is the first expression in a linked list of expressions (used to simplify iterations though all expressions associated with a particular admission rule)
    ifFull: DS.attr('boolean') // Indicates whether this is the a hard rule or only used when program cap is reached
});
