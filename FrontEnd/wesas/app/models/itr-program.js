import DS from 'ember-data';

export default DS.Model.extend({
    order: DS.attr('number'),
    eligibility: DS.attr('boolean'),
    student: DS.belongsTo('student', {async: true}),
    program: DS.belongsTo('academicProgramCode', {async: true})
});