import DS from 'ember-data';

export default DS.Model.extend({
    name: DS.attr(),
    code: DS.attr(),
    subCode: DS.attr(),
    itrProgram: DS.hasMany('itrProgram', { async: true }),
    dept: DS.hasMany('programAdministration', { async: true, inverse: 'academicProgramCode' }),
    rule: DS.belongsTo('admissionRule', { async: true, inverse: 'code' }),
    commentCode: DS.belongsTo('commentCode', { async: true})
});
