import Ember from 'ember';
import Resolver from './resolver';
import loadInitializers from 'ember-load-initializers';
import config from './config/environment';

let App;

Ember.MODEL_FACTORY_INJECTIONS = true;

App = Ember.Application.extend({
  modulePrefix: config.modulePrefix,
  podModulePrefix: config.podModulePrefix,
  Resolver,
  customEvents: {
    "mouseover": "mouseOver",
    "mouseenter": "mouseEnter",
    "mouseleave": "mouseLeave",
    "input": "input",
    "dragover": "dragover",
    "drop": "drop",
    "change": "change"
  }
});

loadInitializers(App, config.modulePrefix);

export default App;
