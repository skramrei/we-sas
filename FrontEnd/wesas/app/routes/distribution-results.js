import Ember from 'ember';

export default Ember.Route.extend({

    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    beforeModel: function () {
        if (!this.get('oudaAuth').get('isAuthenticated')) {
            this.transitionTo('login');
        }
    },

    model: function() {
        return null;
    }

});
