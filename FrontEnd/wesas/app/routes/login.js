import Ember from 'ember';

export default Ember.Route.extend({
    /*beforeModel () {
        var authentication = this.get('oudaAuth');
        var self = this;
        authentication.fetch().then(
                function (success) {
                    self.transitionTo('home');
                },
                function (error) {
                    console.log("error -->" + error);
                });
    }*/
    renderTemplate: function () {
        if (this.get('oudaAuth').get('isAuthenticated')) {
            this.get('oudaAuth').close();
            this.render('login', {
                into: 'application',
                outlet: 'login'
            });
        } else {
            this.render('login');
        }
    }
});
