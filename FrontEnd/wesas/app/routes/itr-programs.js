import Ember from 'ember';

export default Ember.Route.extend({

    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    rowWidth: function () {
        return this.$('#itr-program-head-row').width();
    }.property(),

    beforeModel: function () {
        if (!this.get('oudaAuth').get('isAuthenticated')) {
            //this.get('notify').warning(this.get('notifyMessages').login(), { closeAfter:5000 });
            this.transitionTo('login');
        }
    },

    model: function() {
        return this.store.findAll('itr-program');
    },

    actions: {
        generateItrData: function () {
            this.get('store').query('itr-program', {generate: true}).then(() => {
                this.refresh();
            });
        },
        clearItrData: function () {
            this.get('store').queryRecord('itr-program', {clearAll: true}).then(() => {
                this.refresh();
            });
        }
    }
});
