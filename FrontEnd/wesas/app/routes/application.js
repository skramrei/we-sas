//
// Created by stefan on 2016-03-12 3:58 PM
//

import Ember from 'ember';

export default Ember.Route.extend({

    notify: Ember.inject.service('notify'),
    notifyMessages: Ember.inject.service('notify-messages'),

    beforeModel () {
        var authentication = this.get('oudaAuth');
        var self = this;
        authentication.fetch().then(
                function (success) {
                  success = success;
                    self.transitionTo('home');
                },
                function (error) {
                    console.log("error -->" + error);
                    self.transitionTo('login');
                });
    }
});
